//
//  ChildButtonCell.swift
//  HT
//
//  Created by Developer on 11/6/19.
//  Copyright © 2019 dharmesh. All rights reserved.
//

import UIKit

class ChildButtonCell: UICollectionViewCell {
    
    @IBOutlet weak var stateNum: UILabel!
    @IBOutlet weak var ui_view: UIView!
    
    var entity : ChildButtonModel!{
        
        didSet{
            stateNum.text = entity.number
            
            if entity.selectState{
                ui_view.backgroundColor = .yellow
            } else {
                ui_view.backgroundColor = .white
            }
        }
    }
}
