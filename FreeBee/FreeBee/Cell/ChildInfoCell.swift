//
//  ChildInfoCell.swift
//  HT
//
//  Created by Developer on 11/7/19.
//  Copyright © 2019 dharmesh. All rights reserved.
//

import UIKit

class ChildInfoCell: UICollectionViewCell {
    
    @IBOutlet weak var ui_lblCardName: UILabel!
    @IBOutlet weak var ui_butMan: UIButton!
    @IBOutlet weak var ui_butWomen: UIButton!
    @IBOutlet weak var ui_texAge: UITextField!
    
    
    var entity : ChildInfoModel! {
        
        didSet{
            ui_lblCardName.text = "\(entity.no)"
            
            if entity.selectedSex == 1 {
                
//                ui_butMan.setTitleColor(.white, for: .normal)
                ui_butMan.backgroundColor = UIColor(named: "primaryColor")
//                ui_butWomen.setTitleColor(.black, for: .normal)
                ui_butWomen.backgroundColor = UIColor(named: "sexUnselected")
            } else if entity.selectedSex == 2 {
                
//                ui_butMan.setTitleColor(.white, for: .normal)
                ui_butMan.backgroundColor = UIColor(named: "sexUnselected")
//                ui_butWomen.setTitleColor(.yellow, for: .normal)
                ui_butWomen.backgroundColor = UIColor(named: "primaryColor")
            } else {
//                ui_butMan.setTitleColor(.black, for: .normal)
                ui_butMan.backgroundColor = UIColor(named: "sexUnselected")
//                ui_butWomen.setTitleColor(.black, for: .normal)
                ui_butWomen.backgroundColor = UIColor(named: "sexUnselected")
            }
            
            if entity.childAge > 0 {
                ui_texAge.text = "\(entity.childAge)"
            }
        }
    }
}
