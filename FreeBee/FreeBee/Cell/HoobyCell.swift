//
//  HoobyCell.swift
//  HT
//
//  Created by Developer on 11/3/19.
//  Copyright © 2019 dharmesh. All rights reserved.
//

import Foundation
import UIKit

class HobbyCell: UICollectionViewCell {
    
    @IBOutlet weak var ui_viewBack: UIView!
    @IBOutlet weak var ui_lblTopic: UILabel!
    
    var entity : HobbyModel! {
        
        didSet{
            
            ui_lblTopic.text = entity.topic
            
            if entity.selectState {
                ui_viewBack.backgroundColor = UIColor.systemYellow
                ui_lblTopic.textColor = UIColor.black
            } else {
                ui_viewBack.backgroundColor = UIColor.lightGray
                ui_lblTopic.textColor = UIColor.black
            }
        }
    }
}
