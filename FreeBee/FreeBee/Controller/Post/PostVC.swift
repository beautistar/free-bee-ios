//
//  PostVC.swift
//  HT
//
//  Created by Dharmesh on 02/12/16.
//  Copyright © 2016 dharmesh. All rights reserved.
//

import UIKit
import Foundation
import MapKit
import GrowingTextView
import DropDown
import MBProgressHUD
import Localize_Swift
import ImagePicker
import Photos

protocol PostCellDelegate {
    
    func btnRemoveTaped(cell : PostCell)
}

class PostCell : UICollectionViewCell {
    
    @IBOutlet weak var imgItem: UIImageView!
    
    var delegate : PostCellDelegate?
    
    //MARK:- Memory Management Method
     //same like dealloc in ObjectiveC
    deinit {}
    
    //MARK:- Action Method
    @IBAction func btnRemoveTaped(_ sender: UIButton) {
        delegate?.btnRemoveTaped(cell: self)
    }
    
    //MARK:- Initialisation
    override func prepareForReuse() {
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        imgItem.contentMode = .scaleAspectFill
        imgItem.clipsToBounds = true
    }
    
}

protocol PostReusableViewDelegate {
    
    func btnCameraTaped(sender: UIButton)
}

class PostReusableView : UICollectionReusableView {
    
    var delegate : PostReusableViewDelegate?
    
    @IBOutlet weak var btnCamera: UIButton!
    
    //MARK: Memory Management Method
    
    deinit {}
    
    //MARK:- Action Methods
    
    @IBAction func btnCameraTaped(_ sender: UIButton) {
        delegate?.btnCameraTaped(sender: sender)
    }
    
    //MARK:- Initialisation
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
}

protocol PostVCDelegate {
    func refreshProductList()
}

class PostVC : BaseVC, UINavigationControllerDelegate, GrowingTextViewDelegate, PostReusableViewDelegate, PostCellDelegate, MapVCDelegate {
    
    var showState = ""
    var selectedCategoryIndexs = ""
    var selectedHobbyIndexs = ""
    var filterData = [CategoryModel]()
    var dataCollection =  [HobbyModel]()
    
    var delegate : PostVCDelegate?
    var productID = ""
    
    @IBOutlet weak var uiPopupView: UIView!
    @IBOutlet weak var uiTableView: UITableView!
    @IBOutlet weak var uiLblPopupTilte: UILabel!
    @IBOutlet weak var uiCollectionHobby: UICollectionView!
    
    @IBOutlet weak var btnCategory: UIButton!
    @IBOutlet weak var lblWhatDoYouWantToSell: UILabel!
    @IBOutlet weak var lblSelling: UILabel!
    @IBOutlet weak var txtSellingItem: UITextField!
    
    @IBOutlet weak var lblQuantiAvailable: UILabel!
    @IBOutlet weak var txtQuantiAvailable: UITextField!
    @IBOutlet weak var lblQuantiPerUser: UILabel!
    @IBOutlet weak var txtQuantiPerUser: UITextField!
    @IBOutlet weak var lblDescription: UILabel!
    
    @IBOutlet weak var txtViewDescription: GrowingTextView!
    @IBOutlet weak var txtAge: UITextField!
    @IBOutlet weak var txtGender: UITextField!
    @IBOutlet weak var butGender: UIButton!
    @IBOutlet weak var txtHobby: UITextField!
    @IBOutlet weak var txtIndustry: UITextField!
    @IBOutlet weak var txtOccurence: UITextField!
    
    @IBOutlet weak var txtType: UITextField!
    @IBOutlet weak var btnType: UIButton!
    @IBOutlet weak var lblChooseCategory: UILabel!
    @IBOutlet weak var txtCategory: UITextField!
    @IBOutlet weak var lblZipcode: UILabel!
    @IBOutlet weak var txtZipcode: UITextField!
    @IBOutlet weak var lblCity: UILabel!
    @IBOutlet weak var txtCity: UITextField!
    @IBOutlet weak var lblState: UILabel!
    @IBOutlet weak var txtState: UITextField!
    @IBOutlet weak var lblCountry: UILabel!
    @IBOutlet weak var txtCountry: UITextField!
    @IBOutlet weak var collectionImages: UICollectionView!
    @IBOutlet weak var mapProduct: MKMapView!
    @IBOutlet weak var btnPost: UIButton!
    
    
    let imagePicker = UIImagePickerController()
    var isMenuScreen : Bool = true
    
    let dropDownIndustry = DropDown()
    let dropDownAge = DropDown()
    let dropDownGender = DropDown()
    let dropDownType = DropDown()
    
    let requestParam = RequestParamModal()
    
    var productImages : [UIImage] = []
    var hud : MBProgressHUD!
    let manager = PHImageManager.default()
    
    var locationManager: CLLocationManager! = CLLocationManager()
    var locationCoordinate2D : CLLocationCoordinate2D? = nil
    var isLocationDidChange : Bool = false
    var selectedAges = [String]()
    
    //MARK:- Memory Management Method
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    deinit { //same like dealloc in ObjectiveC
        
        locationManager.stopUpdatingLocation()
        MyNotificationCenter.removeObserver(self)
    }
    
    //MARK:- Custom Methods
    @objc func configureUI() {
        if productID.isEmpty {
            self.title = "Post".localized()
        } else {
            self.title = "Post Edit".localized()
        }
        
        
        MyNotificationCenter.addObserver(self, selector: #selector(profileTapped), name: NSNotification.Name(rawValue: kNotificationPostVC), object: nil)
        
        txtViewDescription.maxLength = 150
        txtViewDescription.trimWhiteSpaceWhenEndEditing = false
        txtViewDescription.placeholderColor = UIColor(white: 0.8, alpha: 1.0)
        txtViewDescription.maxHeight = 200.0
        txtViewDescription.placeholder = "i.e My first bicycle, year 2015, in perfect condition".localized() as String
        txtViewDescription.backgroundColor = UIColor.clear
        txtViewDescription.layer.cornerRadius = 4.0
        txtViewDescription.delegate = self
        
        LocalizedControl(view: lblWhatDoYouWantToSell, key : "What do you want to give away today?")
        LocalizedControl(view: lblSelling, key : "Selling")
        LocalizedControl(view: txtSellingItem, key : "i.e 21 speed 26' Bicycle")
        LocalizedControl(view: lblQuantiAvailable, key: "Quantitiy Available")
        LocalizedControl(view: lblQuantiPerUser, key: "Quantity per User")
        LocalizedControl(view: txtAge, key : "How old are you?")
        LocalizedControl(view: lblChooseCategory, key : "Choose a category")
        LocalizedControl(view: lblDescription, key: "Description")
        
        //MARK:-   Type data set
        dropDownType.dataSource = TypeData
        dropDownType.anchorView = btnType
        dropDownType.selectionAction = { [unowned self] (index, item) in
            self.txtType.text = item
            self.requestParam.productType = item
            
        }
        dropDownType.backgroundColor = UIColor.white
        dropDownType.textColor = UIColor.black
        dropDownType.separatorColor = kColorGray
        txtType.text = TypeData[0]
        
        //MARK:-   Industry data set
        dropDownIndustry.dataSource = industryData
        dropDownIndustry.anchorView = btnCategory
        dropDownIndustry.selectionAction = { [unowned self] (index, item) in
            if self.txtIndustry.text != item {
                self.txtOccurence.text = ""
            }
            self.txtIndustry.text = item
            self.requestParam.productIndustry = item
            
        }
        dropDownIndustry.backgroundColor = UIColor.white
        dropDownIndustry.textColor = UIColor.black
        dropDownIndustry.separatorColor = kColorGray
        
        //MARK:- Age data set
//        dropDownAge .dataSource = ageData
//        dropDownAge.anchorView = btnCategory
//        dropDownAge.selectionAction = { [unowned self] (index, item) in
//            self.txtAge.text = item
//            let ageTemp : [String] = item.components(separatedBy: [" ", "-"])
//            self.requestParam.productAgeFrom = ageTemp.count > 3 ? ageTemp[0] : ageTemp[0].split(every: 2)[0]
//            self.requestParam.productAgeTo = ageTemp.count > 3 ? ageTemp[1] : "100"
//        }
//        dropDownAge.backgroundColor = UIColor.white
//        dropDownAge.textColor = UIColor.black
//        dropDownAge.separatorColor = kColorGray
        
        //MARK:- gender data set
        dropDownGender .dataSource = genders
        dropDownGender.anchorView = butGender
        dropDownGender.selectionAction = { [unowned self] (index, item) in
            self.txtGender.text = item
            self.requestParam.productGender = item
        }
        dropDownGender.backgroundColor = UIColor.white
        dropDownGender.textColor = UIColor.black
        dropDownGender.separatorColor = kColorGray
        
        //location
        self.locationManager.requestWhenInUseAuthorization()
        self.locationManager.delegate = self
        
        if CLLocationManager.locationServicesEnabled() {
            switch(CLLocationManager.authorizationStatus()) {
            case .notDetermined, .restricted, .denied: break
            case .authorizedAlways, .authorizedWhenInUse:
                self.locationManager.startUpdatingLocation()
                break
            }
        } else {
            //locationManagerPopup()
        }
        
        if productID.isEmpty {
            requestParam.category = ""
            requestParam.productIndustry = ""
            requestParam.productHobby = ""
            requestParam.productOccupation = ""
            requestParam.productAgeFrom = "0"
            requestParam.productAgeTo = "100"
            requestParam.productGender = ""
            requestParam.productType = ""
        }
        else {
            if userId == 0 {
                messageBox(message: "Your device is not registered yet, please try again")
                return;
            }
            
            let request = "\(kAPIProductDetail)"
            
            let requestParam = RequestParamModal()
            requestParam.action = kAPIProductDetail
            requestParam.userId = String(userId)
            requestParam.productId = productID
            
            RequestManager.singleton.requestPOST(requestMethod: request, parameter: requestParam.toDictionary(), showLoader: true, successBlock: { (resposne :MessageModal, message : String?) in
                
                self.priviewProductDetail(resposne.productDetail)
            }, failureBlock: {(error : Error?) in })
        }
    }
    
    func priviewProductDetail(_ data: ProductDetailModal) {
        requestParam.category = data.category
        requestParam.productIndustry = data.industry
        requestParam.productHobby = data.hobby
        requestParam.productOccupation = data.occupation
        requestParam.productAgeFrom = data.age_min
        requestParam.productAgeTo = data.age_max
        requestParam.productGender = data.gender
        requestParam.productGender = data.type
        
        txtSellingItem.text = data.productName
        txtViewDescription.text = data.productDescription
        txtQuantiAvailable.text = data.enableQuantity
        txtQuantiPerUser.text = data.userQuantity
        if data.age_min != "0" && data.age_max != "100" {
            txtAge.text = data.age_min + "-" + data.age_max + " years old"  //12-17 years old
        }
        
        txtType.text = data.type
        txtGender.text = data.gender
        txtHobby.text = data.hobby
        txtIndustry.text = data.industry
        txtOccurence.text = data.occupation
        txtCategory.text = data.category
        txtZipcode.text = data.zipcode
        txtCity.text = data.city
        txtState.text = data.state
        txtCountry.text = data.country
//        productImages = data.productImage
        
    }
    
    func addAnnotation(title : String, coordinate : CLLocationCoordinate2D) {
        
        let annotation = MKPointAnnotation()
        annotation.coordinate = coordinate
        annotation.title = title
        self.mapProduct.addAnnotation(annotation)
        
        let miles : Double = 5.0;
        let scalingFactor : Double = abs(cos(2 * M_PI * coordinate.latitude / 360.0))
        
        var span : MKCoordinateSpan = MKCoordinateSpan()
        
        span.latitudeDelta = miles/69.0;
        span.longitudeDelta = miles/(scalingFactor * 69.0);
        
        var region : MKCoordinateRegion = MKCoordinateRegion()
        region.span = span;
        region.center = coordinate;
        
        self.mapProduct?.setRegion(region, animated: false)
    }
    
    func getAssetThumbnail(asset: PHAsset) -> UIImage {
        let manager = PHImageManager.default()
        let option = PHImageRequestOptions()
        var thumbnail = UIImage()
        option.isSynchronous = true
        manager.requestImage(for: asset, targetSize: CGSize(width: 800, height: 800), contentMode: .aspectFill, options: option, resultHandler: {(result, info)->Void in
            thumbnail = result!
        })
        return thumbnail
    }
    
    func locationManagerPopup() {
        
        let alertController = UIAlertController (title: kAppName, message: "Your location service currently disable, Do you wants to enable location service?", preferredStyle: .alert)
        
        let settingsAction = UIAlertAction(title: kSettings, style: .default) { (_) -> Void in
            
            guard let settingsUrl = URL(string: UIApplicationOpenSettingsURLString) else {
                return
            }
            
            if UIApplication.shared.canOpenURL(settingsUrl) {
                
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                        
                        print("Settings opened: \(success)") // Prints true
                    })
                } else {
                    UIApplication.shared.openURL(settingsUrl)
                }
            }
        }
        alertController.addAction(settingsAction)
        
        let cancelAction = UIAlertAction(title: kCancel, style: .default, handler: { (action :UIAlertAction) in
            
            if self.isMenuScreen {
                
            } else {
                //_ = self.navigationController?.popViewController(animated: true)
            }
        })
        alertController.addAction(cancelAction)
        
        present(alertController, animated: true, completion: nil)
    }
    
    //MARK:- Action Methods
    @IBAction func onTapedIndustry(_ sender: Any) {
        showState = "industry"
        dropDownIndustry.show()
    }
    
    @IBAction func btnTappedShowPopup(_ sender: UIButton) {
        if sender.tag == 1 {
            if showState == "hobby" {} else {
                uiLblPopupTilte.text = "Choose the Hobby/Activity"
                uiTableView.isHidden = true
                uiCollectionHobby.isHidden = false
                showState = "hobby"
                dataCollection.removeAll()
                
                for one in hobbys {
                    let model = HobbyModel(topic: one, state: false)
                    dataCollection.append(model)
                }
                uiCollectionHobby.reloadData()
            }
            
        }
        else if sender.tag == 2 {
            uiLblPopupTilte.text = "Choose the Category"
            uiTableView.isHidden = false
            uiCollectionHobby.isHidden = true
            showState = "category"
            filterData.removeAll()
            for i in 0 ..< categorys.count {
                filterData.append(CategoryModel(index: i, title: categorys[i], selected: false))
            }
            uiTableView.reloadData()
            
        }
        else if sender.tag == 3 {
            if txtState.text! == "" {
                messageBox(message: "Please choose the State.")
                return
            } else {
                if showState == "city" {} else {
                    uiLblPopupTilte.text = "Choose the City"
                    uiTableView.isHidden = false
                    uiCollectionHobby.isHidden = true
                    showState = "city"
                    
                    filterData.removeAll()
                    
                    var selectedStates = txtState.text?.components(separatedBy: ";")
                    var selectedCitys = txtCity.text?.components(separatedBy: ";")
                    for index in 0 ..< selectedStates!.count {
                        selectedStates![index] = selectedStates![index].trimmed
                    }
                    for index in 0 ..< selectedCitys!.count {
                        selectedCitys![index] = selectedCitys![index].trimmed
                    }
                    for oneState in dataStateCity! {
                        if selectedStates!.contains(oneState.key) {
                            for oneCity in oneState.value as! [String] {
                                let isSelected = selectedCitys?.contains(oneCity)
                                filterData.append(CategoryModel(index: 0, title: oneCity, selected: isSelected!))
                            }
                        } else {}
                    }
                    
                    uiTableView.reloadData()
                }
            }
        }
        else if sender.tag == 4 {
            if showState == "state" {} else {
                showState = "state"
                uiLblPopupTilte.text = "Choose the State"
                uiTableView.isHidden = true
                uiCollectionHobby.isHidden = false
                dataCollection.removeAll()
                
                var selectedStates = txtState.text?.components(separatedBy: ";")
                for index in 0 ..< selectedStates!.count {
                    selectedStates![index] = selectedStates![index].trimmed
                }
                for oneState in dataStateCity! {
                    let isSelected = selectedStates?.contains(oneState.key)
                    dataCollection.append(HobbyModel(topic: oneState.key, state: isSelected!))
                }
                
                uiCollectionHobby.reloadData()
            }
        }
        else if sender.tag == 5 {
            if showState == "occupation" {} else {
                if txtIndustry.text! == "" {
                    messageBox(message: "Please choose the Industry.")
                    return
                } else {
                    showState = "occupation"
                    uiLblPopupTilte.text = "Choose the Occupation"
                    uiTableView.isHidden = false
                    uiCollectionHobby.isHidden = true

                    filterData.removeAll()
                    
                    let dataOccupation = loadJsonFile("occupation")
                    let selectedIndustry = txtIndustry.text!.trimmed
                    var selectedOccupation = txtOccurence.text?.components(separatedBy: ";")
                    for index in 0 ..< selectedOccupation!.count {
                        selectedOccupation![index] = selectedOccupation![index].trimmed
                    }
                    
                    if dataOccupation!.keys.contains(selectedIndustry) {
                        for oneOccupation in dataOccupation![selectedIndustry] as! [String] {
                            let isSelected = selectedOccupation?.contains(oneOccupation)
                            filterData.append(CategoryModel(index: 0, title: oneOccupation, selected: isSelected!))
                        }
                        uiTableView.reloadData()
                        
                    } else {
                        // does not contain key
                        messageBox(message: "There is no occupation on selected industry.")
                        return
                    }
                }
            }
        }
        uiPopupView.isHidden = false
    }
    
    @IBAction func onTapedCancel(_ sender: Any) {
        uiPopupView.isHidden = true
    }
    
    @IBAction func uiTapedSelect(_ sender: Any) {
        
        var tempTitle = ""
        var tempIndexs = ""
        
        if showState == "category" || showState == "city" || showState == "occupation" {
            
            for one in filterData {
                if one.selected  {
                    if tempTitle.isEmpty {
                        tempTitle = one.title
                        tempIndexs = "\(one.index)"
                    } else {
                        tempTitle = tempTitle + "; " + one.title
                        tempIndexs = tempIndexs  + ",\(one.index)"
                    }
                } else {}
            }
            
            if showState == "category" {
                selectedCategoryIndexs = tempIndexs
                txtCategory.text = tempTitle
                requestParam.category = tempTitle
                
            } else if showState == "occupation"  {
                txtOccurence.text = tempTitle
                requestParam.productOccupation = tempTitle
                
            } else if showState == "city"  {
                txtCity.text = tempTitle
                requestParam.productCity = tempTitle
            }
            
        }
        else if showState == "hobby" || showState == "state" {
            for index in 0 ..< dataCollection.count {
                if dataCollection[index].selectState  {
                    if tempTitle.isEmpty {
                        tempTitle = dataCollection[index].topic
                        tempIndexs = "\(index)"
                    } else {
                        tempTitle = tempTitle + "; " + dataCollection[index].topic
                        tempIndexs = tempIndexs  + ",\(index)"
                    }
                } else {}
            }
            
            if showState == "hobby" {
                selectedHobbyIndexs = tempIndexs
                txtHobby.text = tempTitle
                requestParam.productHobby = tempTitle
            } else if showState == "state" {
                txtState.text = tempTitle
                requestParam.productState = tempTitle
            }
            
        }
        else if showState == "age" {
            selectedAges.removeAll()
            
            var formAge = 100
            var toAge = 0
            
            
            for one in filterData {
                if one.selected  {
                    let item = one.title
                    selectedAges.append(item)
                    
                    let ageTemp : [String] = item.components(separatedBy: [" ", "-"])
                    let fromTemp = ageTemp.count > 3 ? ageTemp[0] : ageTemp[0].split(every: 2)[0]
                    let toTemp = ageTemp.count > 3 ? ageTemp[1] : "100"
                    
                    formAge = fromTemp.toInt()! > formAge ? formAge : fromTemp.toInt()!
                    toAge   = toTemp.toInt()! < toAge ? toAge : toTemp.toInt()!
                    
                    
                    if tempTitle.isEmpty {
                        tempTitle = fromTemp + "-" + toTemp
                    } else {
                        tempTitle = tempTitle + "; " + fromTemp + "-" + toTemp
                    }
                    
                } else {}
            }
            
            txtAge.text = tempTitle
            self.requestParam.productAgeFrom = "\(formAge)"
            self.requestParam.productAgeTo = "\(toAge)"
        }
        uiPopupView.isHidden = true
    }
    
    @IBAction func onTapedType(_ sender: Any) {
        dropDownType.show()
    }
    
    @IBAction func onTapedAge(_ sender: Any) {
//        dropDownAge.show()
        
        if showState == "age" {} else {
            uiLblPopupTilte.text = "Choose the Age"
            uiTableView.isHidden = false
            uiCollectionHobby.isHidden = true
            showState = "age"
            
            filterData.removeAll()
            
            for oneAge in ageData {
                
                let isSelected = selectedAges.contains(oneAge)
                filterData.append(CategoryModel(index: 0, title: oneAge, selected: isSelected))
            }
            
            uiTableView.reloadData()
        }
        
        uiPopupView.isHidden = false
    }
    
    @IBAction func onTapedGender(_ sender: Any) {
        dropDownGender.show()
    }
    
    @IBAction func btnPostTapped(_ sender: UIButton) {
        
        if productID.isEmpty && productImages.count == 0 {
            messageBox(message: "Please select image.")
            return
        }
        
        if txtSellingItem.text?.isEmpty == true {
            messageBox(message: "Please enter selling name.")
            return
        }
        
        if txtQuantiAvailable.text?.isEmpty == true {
            messageBox(message: "Please enter quantity available.")
            return
        }
        
        if txtQuantiPerUser.text?.isEmpty == true {
            messageBox(message: "Please enter quantity per user.")
            return
        }
        if txtQuantiPerUser.text!.toInt()! > (txtQuantiAvailable.text?.toInt())! {
            messageBox(message: "Please enter quantity available and quantity per user correctly.")
            return
        }
        
        
        if txtViewDescription.text!.characters.count <= 15 {
            messageBox(message: "Product description cannot be shorter than 15 characters.")
            return
        }
        
        if txtType.text?.isEmpty == true  {
            messageBox(message: "Please select Product Type.")
            return
        }
        
        /*
        if txtAge.text?.isEmpty == true {
            messageBox(message: "Please select age range")
            return
        }
        
        if txtGender.text?.isEmpty == true {
            messageBox(message: "Please enter gender")
            return
        }
        if txtHobby.text?.isEmpty == true {
            messageBox(message: "Please select hobby/activity")
            return
        }
        if txtIndustry.text?.isEmpty == true {
            messageBox(message: "Please select industry")
            return
        }
        if txtOccurence.text?.isEmpty == true {
            messageBox(message: "Please select occupation")
            return
        }*/
        if txtCategory.text?.isEmpty == true {
            messageBox(message: "Please select category")
            return
        }
        /*
        if txtZipcode.text?.isEmpty == true {
            messageBox(message: "Please enter zipcode")
            return
        }
        if txtCity.text?.isEmpty == true {
            messageBox(message: "Please enter city")
            return
        }
        if txtState.text?.isEmpty == true {
            messageBox(message: "Please enter state")
            return
        }
        if txtCountry.text?.isEmpty == true {
            messageBox(message: "Please enter country")
            return
        }
        */
        
        if locationCoordinate2D == nil {
            //locationManagerPopup()
//            return
            requestParam.latitude = "0"
            requestParam.longitude = "0"
        } else {
            requestParam.latitude = String(locationCoordinate2D!.latitude)
            requestParam.longitude = String(locationCoordinate2D!.longitude)
        }
        
        var request = "\(kAPIPostAdd)"
        if productID.isEmpty {
            requestParam.action = kAPIPostAdd
            requestParam.userId = String(userId)
        } else {
            request = "\(kAPIPostEdit)"
            requestParam.action = kAPIPostEdit
            requestParam.productId = productID
        }
        
        
        requestParam.productName = txtSellingItem.text
        requestParam.productDescription = txtViewDescription.text
        requestParam.productPrice = "0"
        requestParam.latitude = String(locationCoordinate2D!.latitude)
        requestParam.longitude = String(locationCoordinate2D!.longitude)
        requestParam.productQuantityAvailable = txtQuantiAvailable.text
        requestParam.productQuantityPerUser   = txtQuantiPerUser.text
        
        requestParam.productGender  = txtGender.text
        requestParam.zipCode = txtZipcode.text
        requestParam.productCity    = txtCity.text
        requestParam.productState   = txtState.text
        requestParam.productCountry = "United State"     //txtCountry.text
        
        RequestManager.singleton.requestMultipartPOST(requestMethod: request, parameter: requestParam.toDictionary(), showLoader: true, name: "product_image[]", images: productImages, successBlock: { (resposne :MessageModal, message : String?) in
            
            self.delegate?.refreshProductList()
            
            if message != nil {
                messageBox(message: message)
            }
            
            let controller = NavigationManager.singleton.screen(screenType: NavigationScreenType.NavigationScreenTypeMyPostList) as! MyPostListVC
            controller.isMenuScreen = true
            self.navigationController?.pushViewController(controller, animated: true)
            
        }, failureBlock: {(error : Error?) in })
    }

    //MARK:- PostCellDelegate
    func btnRemoveTaped(cell: PostCell) {
        
        let indexPath = collectionImages.indexPath(for: cell)
        productImages.remove(at: (indexPath?.row)!)
        collectionImages.reloadData()
    }
    
    //MARK:- PostReusableViewDelegate
    func btnCameraTaped(sender: UIButton) {
        
        let imagePickerController = ImagePickerController()
        imagePickerController.delegate = self
        imagePickerController.imageLimit = 6
        imagePickerController.modalPresentationStyle = .fullScreen
        present(imagePickerController, animated: true, completion: nil)
    }
    
    //MARK:- Notification
    @objc func profileTapped(notification : Notification) {
        
        let controller = NavigationManager.singleton.screen(screenType: NavigationScreenType.NavigationScreenTypeProfile) as! ProfileVC
        navigationController?.pushViewController(controller, animated: true)
    }
    
    //MARK:- MapVCDelegate
    func annotationDidChange(locationCoordinate2D: CLLocationCoordinate2D) {
        
        isLocationDidChange = true
        self.locationCoordinate2D = locationCoordinate2D
        mapProduct.removeAnnotations(self.mapProduct.annotations)
        addAnnotation(title: "Current location".localized(), coordinate: locationCoordinate2D)
    }
        
    //MARK:- UIViewController
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "segueMapVC" {
            
            let controller = segue.destination as! MapVC
            controller.productName = txtSellingItem.text
            controller.locationCoordinate2D = locationCoordinate2D
            controller.delegate = self
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureUI()
        
        for i in 0 ..< categorys.count {
            filterData.append(CategoryModel(index: i, title: categorys[i], selected: false))
        }
        uiTableView.delegate = self
        uiTableView.dataSource = self
        uiTableView.tableFooterView = UIView()
        uiTableView.reloadData()
        
        MyNotificationCenter.addObserver(self, selector: #selector(configureUI), name: NSNotification.Name( LCLLanguageChangeNotification), object: nil)
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if isMenuScreen {
            self.setNavigationBarItem()
        } else {
            self.navigationController?.isNavigationBarHidden = false
        }
    }
    
}

//MARK:- CLLocationManagerDelegate
extension PostVC : CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        if (CLLocationCoordinate2DIsValid(locations[0].coordinate) && isLocationDidChange == false) {
           
            if locationCoordinate2D?.latitude != locations[0].coordinate.latitude && locationCoordinate2D?.longitude != locations[0].coordinate.longitude {
                
                mapProduct.removeAnnotations(mapProduct.annotations)
                locationCoordinate2D = locations[0].coordinate
                addAnnotation(title: "Current location".localized(), coordinate: locationCoordinate2D!)
            }
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        
    }
    
}


//MARK:- UIImagePickerControllerDelegate
extension PostVC : ImagePickerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        //let cropImage = info[UIImagePickerControllerOriginalImage] as? UIImage
        //let cropImage = cropToBounds(image: image!, width: 400, height: 400)
        //requestParam.productImage = cropImage
    }
    
    func wrapperDidPress(_ imagePicker: ImagePickerController, images: [UIImage]) {
        
    }
    
    func doneButtonDidPress(_ imagePicker: ImagePickerController, images: [UIImage]) {
        
        productImages = images
        collectionImages.reloadData()
        imagePicker.dismiss(animated: true, completion: {})
    }
    
    func cancelButtonDidPress(_ imagePicker: ImagePickerController) {
        imagePicker.dismiss(animated: true, completion: {})
    }
}

//MARK:- UITextFieldDelegate
extension PostVC : UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let text = (textField.text! as NSString).replacingCharacters(in: range, with: string)
        
        if text == "" {
            return true
        }
        
        if text.characters.count > 6 {
            return false
        }
        
        if Int(text) != nil {
            return true
        } else {
            return false
        }
    }
}

//MARK:- UICollectionView
extension PostVC : UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == collectionImages {}
        else {
//
//        if self.count >= 20 {
//            return
//        }
//
        dataCollection[indexPath.row].selectState = !dataCollection[indexPath.row].selectState

            self.uiCollectionHobby .reloadData()
        }
    }
}

extension PostVC : UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if collectionView == collectionImages {

            let width : CGFloat = 72
            let height : CGFloat = 72
            return CGSize(width: width, height: height)
        }
        
        let w = CGFloat(dataCollection[indexPath.row].topic.count * 9 + 15)
        return CGSize(width: w, height: 25)
    }
    
}

extension PostVC : UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == collectionImages {
            return productImages.count
        }
        
        return dataCollection.count
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        if collectionView == uiCollectionHobby {}
        else {
            switch kind {
                case UICollectionElementKindSectionHeader:
                    
                    let reusableView = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionElementKindSectionHeader, withReuseIdentifier: String.className(PostReusableView.self), for: indexPath) as! PostReusableView
                    reusableView.delegate = self
                    return reusableView
                    
                default:
                    return UICollectionReusableView()
            }
        }
        
        return UICollectionReusableView()
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == collectionImages {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String.className(PostCell.self), for: indexPath) as! PostCell
            cell.delegate = self
            cell.imgItem.image = productImages[indexPath.row]
            return cell;
        }
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PostHobbyCell", for: indexPath) as! HobbyCell
        cell.entity = dataCollection[indexPath.row]
        return cell
    }
}

//MARK:- UITableView
extension PostVC : UITableViewDelegate {

    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {

        if filterData[indexPath.row].selected {
            cell.accessoryType = .checkmark
        } else {
            cell.accessoryType = .none
        }
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        filterData[indexPath.row].selected = !filterData[indexPath.row].selected
        uiTableView.reloadData()
    }
}

extension PostVC : UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filterData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "CategoryCell") as! CategoryCell
        cell.entity = filterData[indexPath.row]
        
        return cell
    }
}
