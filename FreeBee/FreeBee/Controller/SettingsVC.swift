//
//  Settings.swift
//  HT
//
//  Created by Dharmesh on 02/12/16.
//  Copyright © 2016 dharmesh. All rights reserved.
//

import UIKit
import Foundation
import DropDown
import Localize_Swift

class SettingsCell : UITableViewCell {
    
}

class SettingsVC : BaseVC {
    
    @IBOutlet weak var tblSettings: UITableView!
    
    var items = ["Rate now", "iOS", "Become a beta tester", "Swich the language"]
    //let version = Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as! String
    var itemDetailss = ["", "12.0.0", "You will get update earlier and help us make FreeeBee more stable", ""] as [String]
    
    let dropDownLanguage = DropDown()
    var selectedItem : String!
    
    let availableLanguages = Localize.availableLanguages()
    
    //MARK:- Memory Management Method
    override func didReceiveMemoryWarning() {}
    
    deinit { //same like dealloc in ObjectiveC
        
        MyNotificationCenter.removeObserver(self)        
    }
    
    //MARK:- Custom Methods
    
    @objc func configureUI() {
        
        self.title = "Settings".localized()
        
        //dropDownLanguage.dataSource = [Localized(key:"English") , Localized(key:"Chinese"), Localized(key:"Japanese")]
        var displayLanguage : [String] = []
        for language in availableLanguages {
            displayLanguage.append(Localize.displayNameForLanguage(language).localized())
        }
        dropDownLanguage.dataSource = displayLanguage
        
        selectedItem = Localize.displayNameForLanguage(Localize.currentLanguage())
        
        dropDownLanguage.backgroundColor = UIColor.white
        dropDownLanguage.textColor = UIColor.black
        dropDownLanguage.separatorColor = kColorGray
        tblSettings.reloadData()
    }
    
    //------------------------------------------------------
    
    //MARK: Notification
    
    @objc func profileTapped(notification : Notification) {
        
        let controller = NavigationManager.singleton.screen(screenType: NavigationScreenType.NavigationScreenTypeProfile) as! ProfileVC
        navigationController?.pushViewController(controller, animated: true)
    }

    //------------------------------------------------------
    
    //MARK: UIViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        MyNotificationCenter.addObserver(self, selector: #selector(configureUI), name: NSNotification.Name( LCLLanguageChangeNotification), object: nil)
        MyNotificationCenter.addObserver(self, selector: #selector(profileTapped), name: NSNotification.Name(rawValue: kNotificationSettingsVC), object: nil)
        
        configureUI()
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setNavigationBarItem()
    }
    
    //------------------------------------------------------
}

extension SettingsVC : UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.row == items.count - 1 {
            dropDownLanguage.show()
        }
    }
}

extension SettingsVC : UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: String.className(SettingsCell.self)) as! SettingsCell
        cell.selectionStyle = .none
       
        cell.textLabel?.textColor = kColorGray
        cell.detailTextLabel?.textColor = kColorGray
        cell.imageView?.image = #imageLiteral(resourceName: "abc_ic_star_black_16dp")
        
        if indexPath.row == items.count - 1 {
            
            cell.textLabel?.text = selectedItem
            LocalizedControl(view: cell.textLabel!)
            cell.detailTextLabel?.text = ""
            
            dropDownLanguage.anchorView = cell
            dropDownLanguage.selectionAction = { [unowned self] (index, item) in
                
                self.selectedItem = item
                cell.textLabel?.text = item
                LocalizedControl(view: cell.textLabel!)
                cell.detailTextLabel?.text = ""
                
                Localize.setCurrentLanguage(self.availableLanguages[index])
            }
            
        } else {
            
            cell.textLabel?.text = items[indexPath.row]
            LocalizedControl(view: cell.textLabel!)
            cell.detailTextLabel?.text = itemDetailss[indexPath.row]
            LocalizedControl(view: cell.detailTextLabel!)
        }
        
        return cell
    }
}
