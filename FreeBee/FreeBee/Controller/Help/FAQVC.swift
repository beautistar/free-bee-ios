//
//  FAQVC.swift
//  FreeBee
//
//  Created by RMS on 11/25/19.
//  Copyright © 2019 dharmesh. All rights reserved.
//

import UIKit


class FAQVC: BaseVC {

    @IBOutlet weak var uiTableView: UITableView!
    
    var openState = [Bool]()
        
    //MARK:- UIView Life Cycle Method
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "FAQ".localized()
        
        uiTableView.tableFooterView = UIView()
        uiTableView.dataSource = self
        uiTableView.delegate = self
        
        
        for _ in 0 ..< qData.count {
            openState.append(false)
        }
        uiTableView.reloadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
}

extension FAQVC : UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return qData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "FCell") as! FCell
        cell.uiNumber.text = "\(indexPath.row + 1)."
        cell.uiQLabel.text = qData[indexPath.row]
        
        if openState[indexPath.row] {
            cell.uiAlabel.isHidden = false
            cell.uiAlabel.text = answerData[indexPath.row]
            cell.answerTopConstrain.constant = 12
        } else {
            cell.uiAlabel.isHidden = true
            cell.uiAlabel.text = nil
            cell.answerTopConstrain.constant = 0
            
        }
        
        return cell
    }
}

extension FAQVC : UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        for index in 0 ..< openState.count {
            openState[index] = false
        }
        openState[indexPath.row] = true
        self.uiTableView.reloadData()
    }
}

class FCell : UITableViewCell {
    @IBOutlet weak var uiNumber: UILabel!
    @IBOutlet weak var uiQLabel: UILabel!
    @IBOutlet weak var uiAlabel: UILabel!
    @IBOutlet weak var answerTopConstrain: NSLayoutConstraint!    
}
