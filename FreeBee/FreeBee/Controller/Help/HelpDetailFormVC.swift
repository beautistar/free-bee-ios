//
//  HelpDetailFormVC.swift
//  HT
//
//  Created by Dharmesh on 05/12/16.
//  Copyright © 2016 dharmesh. All rights reserved.
//

import Foundation
import UIKit

class HelpDetailFormVC : UIViewController {
        
    //MARK:- Outletsz
    
    @IBOutlet weak var txtReason: UITextField!
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var tvDiscription: UITextView!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var lblHowCanWeHelp: UILabel!
    @IBOutlet weak var lblHTUAE: UILabel!
    @IBOutlet weak var lblFaser: UILabel!
    @IBOutlet weak var lblSendUsContact: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblTellUsHappened: UILabel!
    @IBOutlet weak var lblYourEmailAddress: UILabel!
    @IBOutlet weak var lblMandatoryFields: UILabel!
    @IBOutlet weak var btnSubmit: UIButton!
    @IBOutlet weak var btnPopuarArticles: UIButton!
    @IBOutlet weak var lblNotAllowedHT: UILabel!
    @IBOutlet weak var lblTipsForSafeTransactions: UILabel!
    @IBOutlet weak var lblDetectFraud: UILabel!
    @IBOutlet weak var lblPayment: UILabel!
    @IBOutlet weak var lblTearmsAndC: UILabel!
    @IBOutlet weak var lblFAQ: UILabel!
    @IBOutlet weak var lblSafetyTips: UILabel!
    @IBOutlet weak var btnCopycight: UIButton!
    
    //MARK:- variables
        
    //MARK: Memory Management Method
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    deinit {}
        
    //MARK:- Custom Methods
    
    func configureUI() {
        
        self.navigationItem.title = "Send us and email".localized()
        
        LocalizedControl(view: txtReason)
        LocalizedControl(view: txtName)
        LocalizedControl(view: tvDiscription)
        LocalizedControl(view: txtEmail)
        LocalizedControl(view: lblHowCanWeHelp, key : "How can we help you?")
        LocalizedControl(view: lblHTUAE, key : "HT UAE > Submit a request")
        LocalizedControl(view: lblFaser, key : "How us help you faster!")
        LocalizedControl(view: lblSendUsContact, key : "Please, tell us the reason of your contact*")
        LocalizedControl(view: lblName, key : "Name*")
        LocalizedControl(view: lblTellUsHappened, key : "Tell us what happend*")
        LocalizedControl(view: lblYourEmailAddress, key : "Your email address*")
        LocalizedControl(view: lblMandatoryFields, key : "*Mandatory fields")
        LocalizedControl(view: btnSubmit, key : "Submit")
        LocalizedControl(view: btnPopuarArticles, key : "Popular Articles")
        LocalizedControl(view: lblNotAllowedHT, key : "Items not allowed in HT")
        LocalizedControl(view: lblTipsForSafeTransactions, key : "Tips for safe transaction - Meet with other part")
        LocalizedControl(view: lblDetectFraud, key : "How to detect fraud?")
        LocalizedControl(view: lblPayment, key : "Payment")
        LocalizedControl(view: lblTearmsAndC, key : "Terms and Conditions")
        LocalizedControl(view: lblFAQ, key : "FAQs")
        LocalizedControl(view: lblSafetyTips, key : "Safety tips")
        LocalizedControl(view: btnCopycight, key : "Ht - Copyright @ 2016")
        
        txtReason.layer.borderColor = UIColor(red: 184/255, green: 184/255, blue: 184/255, alpha: 0.5 ).cgColor
        txtReason.layer.borderWidth = 1
        
        txtName.layer.borderColor = UIColor(red: 184/255, green: 184/255, blue: 184/255, alpha: 0.5 ).cgColor
        txtName.layer.borderWidth = 1
        
        tvDiscription.layer.borderColor = UIColor(red: 184/255, green: 184/255, blue: 184/255, alpha: 0.5 ).cgColor
        tvDiscription.layer.borderWidth = 1
        
        txtEmail.layer.borderColor = UIColor(red: 184/255, green: 184/255, blue: 184/255, alpha: 0.5 ).cgColor
        txtEmail.layer.borderWidth = 1
        
    }
    
    //MARK:- Action Methods
        
    //MARK:- UIView Life Cycle Method
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
}
