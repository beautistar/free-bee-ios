//
//  TermsConditionVC.swift
//  HT
//
//  Created by Dharmesh on 05/02/17.
//  Copyright © 2017 dharmesh. All rights reserved.
//

import UIKit
import Foundation

class TermsConditionVC : BaseVC {
    
    //MARK: Memory Management Method
    
    override func didReceiveMemoryWarning() {}
    
    
    deinit { //same like dealloc in ObjectiveC
        
    }
    
    //MARK:- UIView Life Cycle Method
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Terms and Conditions".localized()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }

}
