//
//  CompanyVC.swift
//  FreeBee
//
//  Created by Developer on 11/11/19.
//  Copyright © 2019 dharmesh. All rights reserved.
//

import UIKit
import Foundation

//import CHIPageControl

class CompanyVC: BaseVC {

    var imgs = ["man1", "man2", "man3"]
    var lables = [
        "Companies list their branded goods\non the Free-Bee marketplace and\nship them to our warehouse",
        "Users must fill out their profiles and\nthe questionnaire so they can receive \nthe products they want",
        "Users must build a boundle of 5\npremium items and 10 regular items.\nEVERYTHING IS FREE",
    ]
    
    internal let numberOfPages = 3
    var progress = 0.0
    var introData = [IntroModel]()

    @IBOutlet weak var ui_collectionView: UICollectionView!
    @IBOutlet weak var pageControl: UIPageControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        pageControl.hidesForSinglePage = true
        pageControl?.numberOfPages = 3
        
        for i in 0 ..< 3 {
            introData.append(IntroModel(index: i, image: imgs[i], title: lables[i]))
        }
        ui_collectionView.reloadData()
    }
    
    @IBAction func onTapedSkip(_ sender: Any) {
        NavigationManager.singleton.setupLogin()
    }
    
    func showSkipButton() {
        if progress < 2.0 {
//            ui_skipBut.isHidden = false
        } else {
//            ui_skipBut.isHidden = true
        }
    }
}


extension CompanyVC : UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return introData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "IntroCell", for: indexPath) as! IntroCell
        cell.entity = introData[indexPath.row]
        
        cell.titleTxt.width = collectionView.frame.width - 100
        return cell
    }
}

extension CompanyVC : UICollectionViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let total = scrollView.contentSize.width - scrollView.bounds.width
        let offset = scrollView.contentOffset.x
        let percent = Double(offset / total)
        progress = percent * Double(self.numberOfPages - 1)
        
        self.pageControl.currentPage = Int(progress)
        
        showSkipButton()
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        self.pageControl.currentPage = indexPath.section
    }
}

extension CompanyVC: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: ui_collectionView.bounds.width, height: collectionView.frame.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        
        return 0
    }
}

class IntroModel {
    var index = 0
    var image = ""
    var title = ""
    
    init(index: Int, image : String, title : String ) {
        self.index = index
        self.image = image
        self.title = title
    }
}


class IntroCell: UICollectionViewCell {
    
    @IBOutlet weak var mainImg: UIImageView!
    @IBOutlet weak var titleTxt: UILabel!
    @IBOutlet weak var NextBut: UIButton!
    
    var entity : IntroModel! {
        
        didSet{
            mainImg.image = UIImage(named: entity.image)
            titleTxt.text = entity.title
            
            if entity.index == 2 {
                NextBut.isHidden = false
            } else {
                NextBut.isHidden = true
            }
        }
    }
}
