//
//  SignupVC.swift
//  HT
//
//  Created by Dharmesh on 05/02/17.
//  Copyright © 2017 dharmesh. All rights reserved.
//

import UIKit
import Foundation
import ImagePicker
import Photos
import DropDown

import MessageUI

import SwiftyJSON
import FacebookCore
import FBSDKLoginKit
import Firebase
import GoogleSignIn
import FirebaseAuth

class SignupVC : BaseVC, UINavigationControllerDelegate {
    
    var selectedProfile = false
    let imagePicker = UIImagePickerController()
    
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var viewFullName: UIView!
    @IBOutlet weak var txtFullname: UITextField!
    
    @IBOutlet weak var viewEmail: UIView!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtCall: UITextField!
    @IBOutlet weak var viewPassword: UIView!
    @IBOutlet weak var txtPassword: UITextField!
    
    @IBOutlet weak var txtAddr: UITextField!
    @IBOutlet weak var txtCity: UITextField!
    @IBOutlet weak var btnCity: UIButton!
    @IBOutlet weak var txtState: UITextField!
    @IBOutlet weak var btnState: UIButton!
    @IBOutlet weak var txtZipcode: UITextField!
    @IBOutlet weak var txtAge: UITextField!
    @IBOutlet weak var txtGender: UITextField!
    @IBOutlet weak var butGender: UIButton!
    
    @IBOutlet weak var btnSignup: UIButton!
    
    @IBOutlet weak var popupVIew: UIView!
    @IBOutlet weak var popupTitle: UILabel!
    @IBOutlet weak var popupCollectionView: UICollectionView!
    @IBOutlet weak var uiTableView: UITableView!
    
    
    var dataSource = [HobbyModel]()
    let dropDownGender = DropDown()
    var popupOpenState = ""
    var tableData = [CategoryModel]()
    
    let requestParam = RequestParamModal()
    
    //MARK:- Memory Management Method
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    deinit {}
    
    //MARK:- Custom Methods
    
    func configureUI() {
        
        imagePicker.delegate = self
        
        LocalizedControl(view: txtEmail, key: "Email")
        LocalizedControl(view: txtPassword, key: "Password")
        LocalizedControl(view: txtFullname, key: "Full name")
        
        //MARK:- gender data set
        dropDownGender .dataSource = genders
        dropDownGender.anchorView = butGender
        dropDownGender.selectionAction = { [unowned self] (index, item) in
            self.txtGender.text = item
            self.requestParam.productGender = item
        }
        dropDownGender.backgroundColor = UIColor.white
        dropDownGender.textColor = UIColor.black
        dropDownGender.separatorColor = kColorGray

    }
    
    func IsValidInputs() -> Bool {
        
        if selectedProfile == false {
            messageBox(message: "Please select profile")
            return false
        }
        
        if txtFullname.text?.isEmpty == true {
            messageBox(message: "Please enter fullname")
            return false
        }
        if ValidationManager.singleton.isValidFullName(string: txtFullname.text!) == false {
            messageBox(message: "Please enter valid fullname")
            return false
        }
        if txtEmail.text?.isEmpty == true {
            messageBox(message: "Please enter an email")
            return false
        }
        if ValidationManager.singleton.isValidEmail(email: txtEmail.text!) == false {
            messageBox(message: "Please enter valid email")
            return false
        }
        
        if txtCall.text?.isEmpty == true {
            messageBox(message: "Please enter phone number")
            return false
        }
//        if ValidationManager.singleton.isValidPhone(phone: txtCall.text!) == false {
//            messageBox(message: "Please enter valid phone number")
//            return false
//        }
        if txtPassword.text?.isEmpty == true {
            messageBox(message: "Please enter password")
            return false
        }
        
        if ValidationManager.singleton.isValidPassword(password: txtPassword.text!) == false {
            messageBox(message: "Please enter valid password")
            return false
        }
        if txtAddr.text?.isEmpty == true {
            messageBox(message: "Please enter address")
            return false
        }
        if txtCity.text?.isEmpty == true {
            messageBox(message: "Please enter city")
            return false
        }
        if txtState.text?.isEmpty == true {
            messageBox(message: "Please enter state")
            return false
        }
        if txtZipcode.text?.isEmpty == true {
            messageBox(message: "Please enter zipcode")
            return false
        }
        if txtAge.text?.isEmpty == true {
            messageBox(message: "Please enter age")
            return false
        }
        if txtGender.text?.isEmpty == true {
            messageBox(message: "Please select gender")
            return false
        }
        

        
//        var phoneNumber : String!
//
//        do {
//            if (txtCall.text?.hasPrefix("+"))! == false {
//                phoneNumber = "+"+txtCall.text!
//            } else {
//                phoneNumber = txtCall.text!
//            }
//            _ = try phoneNumberKit.parse(phoneNumber)
//        }
//        catch {
//            messageBox(message: "Please enter valid phone")
//            return false
//        }
//
        return true
    }
    
    func gotoVC() {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let nextVC = storyBoard.instantiateViewController(withIdentifier: "FewQuestion1VC") as! FewQuestion1VC
        nextVC.modalPresentationStyle = .fullScreen
        self.present(nextVC, animated: false, completion: nil)
    }
    
    func requestForSignup() {
        
        let request = "\(kAPIRegister)"
        
        let requestParam = RequestParamModal()
        requestParam.action = kAPIRegister
        requestParam.email = txtEmail.text
        requestParam.password = txtPassword.text
        requestParam.username = txtFullname.text
        requestParam.phone = txtCall.text
        requestParam.address = txtAddr.text
        requestParam.city = txtCity.text
        requestParam.state = txtState.text
        requestParam.profileZipcode = txtZipcode.text
        requestParam.age = txtAge.text
        requestParam.gender = txtGender.text
        
        RequestManager.singleton.requestMultipartPOST(requestMethod: request, parameter: requestParam.toDictionary(), showLoader: true, name: "photo", images: [imgProfile.image!], successBlock: { (resposne :MessageModal, message : String?) in
            
            if resposne.resultCode == 0 {
                MyUserDefaults.removeObject(forKey: kCurrentUser);
                MyUserDefaults.set(Int(resposne.userProfile.userid), forKey: kCurrentUserId)
                MyUserDefaults.synchronize()
                self.gotoVC()
                
            } else {
                messageBox(message: resposne.error)
            }
        }, failureBlock: {(error : Error?) in })
    }
    
    func requestForLogin() {
        
        let request = "\(kAPILogin)"
        
        let requestParam = RequestParamModal()
        requestParam.action = kAPILogin
        requestParam.email = txtEmail.text
        requestParam.password = txtPassword.text
        
        RequestManager.singleton.requestPOST(requestMethod: request, parameter: requestParam.toDictionary(), showLoader: true, successBlock: { (resposne :MessageModal, message : String?) in
            
            if resposne.resultCode == 0 {
                
                MyUserDefaults.removeObject(forKey: kCurrentUser);
                let rUserId = resposne.userProfile.userid
                let userModal = UserModal(fromDictionary: [:])
                userModal.userid =  resposne.userProfile.userid
                userModal.userimage =  resposne.userProfile.userimage
                userModal.username = resposne.userProfile.username
                
                let userData = NSKeyedArchiver.archivedData(withRootObject: userModal.toDictionary())
                MyUserDefaults.setValue(userData, forKey: kCurrentUser)
                
                MyUserDefaults.set(Int(rUserId!), forKey: kCurrentUserId)
                MyUserDefaults.synchronize()
                NavigationManager.singleton.setupHome()
                
            } else {
                messageBox(message: resposne.error)
            }
            
        }, failureBlock: {(error : Error?) in
            
        })
    }
    
    func getFaceBookUserData() {
            
        let connection = GraphRequestConnection()
        connection.add(GraphRequest(graphPath: "/me", parameters: ["fields": "id, name, email, first_name, last_name"])) { (httpResponse, result, error) in

            let jsonResult = JSON(result)
            let fname = jsonResult["first_name"].stringValue
            let lname = jsonResult["last_name"].stringValue
            let email = jsonResult["email"].stringValue
            
            let socialId = jsonResult["id"].intValue
            let photoUrlString = "http://graph.facebook.com/\(socialId)/picture?type=large"
            
            self.requestSingupSocial(socialId: "\(socialId)", username: fname + " " + lname, email: email, photoUrl: photoUrlString, socialType: "facebook")
        }

        connection.start()
    }
    
    //MARK:- Action Methods
    @IBAction func btnSelectProfile(_ sender: Any) {
        
        let actionsheet = UIAlertController(title: kAppName, message: "Select Photo".localized(), preferredStyle: UIAlertControllerStyle.actionSheet)
        
        let actionGallery = UIAlertAction(title: kGallery, style: UIAlertActionStyle.default, handler: {( action : UIAlertAction) in
            
            if (UIImagePickerController.isSourceTypeAvailable(.photoLibrary)) {
                self.imagePicker.sourceType = .photoLibrary
                self.present(self.imagePicker, animated: true, completion: {
                })
            } else {
                messageBox(message: "Photo gallery unsupported")
            }
        })
        actionsheet.addAction(actionGallery)
        
        let actionCamera = UIAlertAction(title: kCamera, style: UIAlertActionStyle.default, handler: {( action : UIAlertAction) in
            
            if (UIImagePickerController.isSourceTypeAvailable(.camera)) {
                self.imagePicker.sourceType = .camera
                self.present(self.imagePicker, animated: true, completion: {
                })
            } else {
                messageBox(message: "Camera unsupported")
            }
        })
        actionsheet.addAction(actionCamera)
        
        let actionCanel = UIAlertAction(title: kCancel, style: UIAlertActionStyle.cancel, handler: {( action : UIAlertAction) in
        })
        actionsheet.addAction(actionCanel)
        present(actionsheet, animated: true, completion: nil)
    }
    
    @IBAction func btnSignupTapped(_ sender: UIButton) {
        
        if IsValidInputs() == false {
            return
        }
        
        requestForSignup()
    }
    
    @IBAction func btnBackTapped(_ sender: UIButton) {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let nextVC = storyBoard.instantiateViewController(withIdentifier: "LoginVC")
        nextVC.modalPresentationStyle = .fullScreen
        self.present(nextVC, animated: false, completion: nil)
    }
    
    @IBAction func onTapedFacebook(_ sender: Any) {
        
        let loginManager = LoginManager()
        
        loginManager.logIn(permissions: ["public_profile", "email"], from: self) { [weak self](result, error) in
            
            if error != nil {
                print("Login failed")
            } else {
                if result!.isCancelled { print("login canceled") }
                else { self!.getFaceBookUserData() }
            }
        }
    }
    
    @IBAction func onTapedGoogle(_ sender: Any) {
        
        GIDSignIn.sharedInstance()?.presentingViewController = self
        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance()?.signIn()
    }
    
    @IBAction func onTapedCity(_ sender: Any) {
        
        if txtState.text! == "" {
            messageBox(message: "Please choose the State.")
            return
        }
        
        if popupOpenState == "city" {} else {
            popupOpenState = "city"
            popupTitle.text = "Choose the City"

            uiTableView.isHidden = false
            popupCollectionView.isHidden = true
            tableData.removeAll()
            
            var selectedStates = txtState.text?.components(separatedBy: ";")
            var selectedCitys = txtCity.text?.components(separatedBy: ";")
            
            for index in 0 ..< selectedStates!.count {
                selectedStates![index] = selectedStates![index].trimmed
            }
            for index in 0 ..< selectedCitys!.count {
                selectedCitys![index] = selectedCitys![index].trimmed
            }
            for oneState in dataStateCity! {
                if selectedStates!.contains(oneState.key) {
                    for oneCity in oneState.value as! [String] {
                        let isSelected = selectedCitys?.contains(oneCity)
                        tableData.append(CategoryModel(index: 0, title: oneCity, selected: isSelected!))
                    }
                } else {}
            }
            
            uiTableView.reloadData()
        }
        popupVIew.isHidden = false
    }
    
    @IBAction func onTapedState(_ sender: Any) {
        
        if popupOpenState == "state" {} else {
            popupOpenState = "state"
            popupTitle.text = "Choose the State"
            uiTableView.isHidden = false
            popupCollectionView.isHidden = true
            tableData.removeAll()
            
            let selectedStates = txtState.text?.components(separatedBy: ";")
            for oneState in dataStateCity! {
                let isSelected = selectedStates?.contains(oneState.key)
                tableData.append(CategoryModel(index: 0, title: oneState.key, selected: isSelected!))
            }
            uiTableView.reloadData()
        }
        popupVIew.isHidden = false
    }
    
    @IBAction func onTapedGender(_ sender: Any) {
        dropDownGender.show()
    }
    
    @IBAction func onTapedCancel(_ sender: Any) {
        popupVIew.isHidden = true
    }
    
    @IBAction func onTapedSelect(_ sender: Any) {
        var str = ""
        for one in tableData {
            if one.selected {
                if str == "" {str=one.title}
                else { str += ";" + one.title }
            }
        }
        if popupOpenState == "state" {
            txtState.text = str
        }
        else {
            txtCity.text = str
        }
        /*
        if popupOpenState == "state" {
            for one in dataSource {
                if one.selectState {
                    if str == "" {str=one.topic}
                    else { str += ";" + one.topic }
                }
            }
            if txtState.text != str {txtCity.text = ""}
            txtState.text = str
        } else {
            for one in tableData {
                if one.selected {
                    if str == "" {str=one.title}
                    else { str += ";" + one.title }
                }
            }
            txtCity.text = str
        }*/
        
        popupVIew.isHidden = true
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureUI()
        popupCollectionView.delegate = self
        popupCollectionView.dataSource = self
        
        uiTableView.delegate = self
        uiTableView.dataSource = self
        uiTableView.tableFooterView = UIView()
        uiTableView.reloadData()
        
    }
        
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    func requestSingupSocial(socialId: String, username: String, email: String, photoUrl: String, socialType: String) {
        
        let request = "\(kAPIRegisterSocialInit)"
        let requestParam = RequestParamModal()
        requestParam.action = kAPIRegisterSocialInit
        requestParam.username = username
        requestParam.email = email
        requestParam.socialId = socialId
        requestParam.photoUrl = photoUrl
        requestParam.socialType = socialType
        
        RequestManager.singleton.requestPOST(requestMethod: request, parameter: requestParam.toDictionary(), showLoader: true, successBlock: { (resposne :MessageModal, message : String?) in
            
            if resposne.resultCode == 0 {
                
                let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let nextVC = storyBoard.instantiateViewController(withIdentifier: "SinupSocailInfoVC") as! SinupSocailInfoVC
                nextVC.userID = resposne.userProfile.userid
                nextVC.modalPresentationStyle = .fullScreen
                self.present(nextVC, animated: false, completion: nil)
            } else {
                messageBox(message: resposne.error)
            }
        }, failureBlock: {(error : Error?) in })
    }
    
}

//MARK:- UIImagePickerControllerDelegate
extension SignupVC : UIImagePickerControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        let cropImage = info[UIImagePickerControllerOriginalImage] as? UIImage
        imgProfile.image = cropImage
        selectedProfile = true
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
}

//MARK:- UICollectionView
extension SignupVC : UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        dataSource[indexPath.row].selectState = !dataSource[indexPath.row].selectState
        self.popupCollectionView.reloadData()

    }
}

extension SignupVC : UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let w = CGFloat(dataSource[indexPath.row].topic.count * 9 + 15)
        return CGSize(width: w, height: 25)
    }
    
}

extension SignupVC : UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return dataSource.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PostHobbyCell", for: indexPath) as! HobbyCell
        cell.entity = dataSource[indexPath.row]
        return cell
    }
}

//MARK:- UITableView
extension SignupVC : UITableViewDelegate {

    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {

        if tableData[indexPath.row].selected {
            cell.accessoryType = .checkmark
        } else {
            cell.accessoryType = .none
        }
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        tableData[indexPath.row].selected = !tableData[indexPath.row].selected
        uiTableView.reloadData()
    }
}

extension SignupVC : UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "CategoryCell") as! CategoryCell
        cell.entity = tableData[indexPath.row]
        
        return cell
    }
}

//MARK:- GIDSignInDelegate
extension SignupVC : GIDSignInDelegate {
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        
        if error == nil {
            
            guard let user = user else {return}
            let socialId = user.userID!                  // For client-side use only!
             // Safe to send to the server
            let fullName = user.profile.name!
            let email = user.profile.email!
            let photoUrlString = user.profile.imageURL(withDimension: 300)!.description
            
            self.requestSingupSocial(socialId: socialId, username: fullName, email: email, photoUrl: photoUrlString, socialType: "google")
        } else {
            print("GIDSignIn:--  ", error.localizedDescription)
        }
    }
    
    func sign(_ signIn: GIDSignIn!, present viewController: UIViewController!) {
        viewController.modalPresentationStyle = .fullScreen
        present(viewController, animated: true) {}
    }
   
}

