//
//  LoginVC.swift
//  HT
//
//  Created by Dharmesh on 04/02/17.
//  Copyright © 2017 dharmesh. All rights reserved.
//

import UIKit
import Foundation

var appStart = false

class LoginVC : BaseVC {
    
    @IBOutlet weak var viewEmail: UIView!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var viewPassword: UIView!
    @IBOutlet weak var txtPassword: UITextField!
   // @IBOutlet weak var viewFullName: UIView!
    //@IBOutlet weak var txtFullname: UITextField!
    @IBOutlet weak var btnSignup: UIButton!
    
    @IBOutlet weak var btnNewUser: UIButton!
    
    //MARK:- Memory Management Method
    override func didReceiveMemoryWarning() {}
    
    deinit { }
    
    //MARK:- Custom Methods
    func configureUI() {
        
        LocalizedControl(view: txtEmail, key: "Email")
        LocalizedControl(view: txtPassword, key: "Password")
        LocalizedControl(view: btnSignup, key: "Sign In")
        
        let attrs = [
            NSAttributedStringKey.font : UIFont.systemFont(ofSize: 19.0),
            NSAttributedStringKey.foregroundColor : UIColor.darkGray,
            NSAttributedStringKey.underlineStyle : 1
        ] as [NSAttributedStringKey : Any] 
        
        let attributedString = NSMutableAttributedString(string:"")
        
        let buttonTitleStr = NSMutableAttributedString(string:"New user? Sign up", attributes:attrs)
        attributedString.append(buttonTitleStr)
        
    }
    
    func IsValidInputs() -> Bool {
        
        if txtEmail.text?.isEmpty == true {
            messageBox(message: "Please enter an email")
            return false
        }
        
        if ValidationManager.singleton.isValidEmail(email: txtEmail.text!) == false {
            messageBox(message: "Please enter valid email")
            return false
        }
        
        if txtPassword.text?.isEmpty == true {
            messageBox(message: "Please enter password")
            return false
        }
        
        if ValidationManager.singleton.isValidPassword(password: txtPassword.text!) == false {
            messageBox(message: "Please enter valid password")
            return false
        }
        
        return true
    }
    
    @IBAction func onTapedLogin(_ sender: Any) {
        
        if IsValidInputs() == false {
            return
        }
        requestForLogin()
    }
    
    @IBAction func onTapSignup(_ sender: Any) {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let nextVC = storyBoard.instantiateViewController(withIdentifier: "SignupVC") as! SignupVC
        nextVC.modalPresentationStyle = .fullScreen
        self.present(nextVC, animated: false, completion: nil)
    }
    
    func requestForSignup() {
        
        let request = "\(kAPIRegister)"
        
        let requestParam = RequestParamModal()
        requestParam.action = kAPIRegister
        requestParam.email = txtEmail.text
        requestParam.password = txtPassword.text
        
        RequestManager.singleton.requestPOST(requestMethod: request, parameter: requestParam.toDictionary(), showLoader: true, successBlock: { (resposne :MessageModal, message : String?) in
            
            if resposne.resultCode == 0 {
                
                MyUserDefaults.removeObject(forKey: kCurrentUser);
                MyUserDefaults.set(Int(resposne.id), forKey: kCurrentUserId)
                MyUserDefaults.synchronize()
                NavigationManager.singleton.setupHome()
                
            } else {
                messageBox(message: resposne.error)
            }
        }, failureBlock: {(error : Error?) in
            
        })
    }
    
    func requestForLogin() {
        
        let request = "\(kAPILogin)"
        
        let requestParam = RequestParamModal()
        requestParam.action = kAPILogin
        requestParam.email = txtEmail.text
        requestParam.password = txtPassword.text
        
        RequestManager.singleton.requestPOST(requestMethod: request, parameter: requestParam.toDictionary(), showLoader: true, successBlock: { (resposne :MessageModal, message : String?) in
            
            if resposne.resultCode == 0 {
                
                MyUserDefaults.removeObject(forKey: kCurrentUser);
                let rUserId = resposne.userProfile.userid
                
                let userModal = UserModal(fromDictionary: [:])
                userModal.userid =  resposne.userProfile.userid
                userModal.userimage =  resposne.userProfile.userimage
                userModal.username = resposne.userProfile.username
                userModal.userAge = resposne.userProfile.userAge
                userModal.usergender = resposne.userProfile.usergender
                
                let userData = NSKeyedArchiver.archivedData(withRootObject: userModal.toDictionary())
                MyUserDefaults.setValue(userData, forKey: kCurrentUser)
                
                MyUserDefaults.set(Int(rUserId!), forKey: kCurrentUserId)
                MyUserDefaults.set(self.txtEmail.text, forKey: kCurrentUserEmail)
                MyUserDefaults.set(self.txtPassword.text, forKey: kCurrentPassword)
                MyUserDefaults.synchronize()
                NavigationManager.singleton.setupHome()
                appStart = true
                
            } else {
                
                messageBox(message: resposne.error)
            }
            
        }, failureBlock: {(error : Error?) in })
    }
    
    //MARK:- Action Methods
    
    @IBAction func btnHelpTapped(_ sender: UIButton) {
        
        let navigationHelpVC =  NavigationManager.singleton.navigation(screenType: NavigationType.NavigationTypeHelp)
        present(navigationHelpVC, animated: true, completion: nil)
    }
    
    @IBAction func segmentTapped(_ sender: UISegmentedControl) {
        
        let index = sender.selectedSegmentIndex
    }
    
    @IBAction func btnSignupTapped(_ sender: UIButton) {
        
        if IsValidInputs() == false {
            return
        }
        requestForLogin()
    }
    
    @IBAction func btnNewUserTapped(_ sender: UIButton) {
        
        let controller = NavigationManager.singleton.screen(screenType: NavigationScreenType.NavigationScreenTypeSignup) as! SignupVC
        _ = navigationController?.pushViewController(controller, animated: true)
        
    }
  
    //MARK:- UIView Life Cycle Method
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureUI()
        
        if userPassword != "" && userEamil != "" && appStart == false {
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.1, execute: {
                //self.requestForLogin()
            })
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if userEamil != "" {
            txtEmail.text = userEamil
        }
        if userPassword != "" {
            txtPassword.text = userPassword
        }
    }
    
}
