//
//  SinupSocailInfoVC.swift
//  FreeBee
//
//  Created by RMS on 12/28/19.
//  Copyright © 2019 dharmesh. All rights reserved.
//

import UIKit
import DropDown

class SinupSocailInfoVC: BaseVC {
    
    var userID = ""

    @IBOutlet weak var txtPhone: UITextField!
    @IBOutlet weak var txtAddr: UITextField!
    @IBOutlet weak var txtCity: UITextField!
    @IBOutlet weak var butCity: UIButton!
    @IBOutlet weak var txtState: UITextField!
    @IBOutlet weak var butState: UIButton!
    @IBOutlet weak var txtZipcode: UITextField!
    @IBOutlet weak var txtAge: UITextField!
    @IBOutlet weak var txtGender: UITextField!
    @IBOutlet weak var butGender: UIButton!
    
    @IBOutlet weak var popupVIew: UIView!
    @IBOutlet weak var popupTitle: UILabel!
    @IBOutlet weak var popupCollectionView: UICollectionView!
    @IBOutlet weak var uiTableView: UITableView!
    
    var dataSource = [HobbyModel]()
    let dropDownGender = DropDown()
    var popupOpenState = ""
    var tableData = [CategoryModel]()
    
    let requestParam = RequestParamModal()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        popupCollectionView.delegate = self
        popupCollectionView.dataSource = self
        
        uiTableView.delegate = self
        uiTableView.dataSource = self
        uiTableView.tableFooterView = UIView()
        uiTableView.reloadData()
        
        configData()
    }
    
    func configData() {
        //MARK:- gender data set
        dropDownGender .dataSource = genders
        dropDownGender.anchorView = butGender
        dropDownGender.selectionAction = { [unowned self] (index, item) in
            self.txtGender.text = item
            self.requestParam.productGender = item
        }
        dropDownGender.backgroundColor = UIColor.white
        dropDownGender.textColor = UIColor.black
        dropDownGender.separatorColor = kColorGray
    }
    
    func IsValidInputs() -> Bool {
        
        if txtPhone.text?.isEmpty == true {
            messageBox(message: "Please enter phone number")
            return false
        }
        
        if txtAddr.text?.isEmpty == true {
            messageBox(message: "Please enter address")
            return false
        }
        if txtCity.text?.isEmpty == true {
            messageBox(message: "Please enter city")
            return false
        }
        if txtState.text?.isEmpty == true {
            messageBox(message: "Please enter state")
            return false
        }
        if txtZipcode.text?.isEmpty == true {
            messageBox(message: "Please enter zipcode")
            return false
        }
        if txtAge.text?.isEmpty == true {
            messageBox(message: "Please enter age")
            return false
        }
        if txtGender.text?.isEmpty == true {
            messageBox(message: "Please select gender")
            return false
        }
 
        return true
    }
    
    @IBAction func onTapedBack(_ sender: Any) {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let nextVC = storyBoard.instantiateViewController(withIdentifier: "SignupVC")
        nextVC.modalPresentationStyle = .fullScreen
        self.present(nextVC, animated: false, completion: nil)
    }
    
    @IBAction func onTapedCity(_ sender: Any) {
        if txtState.text! == "" {
            messageBox(message: "Please choose the State.")
            return
        }
        
        if popupOpenState == "city" {} else {
            popupOpenState = "city"
            popupTitle.text = "Choose the City"
            uiTableView.isHidden = false
            popupCollectionView.isHidden = true
            tableData.removeAll()
            
            var selectedStates = txtState.text?.components(separatedBy: ";")
            var selectedCitys = txtCity.text?.components(separatedBy: ";")
            
            for index in 0 ..< selectedStates!.count {
                selectedStates![index] = selectedStates![index].trimmed
            }
            for index in 0 ..< selectedCitys!.count {
                selectedCitys![index] = selectedCitys![index].trimmed
            }
            for oneState in dataStateCity! {
                if selectedStates!.contains(oneState.key) {
                    for oneCity in oneState.value as! [String] {
                        let isSelected = selectedCitys?.contains(oneCity)
                        tableData.append(CategoryModel(index: 0, title: oneCity, selected: isSelected!))
                    }
                } else {}
            }
            
            uiTableView.reloadData()
        }
        popupVIew.isHidden = false
    }
    
    @IBAction func onTapedState(_ sender: Any) {
        if popupOpenState == "state" {} else {
            popupOpenState = "state"
            popupTitle.text = "Choose the State"
            uiTableView.isHidden = false
            popupCollectionView.isHidden = true
            tableData.removeAll()
            
            let selectedStates = txtState.text?.components(separatedBy: ";")
            for oneState in dataStateCity! {
                let isSelected = selectedStates?.contains(oneState.key)
                tableData.append(CategoryModel(index: 0, title: oneState.key, selected: isSelected!))
            }
            uiTableView.reloadData()
        }
        popupVIew.isHidden = false
    }
    
    @IBAction func onTapedGender(_ sender: Any) {
        dropDownGender.show()
    }
    
    @IBAction func onTapedCancel(_ sender: Any) {
        popupVIew.isHidden = true
    }
    
    @IBAction func onTapedSelect(_ sender: Any) {
        var str = ""
        
        for one in tableData {
            if one.selected {
                if str == "" {str=one.title}
                else { str += ";" + one.title }
            }
        }
        if popupOpenState == "state" {
            txtState.text = str
        }
        else {
            txtCity.text = str
        }
        
        /*if popupOpenState == "state" {
            for one in dataSource {
                if one.selectState {
                    if str == "" {str=one.topic}
                    else { str += ";" + one.topic }
                }
            }
            if txtState.text != str {txtCity.text = ""}
            txtState.text = str
        } else {
            for one in tableData {
                if one.selected {
                    if str == "" {str=one.title}
                    else { str += ";" + one.title }
                }
            }
            txtCity.text = str
        }*/
        
        popupVIew.isHidden = true
    }
    
    @IBAction func onTapedSave(_ sender: Any) {
        if IsValidInputs() == false {
            return
        }
        
        let request = "\(kAPIRegisterSocialAdd)"
        
        let requestParam = RequestParamModal()
        requestParam.action = kAPIRegisterSocialAdd
        
        requestParam.Id = userID
        requestParam.phone = txtPhone.text
        requestParam.address = txtAddr.text!
        requestParam.city = txtCity.text!
        requestParam.state = txtState.text!
        requestParam.profileZipcode = txtZipcode.text!
        requestParam.age = txtAge.text!
        requestParam.gender = txtGender.text!
        
        RequestManager.singleton.requestPOST(requestMethod: request, parameter: requestParam.toDictionary(), showLoader: true, successBlock: { (resposne :MessageModal, message : String?) in
            
            if resposne.resultCode == 0 {
                let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let nextVC = storyBoard.instantiateViewController(withIdentifier: "FewQuestion1VC") as! FewQuestion1VC
                nextVC.userID = self.userID
                nextVC.modalPresentationStyle = .fullScreen
                self.present(nextVC, animated: false, completion: nil)
            } else {
                messageBox(message: resposne.error)
            }
        }, failureBlock: {(error : Error?) in })
    }
    
}

//MARK:- UICollectionView
extension SinupSocailInfoVC : UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        dataSource[indexPath.row].selectState = !dataSource[indexPath.row].selectState
        self.popupCollectionView.reloadData()

    }
}

extension SinupSocailInfoVC : UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let w = CGFloat(dataSource[indexPath.row].topic.count * 9 + 15)
        return CGSize(width: w, height: 25)
    }
    
}

extension SinupSocailInfoVC : UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return dataSource.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PostHobbyCell", for: indexPath) as! HobbyCell
        cell.entity = dataSource[indexPath.row]
        return cell
    }
}

//MARK:- UITableView
extension SinupSocailInfoVC : UITableViewDelegate {

    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {

        if tableData[indexPath.row].selected {
            cell.accessoryType = .checkmark
        } else {
            cell.accessoryType = .none
        }
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        tableData[indexPath.row].selected = !tableData[indexPath.row].selected
        uiTableView.reloadData()
    }
}

extension SinupSocailInfoVC : UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "CategoryCell") as! CategoryCell
        cell.entity = tableData[indexPath.row]
        
        return cell
    }
}


