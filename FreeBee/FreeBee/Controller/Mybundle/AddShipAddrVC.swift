//
//  AddShipAddr.swift
//  FreeBee
//
//  Created by AngelDev on 2/4/20.
//  Copyright © 2020 dharmesh. All rights reserved.
//

import UIKit
import MBProgressHUD
import Localize_Swift

class AddShipAddrVC: BaseVC {
    
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var txtAddress: TextField!
    @IBOutlet weak var txtDistrict: TextField!
    @IBOutlet weak var txtCity: TextField!
    @IBOutlet weak var txtZipcode: TextField!
    @IBOutlet weak var txtState: TextField!
    
    var hud : MBProgressHUD!
    var shipAddressData = [ShipAddressModel]()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.title = "Add Shipping Address".localized()
                
        let saveButton: UIButton = UIButton(type: .custom)
        let origImage = UIImage(named: "ic_checked") // UIImage(systemName: "")
        let tintedImage = origImage?.withRenderingMode(.alwaysTemplate)
        saveButton.setImage(tintedImage, for: .normal)
        saveButton.tintColor = .white
        saveButton.addTarget(self, action: #selector(didTapSaveButton(_:)), for: .touchUpInside)
       
        let rightBarButton = UIBarButtonItem(customView: saveButton)
        self.navigationItem.rightBarButtonItem = rightBarButton
        
    }
    
    func isInvalid() -> Bool {
        if txtName.text!.isEmpty {
            showAlertOk("Please input Full Name.")
            return false
        }
        if txtAddress.text!.isEmpty {
            showAlertOk("Please input Address.")
            return false
        }
        
        if txtDistrict.text!.isEmpty {
            showAlertOk("Please input District.")
            return false
        }
        
        if txtCity.text!.isEmpty {
            showAlertOk("Please input City.")
            return false
        }
        if txtZipcode.text!.isEmpty {
            showAlertOk("Please input Zipcode.")
            return false
        }
        if txtState.text!.isEmpty {
            showAlertOk("Please input State.")
            return false
        }
        
        return true
    }
    
    @objc func didTapSaveButton(_ sender: UIButton) {
        
        if !isInvalid() {
            return
        }
        
        hud = progressHUD(view: self.view, mode: .indeterminate)
        hud.show(animated: true)
        
        let request = "\(kAPIAddAdress)"
        
        let requestParam = RequestParamModal()
        requestParam.action = kAPIAddAdress
        requestParam.userId     = String(userId)
        requestParam.shipname   = txtName.text!
        requestParam.address    = txtAddress.text!
        requestParam.city       = txtCity.text!
        requestParam.zipCode    = txtZipcode.text!
        requestParam.state      = txtState.text!
        
        RequestManager.singleton.requestPOST(requestMethod: request, parameter: requestParam.toDictionary(), showLoader: false, successBlock: { (resposne :MessageModal, message : String?) in
            
            self.hud.hide(animated: true)
            self.navigationController?.popViewController(animated: true)
            
        }, failureBlock: {(error : Error?) in
            self.hud.hide(animated: true)
            self.navigationItem.rightBarButtonItem?.isEnabled = false
        })
        
    }
    

}
