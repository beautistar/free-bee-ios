//
//  ShippingConfirmVC.swift
//  FreeBee
//
//  Created by AngelDev on 2/5/20.
//  Copyright © 2020 dharmesh. All rights reserved.
//

import UIKit

class ShippingConfirmVC: BaseVC {

    @IBOutlet weak var lblUsername: UILabel!
    @IBOutlet weak var lblShipname: UILabel!
    @IBOutlet weak var lblShipAddr: UILabel!
    @IBOutlet weak var lblShipCity: UILabel!
    @IBOutlet weak var lblShipState: UILabel!
    @IBOutlet weak var uiTableview: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        configureUI()
        
        uiTableview.dataSource = self
        uiTableview.tableFooterView = UIView()
    }
    
    func configureUI() {
        
        self.title = "Order Confirmation".localized()
        lblUsername.text = userData?.username
        lblShipname.text = ShipAddressData.shipName
        lblShipAddr.text = ShipAddressData.shipAdress
        lblShipCity.text = ShipAddressData.shipCity + ", " + ShipAddressData.shipZipcode
        lblShipState.text = ShipAddressData.shipProvince
        
    }

    // MARK: - custom action
    @IBAction func onTapedOrder(_ sender: Any) {
                
        var orderList = [Dictionary<String, String>()]
        orderList.removeAll()
        for one in myCartData {
            orderList.append(["product_id": one.productId, "count": one.soldCount])
        }
        
        let request = "\(kAPICheckout)"
        let requestParam = RequestParamModal()
        requestParam.action = kAPICheckout
        requestParam.userId = String(userId)
        requestParam.addressId = ShipAddressData.shipId
        requestParam.orderList = orderList
        
        
        RequestManager.singleton.requestRowPOST(requestMethod: request, parameter: requestParam.toDictionary(), showLoader: false, successBlock: { (resposne :MessageModal, message : String?) in
            
            if resposne.resultCode == 0 {
                self.navigationController?.popToRootViewController(animated: true)
                
            } else {
                self.showAlertOk(message!)
            }
            
        }, failureBlock: {(error : Error?) in })
        
//        self.navigationController?.popViewController(animated: true)
    }
    
}

extension ShippingConfirmVC : UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return myCartData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String.className(CartOrderCell.self), for: indexPath) as! CartOrderCell
        
        let product = myCartData[indexPath.row]
        
        cell.lblProductName.text  = "Name : " + product.productName
        cell.lblProductCount.text = product.soldCount
        cell.lblProductType.text  = product.type.prefix(1).capitalized + product.type.dropFirst()
        cell.lblProductUsername.text = "Advertiser : " + product.product_username
        
        return cell;
    }
    
}


class CartOrderCell : UITableViewCell {
    
    @IBOutlet weak var lblProductName: UILabel!
    @IBOutlet weak var lblProductCount: UILabel!
    @IBOutlet weak var lblProductType: UILabel!
    @IBOutlet weak var lblProductUsername: UILabel!
    
}
