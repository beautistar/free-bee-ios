//
//  MyNetworksVC.swift
//  HT
//
//  Created by Dharmesh on 02/12/16.
//  Copyright © 2016 dharmesh. All rights reserved.
//

import UIKit
import Foundation
import Localize_Swift
import Toucan
import FCUUID
import MBProgressHUD

var myCartData = [ProductListModal]()

class MyNetworksVC : BaseVC {
    
    @IBOutlet weak var uiTableview: UITableView!
    @IBOutlet weak var uiViewContinue: UIView!
    
    
    var isMenuScreen : Bool = true
    let pageSize = 9
    let preloadMargin = 1
    var lastLoadedPage = 0
    
    var hud : MBProgressHUD!
    
    //MARK:- Memory Management Method
    override func didReceiveMemoryWarning() {}
    
    deinit { //same like dealloc in ObjectiveC
        MyNotificationCenter.removeObserver(self)        
    }
    
    //MARK:- Custom Methods
    @objc func configureUI() {
        
        uiViewContinue.roundCorners([.topLeft, .topRight], radius: 20)
        if #available(iOS 11.0, *) {
            uiViewContinue.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        } else {
            // Fallback on earlier versions
        }
        
        self.title = "My Bundle".localized()
        
        MyNotificationCenter.addObserver(self, selector: #selector(profileTapped), name: NSNotification.Name(rawValue: kNotificationMyNetworkVC), object: nil)
        uiTableview.delegate = self
        uiTableview.dataSource = self
        uiTableview.tableFooterView = UIView()
    }
    
    func requestForCartList(page : NSInteger, showLoader : Bool)  {
        
        let request = "\(kAPIGetCartList)"
        let requestParam = RequestParamModal()
        requestParam.action = kAPIGetCartList
        requestParam.userId = String(userId)
        requestParam.page = String(page + 1)
        
        RequestManager.singleton.requestPOST(requestMethod: request, parameter: requestParam.toDictionary(), showLoader: false, successBlock: { (resposne :MessageModal, message : String?) in
            
            if page == 0 {
                myCartData.removeAll()
            }
            
            myCartData.append(contentsOf: resposne.productList)
            self.uiTableview.reloadData()
            self.hud.hide(animated: true)
            
        }, failureBlock: {(error : Error?) in
            self.hud.hide(animated: true)
        })
    }
    
    func requestForSignup() {
        
        let request = "\(kAPIRegister)"
        
        let requestParam = RequestParamModal()
        requestParam.action = kAPIRegister
        requestParam.deviceId = FCUUID.uuidForDevice()
        
        RequestManager.singleton.requestPOST(requestMethod: request, parameter: requestParam.toDictionary(), showLoader: true, successBlock: { (resposne :MessageModal, message : String?) in
            
            let currentUser = resposne.userInfo
            if currentUser != nil {
                
                if (currentUser!.userid != nil && currentUser!.userid != "0") {
                    MyUserDefaults.set(Int(currentUser!.userid), forKey: kCurrentUserId)
                }
                let userData = NSKeyedArchiver.archivedData(withRootObject: currentUser!.toDictionary())
                MyUserDefaults.setValue(userData, forKey: kCurrentUser)
                
                if currentUser?.username == "" {
                    
                    let controller = NavigationManager.singleton.screen(screenType: NavigationScreenType.NavigationScreenTypeEditProfile) as! EditProfileVC
                    self.present(controller, animated: false, completion: nil);
                }
                
            } else {
                
                let controller = NavigationManager.singleton.screen(screenType: NavigationScreenType.NavigationScreenTypeEditProfile) as! EditProfileVC
                self.present(controller, animated: false, completion: nil);
            }
            self.requestForCartList(page: 0, showLoader: true)
            
        }, failureBlock: {(error : Error?) in
            
        })
    }
    
    @objc func accessoryButtonTapped (_ sender: UIButton) {
        let message = "Do you want to remove this product from the cart?".localized()
        
        let alert = UIAlertController(title: kAppName, message: message, preferredStyle: .alert)
    
        alert.addAction (UIAlertAction(title: "Delete".localized(), style: .default) { (alertAction) in
            let request = "\(kAPIRemoveCart)"
            let requestParam = RequestParamModal()
            requestParam.action = kAPIRemoveCart
            requestParam.userId = String(userId)
            requestParam.productSmallId = myCartData[sender.tag].productId
            requestParam.cartNum = myCartData[sender.tag].soldCount
            RequestManager.singleton.requestPOST(requestMethod: request, parameter: requestParam.toDictionary(), showLoader: true, successBlock: { (resposne :MessageModal, message : String?) in
                myCartData.remove(at: sender.tag)
                self.uiTableview.reloadData()
            }, failureBlock: {(error : Error?) in
            })
        })

        //Cancel action
        alert.addAction(UIAlertAction(title: kCancel, style: .default) { (alertAction) in })
        self.present(alert, animated:true, completion: nil)
        
    }
    
    @IBAction func onTapedCheckout(_ sender: Any) {
        
        var primiumCount = 0
        var regularCount = 0
        var strLow = ""
        var strOver = ""
        
        for one in myCartData {
            if one.type!.lowercased() == "regular" {
                regularCount += one.soldCount.toInt()!
            } else {
                primiumCount += one.soldCount.toInt()!
            }
        }
        
        
        if primiumCount < 5 {
            strLow = "\(5 - primiumCount) more premium items"
        } else if primiumCount > 5 {
            strOver = "\(primiumCount - 5) more premium items"
        }
        
        if regularCount < 10 {
            if strLow.isEmpty {
                strLow = "\(10 - regularCount) more regular items"
            }
            else {
                strLow += " and \(10 - regularCount) more regular items"
            }
                        
        } else if regularCount > 10 {
            
            if strLow.isEmpty {
                strOver = "\(regularCount - 10) more regular items"
            }
            else {
                strOver += " and \(regularCount - 10) more regular items"
            }
        }
        
        if strLow.isEmpty && strOver.isEmpty {
            let controller = NavigationManager.singleton.screen(screenType: NavigationScreenType.NavigationScreenTypeShipAddress) as! ShipAdressVC
            navigationController?.pushViewController(controller, animated: true)
            
        } else {
            
            if !strLow.isEmpty {
                messageBox(message: "Please choose " + strLow + " to proceed.")
            }
            
            if !strOver.isEmpty {
                messageBox(message: "You have exceeded the limit of premium items. Please remove " + strOver + " from your bundle to checkout.")
                
            }
        }
    }
    
    //MARK:- Notification
    @objc func profileTapped(notification : Notification) {
        
        let controller = NavigationManager.singleton.screen(screenType: NavigationScreenType.NavigationScreenTypeProfile) as! ProfileVC
        navigationController?.pushViewController(controller, animated: true)
    }
    
    //MARK:- UIViewController
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {}
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        MyNotificationCenter.addObserver(self, selector: #selector(configureUI), name: NSNotification.Name( LCLLanguageChangeNotification), object: nil)
        
        myCartData.removeAll()
        configureUI()
        
//        self.uiTableview.addSubview(self.refreshControl)
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if isMenuScreen {
            self.setNavigationBarItem()            
        }
        
        self.uiTableview.isHidden = false
        
        if userId == 0 {
            self.requestForSignup()
        } else {
            self.hud = progressHUD(view: self.view, mode: .indeterminate)
            self.hud.show(animated: true)
            self.requestForCartList(page: 0, showLoader: true)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
}

extension MyNetworksVC : UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return myCartData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String.className(CartListCell.self), for: indexPath) as! CartListCell
        
        let product = myCartData[indexPath.row]
        let imageURL = URL(string: product.productImage)
        if imageURL != nil {
            
            let imgDownloadRequest = URLRequest(url: imageURL!)
            cell.producImg.setImageWith(imgDownloadRequest, placeholderImage: #imageLiteral(resourceName: "placeholder"), success: { (request : URLRequest, response : HTTPURLResponse?, image : UIImage) in
                
                cell.producImg.image = Toucan(image: image).resizeByCropping(cell.producImg.bounds.size).image
            }, failure: { (request : URLRequest, response : HTTPURLResponse?, error : Error) in
                
            })
        }
        cell.title.text = product.productName
        cell.lblType.text = product.type.prefix(1).capitalized + product.type.dropFirst()

        cell.quanNum.text = "Quantities : " + product.soldCount
        cell.quanNum.numberOfLines = 0
        
        let cellAudioButton = UIButton(type: .custom)
        cellAudioButton.tag = indexPath.row
        cellAudioButton.frame = CGRect(x: 0, y: 0, width: 20, height: 20)
        cellAudioButton.addTarget(self, action: #selector(MyNetworksVC.accessoryButtonTapped(_ :)), for: .touchUpInside)
        cellAudioButton.setImage(UIImage(named: "delete.png"), for: .normal)
        cellAudioButton.contentMode = .scaleAspectFill
        cell.accessoryView = cellAudioButton as UIView
        
        return cell;
    }
    
}

extension MyNetworksVC : UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let controller = NavigationManager.singleton.screen(screenType: NavigationScreenType.NavigationScreenTypeProfileDetail) as! ProfileDetailVC
        controller.product = myCartData[indexPath.item]
        navigationController?.pushViewController(controller, animated: true)
    }
}

extension MyNetworksVC : UIScrollViewDelegate {
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
//                                        indexPathsForVisibleItems
        if let indexPath = uiTableview.indexPathsForVisibleRows?.last {
            
            let nextPage: Int = Int(indexPath.row / pageSize) + 1
            let preloadIndex = nextPage * (pageSize - preloadMargin)
            
            if (indexPath.item >= preloadIndex && lastLoadedPage < nextPage) {
                
                lastLoadedPage = nextPage
                requestForCartList(page: nextPage, showLoader: false)
            }
        }
    }
}


class CartListCell : UITableViewCell {
    
    @IBOutlet weak var producImg: UIImageView!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var quanNum: UILabel!
    @IBOutlet weak var lblType: UILabel!
    
    //MARK:- Initialisation
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        //imgItem.contentMode = .scaleAspectFit
        //lblItemName.numberOfLines = 0
        //lblItemName.sizeToFit()
    }
    
}
