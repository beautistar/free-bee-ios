//
//  ShipAdressVC.swift
//  FreeBee
//
//  Created by AngelDev on 2/4/20.
//  Copyright © 2020 dharmesh. All rights reserved.
//

import UIKit
import MBProgressHUD
import Localize_Swift

var ShipAddressID = -1
var ShipAddressData = ShipAddressModel()

class ShipAdressVC: BaseVC {
    
    @IBOutlet weak var uiTableview: UITableView!
    @IBOutlet weak var uiButSelectAddress: UIButton!
    
    var hud : MBProgressHUD!
    var shipAddressData = [ShipAddressModel]()
    
    var selectedShipRowID = -1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        MyNotificationCenter.addObserver(self, selector: #selector(configureUI), name: NSNotification.Name( LCLLanguageChangeNotification), object: nil)
        configureUI()

    }
    
    //MARK:- UIViewController
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {}
        
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let addButton = UIButton()
        addButton.frame = CGRect(x:0, y:0, width:30, height:30)
        addButton.setTitle("+", for: .normal)
        addButton.titleLabel?.font = .systemFont(ofSize: 30)
        addButton.addTarget(self, action: #selector(didTapAddButton(_:)), for: .touchUpInside)

        let rightBarButton = UIBarButtonItem(customView: addButton)
        self.navigationItem.rightBarButtonItem = rightBarButton
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1, execute: {
            
            self.hud = progressHUD(view: self.view, mode: .indeterminate)
            self.hud.show(animated: true)
            self.requestForShipAdressList()
        })
    }
        
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    

    @IBAction func onTapedSelectAdress(_ sender: Any) {
        if selectedShipRowID == -1 {
            showAlertOk("Please select shipping address.")
            return
        }
        
        let shipModel = ShipAddressModel(fromDictionary: [:])
        shipModel.shipId        =  shipAddressData[selectedShipRowID].shipId
        shipModel.shipName      =  shipAddressData[selectedShipRowID].shipName
        shipModel.shipAdress    =  shipAddressData[selectedShipRowID].shipAdress
        shipModel.shipCity      =  shipAddressData[selectedShipRowID].shipCity
        shipModel.shipZipcode   =  shipAddressData[selectedShipRowID].shipZipcode
        shipModel.shipProvince  =  shipAddressData[selectedShipRowID].shipProvince
        
        let lastShipAdress = NSKeyedArchiver.archivedData(withRootObject: shipModel.toDictionary())
//        MyUserDefaults.setValue(lastShipAdress, forKey: kLastShipAdress)
//        MyUserDefaults.set(Int(selectedShipRowID), forKey: kLastShipRowNum)
        
        ShipAddressID = selectedShipRowID
        ShipAddressData = shipAddressData[selectedShipRowID]
        
        let controller = NavigationManager.singleton.screen(screenType: NavigationScreenType.NavigationScreenTypeShipConfirm) as! ShippingConfirmVC
        navigationController?.pushViewController(controller, animated: true)
    }
    
    //MARK:- Memory Management Method
    override func didReceiveMemoryWarning() {}
    
    deinit {
        MyNotificationCenter.removeObserver(self)
    }
    
    //MARK:- Custom Methods
    @objc func configureUI() {
        
        self.title = "Shipping Adress".localized()
        
        uiTableview.delegate = self
        uiTableview.dataSource = self
        uiTableview.tableFooterView = UIView()
    }
    
    @objc func didTapAddButton(_ sender: UIButton) {
        let controller = NavigationManager.singleton.screen(screenType: NavigationScreenType.NavigationScreenTypeAddShipAddr) as! AddShipAddrVC
        navigationController?.pushViewController(controller, animated: true)
    }
    
    func requestForShipAdressList()  {
        
        shipAddressData.removeAll()
        
        let request = "\(kAPIGetAdress)"
        
        let requestParam = RequestParamModal()
        requestParam.action = kAPIGetAdress
        requestParam.userId = String(userId)
        
        RequestManager.singleton.requestPOST(requestMethod: request, parameter: requestParam.toDictionary(), showLoader: false, successBlock: { (resposne :MessageModal, message : String?) in
            
            self.shipAddressData = resposne.shipAdressList
            self.uiTableview.reloadData()
            self.hud.hide(animated: true)
            
        }, failureBlock: {(error : Error?) in
            self.uiTableview.reloadData()
            self.hud.hide(animated: true)
        })
    }
}

//MARK:- UITableViewDataSource
extension ShipAdressVC : UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return shipAddressData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String.className(ShipAdressCell.self), for: indexPath) as! ShipAdressCell
        
        let oneAddr = shipAddressData[indexPath.row]
        cell.uiLblName.text = oneAddr.shipName
        cell.uiLblAddr.text = oneAddr.shipAdress
        cell.uiLblCity.text = oneAddr.shipCity + ", " + oneAddr.shipZipcode
        cell.uiLblProvince.text = oneAddr.shipProvince
        
        return cell;
    }
    
}

extension ShipAdressVC : UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedShipRowID = indexPath.row
    }
}

//MARK:- ShipAdressCell
class ShipAdressCell : UITableViewCell {
    
    @IBOutlet weak var uiLblName: UILabel!
    @IBOutlet weak var uiLblAddr: UILabel!
    @IBOutlet weak var uiLblCity: UILabel!
    @IBOutlet weak var uiLblProvince: UILabel!
        
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}
