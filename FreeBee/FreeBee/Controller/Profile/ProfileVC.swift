//
//  ProfileVC.swift
//  HT
//
//  Created by Dharmesh on 04/12/16.
//  Copyright © 2016 dharmesh. All rights reserved.
//

import UIKit
import Foundation
import Toucan

class ProfileVC : BaseVC {
    
    @IBOutlet weak var scrollProfile: UIScrollView!
    @IBOutlet weak var scrollContainer: UIScrollView!
    
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var lblFollowing: UILabel!
    @IBOutlet weak var lblFollowers: UILabel!
    @IBOutlet weak var btnUserName: UIButton!
    @IBOutlet weak var btnListed: UIButton!
    @IBOutlet weak var btnLike: UIButton!
    @IBOutlet weak var btnBought: UIButton!
    
    @IBOutlet weak var imgFB: UIButton!
    @IBOutlet weak var imgGoogle: UIButton!
    @IBOutlet weak var imgCall: UIButton!
    
    @IBOutlet weak var imgFollowing: UIImageView!
    @IBOutlet weak var imgFollowers: UIImageView!
    
    @IBOutlet weak var btnEdit: UIButton!
    @IBOutlet weak var lblVerifiedAccounts: UILabel!
    
    @IBOutlet weak var layoutSeperatorLeading: NSLayoutConstraint!
    @IBOutlet weak var layoutLineLeading: NSLayoutConstraint!
    @IBOutlet weak var layoutScrollContainerHeight: NSLayoutConstraint!
    
    var listedVC : ListedVC!
    var likesVC : LikedVC!
    var boughtVC : BoughtVC!
    var editProfileVC : EditProfileVC!
    
    var controllers : [UIViewController]! {
        return [listedVC, likesVC, boughtVC]
    }
    
    var userProfile : UserProfile!
    var mobile : String?
    
    //MARK:- Memory Management Method
    
    override func didReceiveMemoryWarning() {}
    
    deinit {}
    
    //MARK:- Custom Methods
    
    func configureUI() {
        
        scrollProfile.delegate = self
        scrollContainer.delegate = self
        
        btnListed.isSelected = true
        
        LocalizedControl(view: btnBought, key : "CLAIMED")
        LocalizedControl(view: btnListed, key : "LISTED")
        LocalizedControl(view: btnLike, key : "LIKED")
        LocalizedControl(view: lblVerifiedAccounts, key : "VERIFIED ACCOUNTS")
        
        lblFollowers.isHidden = true
        imgFollowers.isHidden = true
        lblFollowing.isHidden = true
        imgFollowing.isHidden = true
        scrollContainer.isHidden = true
        btnEdit.isHidden = true
        imgCall.isHidden = true
    }
    
    func configureData() {

        lblFollowers.isHidden = false
        imgFollowers.isHidden = false
        lblFollowing.isHidden = false
        imgFollowing.isHidden = false
        scrollContainer.isHidden = false
        
        if userProfile == nil && userProfile.userid == nil {
            //do something
        } else {
            btnEdit.isHidden = false
            imgCall.isHidden = false
        }
        
        if userProfile.userimage != nil && URL(string: userProfile.userimage) != nil {
            
            //imgProfile.setImageWith( URL(string: userProfile.userimage)!, placeholderImage: UIImage())
            let imgDownloadRequest = URLRequest(url: URL(string: userProfile.userimage)!)
            imgProfile.setImageWith(imgDownloadRequest, placeholderImage: UIImage(), success: { (request : URLRequest, response : HTTPURLResponse?, image : UIImage) in
                
                self.imgProfile.image = Toucan(image: image).resizeByCropping(self.imgProfile.bounds.size).image
                
            }, failure: { (request : URLRequest, response : HTTPURLResponse?, error : Error) in
                
            })
        }
        
        lblFollowing.text = "\(userProfile.countFollowing!) \("Following".localized())"
        lblFollowers.text = "\(userProfile.countFollowers!) \("Followers".localized())"
        btnUserName .setTitle(userProfile.username, for: .normal)
        
        //listed
        listedVC.productItems = userProfile.listOfProducts
        listedVC.collectionProduct.reloadData()
        
        //likes
        likesVC.productItems = userProfile.productLikes
        likesVC.collectionProduct.reloadData()
        
        //bought
        boughtVC.productItems = userProfile.productBought
        boughtVC.collectionProduct.reloadData()
        
        //verified
        if userProfile.verifyFacebook == "Y" {
            imgFB.isSelected = true
        } else {
            imgFB.isSelected = false
        }
        
        if userProfile.verifyGoogle == "Y" {
            imgGoogle.isSelected = true
        } else {
            imgGoogle.isSelected = false
        }
        
        if userProfile.isVerified == 1 {
            imgCall.isSelected = true
        } else {
            imgCall.isSelected = false
        }
    }
    
    func requestToGetProfile() {
        
        let request = "\(kAPIProfile)"
        
        let requestParam = RequestParamModal()
        requestParam.action = kAPIProfile
        
        if userProfile != nil && userProfile.userid != nil {
            requestParam.userId = userProfile.userid
        } else {
            requestParam.userId = String(userId)
        }
        
        RequestManager.singleton.requestPOST(requestMethod: request, parameter: requestParam.toDictionary(), showLoader: true, successBlock: { (resposne :MessageModal, message : String?) in
            
            self.userProfile = resposne.userProfile
            self.configureData()
            
        }, failureBlock: {(error : Error?) in
            
        })
    }
    
    //MARK:- Action Methods
    
    @IBAction func btnListedTapped(_ sender: UIButton) {
        scrollContainer.contentOffset = CGPoint(x: 0, y: 0)
    }
    
    @IBAction func btnLikeTapped(_ sender: UIButton) {
        scrollContainer.contentOffset = CGPoint(x: ScreenRect.width, y: 0)
    }
    
    @IBAction func btnBoughtTapped(_ sender: UIButton) {
        scrollContainer.contentOffset = CGPoint(x: (ScreenRect.width * 2), y: 0)
    }
    
    @IBAction func btnShareTapped(_ sender: UIButton) {
        
        let productName = userProfile.username ?? ""
        let follower = String(userProfile.countFollowers) 
        let following = String(userProfile.countFollowing)
        
        /*let productPrice = userProfile.productBought.map { (object : ProductListModal) -> String in
            return object.productName
        }*/
        
        let stringToShare = "\(productName)\n".appending("Follower : \(follower)\n").appending("Following : \(following)\n")//.appending("Bought : \(productPrice)\n")
        NavigationManager.singleton.shareIt(view: self, message: stringToShare, image: imgProfile.image)
    }
    
    @IBAction func btnEditTapped(_ sender: UIButton) {
        
        let controller = NavigationManager.singleton.screen(screenType: NavigationScreenType.NavigationScreenTypeEditProfile) as! EditProfileVC
        if userProfile != nil {
            controller.userProfile = userProfile
        }
        navigationController?.pushViewController(controller, animated: true)
    }
    
    @IBAction func btnCallTap(_ sender: UIButton) {
        
        let busPhone = mobile
        
        if busPhone != nil {
            
            if let url = URL(string: "tel://\(busPhone!)") {
                
                if UIApplication.shared.canOpenURL(url) == true {
                    UIApplication.shared.openURL(url)
                }
            }
        }
    }
    
    //MARK:- UISCrollViewDelegate
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        if scrollContainer == scrollView {
         
            let pageWidth = scrollView.bounds.width
            let offset = scrollView.contentOffset.x
            let currentPage = offset/pageWidth
            
            layoutLineLeading.constant = (offset/CGFloat(controllers.count))
            
            if currentPage == 0 {
                btnListed.isSelected = true
                btnLike.isSelected = false
                btnBought.isSelected = false
            }
            
            if currentPage == 1 {
                btnListed.isSelected = false
                btnLike.isSelected = true
                btnBought.isSelected = false
            }
            
            if currentPage == 2 {
                btnListed.isSelected = false
                btnLike.isSelected = false
                btnBought.isSelected = true
            }
        }
    }
    
    //MARK:- UIView Life Cycle Method
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    
        if segue.identifier == "segueListedVC" {
            listedVC = segue.destination as! ListedVC
        } else if segue.identifier == "segueLikedVC" {
            likesVC = segue.destination as! LikedVC
        } else if segue.identifier == "segueBoughtVC" {
            boughtVC = segue.destination as! BoughtVC
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.isNavigationBarHidden = false
        
        requestToGetProfile()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewWillAppear(animated)          
            
        self.navigationController?.isNavigationBarHidden = false
        
        scrollContainer.layoutIfNeeded()
        let width = scrollContainer.bounds.width
        let height = scrollContainer.bounds.height
        
        if ScreenRect.height < 667 {
            scrollProfile.contentSize = CGSize(width: width, height: 667)
        }
        
        //layoutScrollContainerHeight.constant = 150
        scrollContainer.contentSize = CGSize(width: width * CGFloat(controllers.count), height: height)
    }
    
}

extension ProfileVC : UIScrollViewDelegate {
    
}
