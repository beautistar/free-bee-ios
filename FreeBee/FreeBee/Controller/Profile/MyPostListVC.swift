//
//  MyPostListVC.swift
//  FreeBee
//
//  Created by RMS on 12/27/19.
//  Copyright © 2019 dharmesh. All rights reserved.
//

import UIKit
import Toucan

class MyPostListVC: BaseVC {
    
    var productItems = [ProductListModal]()
    var isReachToEnd = false
    let pageSize = 9
    let preloadMargin = 1
    var lastLoadedPage = 0
    
    var userProfile : UserProfile!
    var isMenuScreen = false
    
    @IBOutlet weak var uiTableview: UITableView!

    //MARK: UIView Life Cycle Method
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        uiTableview.dataSource = self
        uiTableview.delegate = self
        uiTableview.tableFooterView = UIView()
        self.title = "My Post List".localized()
        
        requestForMyPostList(page: 0, showLoader: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.setNavigationBarItem()
        if isMenuScreen {
            self.setNavigationBarItem()
        } else {
            self.navigationController?.isNavigationBarHidden = false
        }
    }
    
    func requestForMyPostList(page: NSInteger, showLoader: Bool) {
        
        let request = "\(kAPIProfile)"
        let requestParam = RequestParamModal()
        requestParam.action = kAPIProfile
        
        if userProfile != nil && userProfile.userid != nil {
            requestParam.userId = userProfile.userid
        } else {
            requestParam.userId = String(userId)
        }
        
        RequestManager.singleton.requestPOST(requestMethod: request, parameter: requestParam.toDictionary(), showLoader: true, successBlock: { (resposne :MessageModal, message : String?) in
            
            self.productItems.removeAll()
            
            self.productItems = resposne.userProfile.listOfProducts
            self.productItems.append(contentsOf: resposne.productList)
            self.uiTableview.reloadData()
            
        }, failureBlock: {(error : Error?) in
        })
    }
    
    @IBAction func editItem(_ sender: UIButton) {
        let controller = NavigationManager.singleton.screen(screenType: NavigationScreenType.NavigationScreenTypePost) as! PostVC
        controller.productID = productItems[sender.tag].productId
        controller.isMenuScreen = false
        navigationController?.pushViewController(controller, animated: true)
    }
    
    @IBAction func deleteItem(_ sender: UIButton) {
        let request = "\(kAPIPostDelete)"
        let requestParam = RequestParamModal()
        requestParam.productId = productItems[sender.tag].productId
        
        RequestManager.singleton.requestPOST(requestMethod: request, parameter: requestParam.toDictionary(), showLoader: true, successBlock: { (resposne :MessageModal, message : String?) in
            
            self.productItems.remove(at: sender.tag)
            self.uiTableview.reloadData()
            
        }, failureBlock: {(error : Error?) in })
    }
    
}

extension MyPostListVC : UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return productItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String.className(MyPostCell.self), for: indexPath) as! MyPostCell
        
        let product = productItems[indexPath.row]
        let imageURL = URL(string: product.productImage)
        if imageURL != nil {
            
            let imgDownloadRequest = URLRequest(url: imageURL!)
            cell.producImg.setImageWith(imgDownloadRequest, placeholderImage: #imageLiteral(resourceName: "Logo"), success: { (request : URLRequest, response : HTTPURLResponse?, image : UIImage) in
                
                cell.producImg.image = Toucan(image: image).resizeByCropping(cell.producImg.bounds.size).image
            }, failure: { (request : URLRequest, response : HTTPURLResponse?, error : Error) in
                
            })
        }
        cell.lblPost.text = product.productName
        cell.butEdit.tag = indexPath.row
        cell.butDelete.tag = indexPath.row
        
        return cell;
    }
    
    
}


extension MyPostListVC : UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let controller = NavigationManager.singleton.screen(screenType: NavigationScreenType.NavigationScreenTypeProfileDetail) as! ProfileDetailVC
        controller.product = productItems[indexPath.item]
        navigationController?.pushViewController(controller, animated: true)
    }
}

class MyPostCell: UITableViewCell {
    
    @IBOutlet weak var producImg: UIImageView!
    @IBOutlet weak var lblPost: UILabel!
    
    @IBOutlet weak var butEdit: UIButton!
    @IBOutlet weak var butDelete: UIButton!
    
}
