//
//  ProfileDetailVC.swift
//  HT
//
//  Created by Dharmesh on 06/12/16.
//  Copyright © 2016 dharmesh. All rights reserved.
//

import UIKit
import Foundation
import AFNetworking
import MapKit
import Toucan
import MessageUI

class ProfileDetailCell : UICollectionViewCell {
    
    @IBOutlet weak var imgItem: UIImageView!
    
    //MARK:- Memory Management Method
    
    deinit {}
    
    //MARK:- Initialisation
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        //imgItem.contentMode = .scaleAspectFit
        imgItem.clipsToBounds = true
    }
}

class ProfileDetailVC : BaseVC, MKMapViewDelegate,MFMessageComposeViewControllerDelegate {
    
    @IBOutlet weak var scrollContainer: UIScrollView!
    
    @IBOutlet weak var collectionImages : UICollectionView!
    
    @IBOutlet weak var btnFollow: UIButton!
    @IBOutlet weak var viewProductCodeBackground: UIView!
    @IBOutlet weak var lblQTY: UILabel!
    @IBOutlet weak var lblClaim: UILabel!
    @IBOutlet weak var btnLike: UIButton!
    @IBOutlet weak var btnCart: UIButton!
    @IBOutlet weak var btnSeeProfile: UIButton!
    @IBOutlet weak var btnSold: UIButton!
    @IBOutlet weak var lblAskFriend: UILabel!
    @IBOutlet weak var lblNoMutualFriends: UILabel!
    @IBOutlet weak var lblProductDescription: UILabel!
    
    @IBOutlet weak var lblType: UILabel!
    @IBOutlet weak var imgOwnerImage: UIImageView!
    @IBOutlet weak var lblOwnerName: UILabel!
    
    @IBOutlet weak var mapProduct: MKMapView!
    @IBOutlet weak var iconProduct: UIButton!
    
    @IBOutlet weak var btnFacebook: UIButton!
    @IBOutlet weak var btnGoogle: UIButton!
    @IBOutlet weak var btnPhone: UIButton!
    
    var product : ProductListModal!
    var productDetail : ProductDetailModal!
    var isMyProduct : Bool! = false

    //MARK:- Memory Management Method
    
    override func didReceiveMemoryWarning() {}
    
    deinit {}
    
    //MARK:- Custom Methods
    func configureUI() {
        
        iconProduct.isHidden = true
        
        scrollContainer.isHidden = true
        self.mapProduct.delegate = self
        
        LocalizedControl(view: lblAskFriend, key : "Tell Your Friends About This")
        LocalizedControl(view: btnSeeProfile, key : "SEE Profile")
    }
    
    func configureData() {
        
        scrollContainer.isHidden = false
        let a1: Int = productDetail.userQuantity!.toInt()!
        let a2: Int = productDetail.claimed!.toInt()!
        let qty = a1 - a2
        lblQTY.text = "QTY: \(qty)"
        lblClaim.text = "Claimed: \(productDetail.claimed!)"
        lblType.text = productDetail.type.prefix(1).uppercased() + productDetail.type.dropFirst()
        
        if productDetail.productOwnerImage.contains("http") {
            let ownerImageURL = URL(string: productDetail.productOwnerImage)
            imgOwnerImage.setImageWith(ownerImageURL!, placeholderImage: #imageLiteral(resourceName: "ic_menu_avatar"))
        } else {
            imgOwnerImage.image = #imageLiteral(resourceName: "ic_menu_avatar")
        }
        
        lblOwnerName.text = productDetail.productOwnerName
        if productDetail.countMutualFriend == 0 {
           LocalizedControl(view: lblNoMutualFriends, key : "No mutual friends yet")
        } else {
            lblNoMutualFriends.text = "\(productDetail.countMutualFriend!) mutual friends"
        }
        
        if productDetail.isSold == "Y" {
            iconProduct.isSelected = true
            LocalizedControl(view: btnSold, key : "SOLD")
        } else {
            iconProduct.isSelected = false
            LocalizedControl(view: btnSold, key : "NOT SOLD")
        }
        
        if isMyProduct == true {
            iconProduct.isUserInteractionEnabled = true
        } else {
            iconProduct.isUserInteractionEnabled = false
        }
        iconProduct.isUserInteractionEnabled = !iconProduct.isHidden
        
        lblProductDescription.text = productDetail.productDescription
        
        if productDetail.productLongitude != nil && productDetail.productLatitude != nil {
            addAnnotation(title: productDetail.productName, coordinate: CLLocationCoordinate2D(latitude: Double(productDetail.productLatitude)!, longitude: Double(productDetail.productLongitude)!))
        }
        
        if productDetail.isLiked == "U" { //unlike
            self.btnLike.setImage(#imageLiteral(resourceName: "ic_like_small"), for: .normal)
            self.btnLike.tintColor = UIColor.white
        } else { //like
            self.btnLike.setImage(#imageLiteral(resourceName: "ic_like_fill_small"), for: .normal)
            self.btnLike.tintColor = kColorLike
        }
        
        if productDetail.addedCart == "N" { //unlike
            self.btnCart.setImage(#imageLiteral(resourceName: "ic_cart"), for: .normal)
            self.btnCart.tintColor = UIColor.white
        } else { //like
            self.btnCart.setImage(#imageLiteral(resourceName: "ic_cart_fill"), for: .normal)
            self.btnCart.tintColor = kColorLike
        }
        
        if productDetail.verifyFacebook == "Y" {
            btnFacebook.setImage(#imageLiteral(resourceName: "ic_social_facebook_active"), for: UIControlState.normal)
        }
        
        if productDetail.verifyGoogle == "Y" {
            btnGoogle.setImage(#imageLiteral(resourceName: "ic_social_google_active"), for: UIControlState.normal)
        }
        
        if productDetail.productOwnerId == String(userId) {
            iconProduct.isHidden = false
            iconProduct.isUserInteractionEnabled = true
        } else {
            iconProduct.isHidden = true
        }
    }
    
    func addAnnotation(title : String, coordinate : CLLocationCoordinate2D) {
        
        let annotation = MKPointAnnotation()
        annotation.coordinate = coordinate
        annotation.title = title
        self.mapProduct.addAnnotation(annotation)
        
        let miles : Double = 5.0;
        let scalingFactor : Double = abs(cos(2 * M_PI * coordinate.latitude / 360.0))
        
        var span : MKCoordinateSpan = MKCoordinateSpan()
        
        span.latitudeDelta = miles/69.0;
        span.longitudeDelta = miles/(scalingFactor * 69.0);
        
        var region : MKCoordinateRegion = MKCoordinateRegion()
        region.span = span;
        region.center = coordinate;
            
        self.mapProduct?.setRegion(region, animated: false)
    }
    
    func requestForProductDetail() {
        
        if userId == 0 {
            messageBox(message: "Your device is not registered yet, please try again")
            return;
        }
        
        let request = "\(kAPIProductDetail)"
        
        let requestParam = RequestParamModal()
        requestParam.action = kAPIProductDetail
        requestParam.userId = String(userId)
        requestParam.productId = product.productId
        
        RequestManager.singleton.requestPOST(requestMethod: request, parameter: requestParam.toDictionary(), showLoader: true, successBlock: { (resposne :MessageModal, message : String?) in
            
            self.collectionImages.delegate = self
            self.collectionImages.dataSource = self
            
            self.productDetail = resposne.productDetail
            self.collectionImages.reloadData()
            
            self.configureData()
            
        }, failureBlock: {(error : Error?) in
            
        })
    }
    
    //MARK:- Gesture
    @IBAction func mapTapped(_ sender: UITapGestureRecognizer) {
        
        let controller = NavigationManager.singleton.screen(screenType: NavigationScreenType.NavigationScreenTypeMap) as! MapVC
        
        if productDetail.productLongitude != nil && productDetail.productLatitude != nil {
            controller.productName = productDetail.productName
            controller.locationCoordinate2D = CLLocationCoordinate2D(latitude: Double(productDetail.productLatitude)!, longitude: Double(productDetail.productLongitude)!)
        }
        navigationController?.pushViewController(controller, animated: true)
    }
    
    //MARK:- Action Methods
    @IBAction func btnFollowTapped(_ sender: UIButton) {
        
        let productName = productDetail.productName ?? ""
        let productPrice = productDetail.productPrice ?? "0"
        let productCategory = productDetail.category ?? ""
        
        let stringToShare = "Product name : \(productName)\n".appending("Price : \(productPrice)\n").appending("Category : \(productCategory)\n")
        
        let imageURL = URL(string: productDetail.productImage[0])
        
        if imageURL != nil {
            
            let imageView = UIImageView()
            let imgDownloadRequest = URLRequest(url:imageURL!)
            
            imageView.setImageWith(imgDownloadRequest, placeholderImage: #imageLiteral(resourceName: "logo"), success: { (request : URLRequest, response : HTTPURLResponse?, image : UIImage) in
            
                NavigationManager.singleton.shareIt(view: self, message: stringToShare, image: image)
            
            }, failure: { (request : URLRequest, response : HTTPURLResponse?, error : Error) in
                
                NavigationManager.singleton.shareIt(view: self, message: stringToShare, image: nil)
            })
        } else {
            
            NavigationManager.singleton.shareIt(view: self, message: stringToShare, image: nil)
        }
    }
    
    @IBAction func btnLikeTapped(_ sender: UIButton) {
        
        let request = "\(kAPILikeProduct)"
        
        let requestParam = RequestParamModal()
        requestParam.action = kAPILikeProduct
        requestParam.userId = String(userId)
        requestParam.productSmallId = product.productId
        
        if productDetail.isLiked == "L" { //unlike
            
            requestParam.type = "U"
            RequestManager.singleton.requestPOST(requestMethod: request, parameter: requestParam.toDictionary(), showLoader: true, successBlock: { (resposne :MessageModal, message : String?) in
                
                self.productDetail.isLiked = "U"
                self.btnLike.setImage(#imageLiteral(resourceName: "ic_like_small"), for: .normal)
                self.btnLike.tintColor = UIColor.white
                
            }, failureBlock: {(error : Error?) in
                
            })
            
        } else { //like
            
            requestParam.type = "L"
            RequestManager.singleton.requestPOST(requestMethod: request, parameter: requestParam.toDictionary(), showLoader: true, successBlock: { (resposne :MessageModal, message : String?) in
                
                self.productDetail.isLiked = "L"
                self.btnLike.setImage(#imageLiteral(resourceName: "ic_like_fill_small"), for: .normal)
                self.btnLike.tintColor = kColorLike
                
            }, failureBlock: {(error : Error?) in
                
            })
        }
    }
    
    @IBAction func btnCartTapped(_ sender: UIButton) {
        
        if productDetail.addedCart == "Y" {
            //added already
            showAlertOk("You already added this product.".localized())
            
        } else if productDetail.userQuantity.toInt()! < 1 {
            //no count
            showAlertOk("There is no product.".localized())
            
        } else { //add
            var message = "You can claim only".localized()
            message += " \(productDetail.userQuantity!) " + "quantites".localized()
            
            //Step : 1
            let alert = UIAlertController(title: kAppName, message: message, preferredStyle: .alert)
            alert.addTextField { (textField : UITextField!) -> Void in
                textField.placeholder = "Please input quantity"
                textField.keyboardType = .numberPad
            }
            alert.addAction (UIAlertAction(title: "ADD TO CART".localized(), style: .default) { (alertAction) in
                let cartStr: String = alert.textFields![0].text!
                
                if cartStr != "" {
                    if cartStr.toInt()! > self.productDetail.userQuantity!.toInt()! {
                        self.showAlertOk("Over quantities to add to the cart".localized())
                    } else {
                        self.addToCart(cartStr)
                    }
                } else {
                    self.showAlertOk("Please input claim count.".localized())
                }
            })

            //Cancel action
            alert.addAction(UIAlertAction(title: kCancel, style: .default) { (alertAction) in })
            self.present(alert, animated:true, completion: nil)
        }
    }
    
    @IBAction func btnSeeProfileTapped(_ sender: UIButton) {
        
        let controller = NavigationManager.singleton.screen(screenType: NavigationScreenType.NavigationScreenTypeProfile) as! ProfileVC
        let userProfile = UserProfile(fromDictionary: [:])
        userProfile.userid = productDetail.productOwnerId
        userProfile.username = productDetail.productOwnerName
        userProfile.userimage = productDetail.productOwnerImage
        controller.userProfile = userProfile
        controller.mobile =  productDetail.productOwnerPhone
        
        navigationController?.pushViewController(controller, animated: true)
    }
    
    @IBAction func btnSoldTapped(_ sender: UIButton) {
        
        if isMyProduct == false {
            return
        }
        
        let request = "\(kAPISoldProduct)"
        
        let requestParam = RequestParamModal()
        requestParam.action = kAPISoldProduct
        requestParam.userId = String(userId)
        requestParam.productSmallId = product.productId
        
        if productDetail.isSold == "Y" { //unlike
            
            requestParam.type = "N"
            RequestManager.singleton.requestPOST(requestMethod: request, parameter: requestParam.toDictionary(), showLoader: true, successBlock: { (resposne :MessageModal, message : String?) in
                
                self.productDetail.isSold = "N"
                LocalizedControl(view: self.btnSold, key : "NOT SOLD")
                
            }, failureBlock: {(error : Error?) in
                
            })
            
        } else { //like
            
            requestParam.type = "Y"
            RequestManager.singleton.requestPOST(requestMethod: request, parameter: requestParam.toDictionary(), showLoader: true, successBlock: { (resposne :MessageModal, message : String?) in
                
                self.productDetail.isSold = "Y"
                LocalizedControl(view: self.btnSold, key : "SOLD")
                
            }, failureBlock: {(error : Error?) in
                
            })
        }
    }        
    
    @IBAction func iconSoldTapped(_ sender: UIButton) {
        
        if isMyProduct == false {
            return
        }
        
        let request = "\(kAPISoldProduct)"
        
        let requestParam = RequestParamModal()
        requestParam.action = kAPISoldProduct
        requestParam.userId = String(userId)
        requestParam.productSmallId = product.productId
        
        if productDetail.isSold == "Y" { //unlike
            
            requestParam.type = "N"
            RequestManager.singleton.requestPOST(requestMethod: request, parameter: requestParam.toDictionary(), showLoader: true, successBlock: { (resposne :MessageModal, message : String?) in
                
                self.productDetail.isSold = "N"
                self.iconProduct.isSelected = false
                LocalizedControl(view: self.btnSold, key : "NOT SOLD")
                
            }, failureBlock: {(error : Error?) in
                
            })
            
        } else { //like
            
            requestParam.type = "Y"
            RequestManager.singleton.requestPOST(requestMethod: request, parameter: requestParam.toDictionary(), showLoader: true, successBlock: { (resposne :MessageModal, message : String?) in
                
                self.productDetail.isSold = "Y"
                self.iconProduct.isSelected = true
                LocalizedControl(view: self.btnSold, key : "SOLD")
                
            }, failureBlock: {(error : Error?) in
                
            })
        }
    }
    
    @IBAction func shareFB(_ sender: UIButton) {
        
        /*let productName = productDetail.productName ?? ""
        let productPrice = productDetail.productPrice ?? "0"
        let productCategory = productDetail.category ?? ""
        
        let stringToShare = "Product name : \(productName)\n".appending("Price : \(productPrice)\n").appending("Category : \(productCategory)\n")
        NavigationManager.singleton.shareIt(view: self, message: stringToShare)*/
    }
    
    @IBAction func shareMessanger(_ sender: UIButton) {
    
        /*let productName = productDetail.productName ?? ""
        let productPrice = productDetail.productPrice ?? "0"
        let productCategory = productDetail.category ?? ""
        
        let stringToShare = "Product name : \(productName)\n".appending("Price : \(productPrice)\n").appending("Category : \(productCategory)\n")
        NavigationManager.singleton.shareIt(view: self, message: stringToShare)*/
    }
    
    @IBAction func shareGroup(_ sender: UIButton) {
        
        let productName = productDetail.productName ?? ""
        let productPrice = productDetail.productPrice ?? "0"
        let productCategory = productDetail.category ?? ""
        
        let stringToShare = "Product name : \(productName)\n".appending("Price : \(productPrice)\n").appending("Category : \(productCategory)\n")
        NavigationManager.singleton.shareIt(view: self, message: stringToShare, image: nil)
    }
    
    @IBAction func btnCallTapped(_ sender: UIButton) {
        
        let busPhone = productDetail.productOwnerPhone
        
        if busPhone != nil {
            
            if let url = URL(string: "tel://\(busPhone!)") {
                
                if UIApplication.shared.canOpenURL(url) == true {
                    UIApplication.shared.openURL(url)
                }
            }
        }
    }
    
    @IBAction func btnSMSTapped(_ sender: UIButton) {
        
        let busPhone = productDetail.productOwnerPhone
        
        if (MFMessageComposeViewController.canSendText() && busPhone != nil) {
            
            let controller = MFMessageComposeViewController()
            controller.body = ""
            controller.recipients = [busPhone!]
            controller.messageComposeDelegate = self
            present(controller, animated: true, completion: nil)
        }
    }
        
    func addToCart(_ count: String) {
        
        let requestParam = RequestParamModal()
        requestParam.userId = String(userId)
        requestParam.productSmallId = product.productId
        requestParam.cartNum = count
        
        let request = "\(kAPIAddCart)"
        requestParam.action = kAPIAddCart
        RequestManager.singleton.requestPOST(requestMethod: request, parameter: requestParam.toDictionary(), showLoader: true, successBlock: { (resposne :MessageModal, message : String?) in
            
            self.productDetail.addedCart = "Y"
            self.btnCart.setImage(#imageLiteral(resourceName: "ic_cart_fill"), for: .normal)
            self.btnCart.tintColor = kColorLike
            self.lblClaim.text = "Claimed: " + count
            
            let a1: Int = self.productDetail.userQuantity!.toInt()!
            let a2: Int = count.toInt()!
            self.lblQTY.text = "QTY: \(a1 - a2)"
            
        }, failureBlock: {(error : Error?) in
            
        })
    }
    
    //MARK:- MFMessageComposeViewControllerDelegate
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        controller.dismiss(animated: true) {}
    }
    
    //MARK:- UIView Life Cycle Method
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureUI()
        requestForProductDetail()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = false
    }
}

extension ProfileDetailVC : UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let width = UIScreen.main.bounds.width
        let height : CGFloat = 372
        return CGSize(width: width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let controller = NavigationManager.singleton.screen(screenType: NavigationScreenType.NavigationScreenTypeImageSlider) as! ImageSliderVC
        controller.productDetail = productDetail
        controller.currentIndexPath = indexPath
        present(controller, animated: true, completion: nil)
    }
}

extension ProfileDetailVC : UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return productDetail.productImage.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String.className(ProfileDetailCell.self), for: indexPath) as! ProfileDetailCell
        
        let imageURL = URL(string: productDetail.productImage[indexPath.row])
        
        if imageURL != nil {
            //cell.imgItem .setImageWith(imageURL!, placeholderImage: #imageLiteral(resourceName: "logo"))
            let imgDownloadRequest = URLRequest(url:imageURL!)
                cell.imgItem.setImageWith(imgDownloadRequest, placeholderImage: #imageLiteral(resourceName: "logo"), success: { (request : URLRequest, response : HTTPURLResponse?, image : UIImage) in
                
               //cell.imgItem.image = Toucan(image: image).maskWithRoundedRect(cornerRadius: 5).image
                cell.imgItem.image = Toucan(image: image).resizeByCropping(cell.imgItem.bounds.size).image
                
            }, failure: { (request : URLRequest, response : HTTPURLResponse?, error : Error) in
                
            })
        }
        return cell;
    }
}

extension ProductDetailModal : MKMapViewDelegate {

    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        guard !(annotation is MKUserLocation) else { return nil }
        
        let identifier = "ProductDetailAnnotation"
        
        var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier) as? MKPinAnnotationView
        
        if annotationView == nil {
            annotationView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: identifier)
            annotationView?.animatesDrop = true
            annotationView?.isDraggable = true
            annotationView?.canShowCallout = false
            annotationView?.rightCalloutAccessoryView = UIButton(type: .infoLight)
        } else {
            annotationView?.annotation = annotation
        }
        
        return annotationView
    }
    
    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, didChange newState: MKAnnotationViewDragState, fromOldState oldState: MKAnnotationViewDragState) {
        switch newState {
        case .starting:
            view.dragState = .dragging
        case .ending, .canceling:
            view.dragState = .none
        default: break
        }
    }
}

