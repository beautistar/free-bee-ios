//
//  ImageSliderVC.swift
//  HT
//
//  Created by Dharmesh on 05/02/17.
//  Copyright © 2017 dharmesh. All rights reserved.
//

import UIKit
import Foundation
import Toucan

class ImageSliderCell : UICollectionViewCell {
    
    @IBOutlet weak var imgItem: UIImageView!
    
    //MARK:- Memory Management Method
    
    deinit { //same like dealloc in ObjectiveC
        
    }
    
    //MARK:- Initialisation
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        imgItem.clipsToBounds = true
    }
    
}

class ImageSliderVC : BaseVC {
    
    @IBOutlet weak var imageSliderCollection: UICollectionView!
    @IBOutlet weak var pageController: UIPageControl!
    
    var productDetail : ProductDetailModal!
    var currentIndexPath : IndexPath!
    
    //MARK: Memory Management Method
    
    override func didReceiveMemoryWarning() {
        
    }
    
    deinit { //same like dealloc in ObjectiveC
        
    }
    
    //MARK: Action Methods
    
    @IBAction func btnCloseTapped(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    //MARK:- UIView Life Cycle Method
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        pageController.numberOfPages = productDetail.productImage.count
        pageController.currentPage = currentIndexPath.row
        imageSliderCollection.scrollToItem(at: currentIndexPath, at: UICollectionViewScrollPosition.centeredHorizontally, animated: false)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
}

extension ImageSliderVC : UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let width = ScreenRect.width
        let height : CGFloat = ScreenRect.height
        return CGSize(width: width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let cell = collectionView.cellForItem(at: indexPath) as! ImageSliderCell
        
        let jtsImageInfo = JTSImageInfo()
        jtsImageInfo.image = cell.imgItem.image
        jtsImageInfo.referenceRect =  cell.imgItem.frame
        jtsImageInfo.referenceView =  cell.imgItem.superview
        
        let controller = JTSImageViewController(imageInfo: jtsImageInfo, mode: JTSImageViewControllerMode.image, backgroundStyle: JTSImageViewControllerBackgroundOptions.blurred)
        controller?.show(from: self, transition: JTSImageViewControllerTransition.fromOriginalPosition)
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
        let index = scrollView.contentOffset.x / ScreenRect.width
        currentIndexPath = IndexPath(row: Int(index), section: 0)
        pageController.currentPage = currentIndexPath.row
    }
}

extension ImageSliderVC : UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return productDetail.productImage.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String.className(ImageSliderCell.self), for: indexPath) as! ImageSliderCell
        
        let imageURL = URL(string: productDetail.productImage[indexPath.row])
        
        if imageURL != nil {
            //cell.imgItem .setImageWith(imageURL!, placeholderImage: #imageLiteral(resourceName: "logo"))
            let imgDownloadRequest = URLRequest(url:imageURL!)
            cell.imgItem.setImageWith(imgDownloadRequest, placeholderImage: #imageLiteral(resourceName: "logo"), success: { (request : URLRequest, response : HTTPURLResponse?, image : UIImage) in
                
                //cell.imgItem.image = Toucan(image: image).maskWithRoundedRect(cornerRadius: 5).image
                cell.imgItem.image = Toucan(image: image).resizeByCropping(cell.imgItem.bounds.size).image
                
            }, failure: { (request : URLRequest, response : HTTPURLResponse?, error : Error) in
                
            })
        }
        return cell;
    }
}
