//
//  FewQuestion5VC.swift
//  HT
//
//  Created by Developer on 11/7/19.
//  Copyright © 2019 dharmesh. All rights reserved.
//

import UIKit
import Foundation

class FewQuestion5VC: BaseVC {
    
    @IBOutlet weak var ui_OwnBtn: UIButton!
    @IBOutlet weak var ui_RentBtn: UIButton!
    
    @IBOutlet weak var ui_yesBtn: UIButton!
    @IBOutlet weak var ui_NoBtn: UIButton!
    @IBOutlet weak var yesnoView: UIView!
    @IBOutlet weak var lbl_selling: UILabel!
    
    var valOwnRent = 0
    var valSeling = 0
    var selectedColer = "#fdb913"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        lbl_selling.isHidden = true
        yesnoView.isHidden = true
    }
    
    @IBAction func onClickRentBtn(_ sender: UIButton) {
        
        valOwnRent = sender.tag
        var selingLbl = ""
        if sender.tag == 1 {
            ui_OwnBtn.backgroundColor = UIColor.init(hex: selectedColer, alpha: 1)
            ui_RentBtn.backgroundColor = .white
            selingLbl = "Do you plan on selling in the next year?"

        } else if sender.tag == 2 {
            ui_RentBtn.backgroundColor = UIColor.init(hex: selectedColer, alpha: 1)
            ui_OwnBtn.backgroundColor = .white
            selingLbl = "Do you plan on buying a home\n in the next year?"
        }

        yesnoView.isHidden = false
        lbl_selling.isHidden = false
        lbl_selling.text = selingLbl
        
    }
    
    @IBAction func onClickYESNOBtn(_ sender: UIButton) {
        
        valSeling = sender.tag

        if sender.tag == 3 {

            ui_yesBtn.backgroundColor = UIColor.init(hex: selectedColer, alpha: 1)
            ui_NoBtn.backgroundColor = .white

        } else if sender.tag == 4 {
            ui_NoBtn.backgroundColor = UIColor.init(hex: selectedColer, alpha: 1)
            ui_yesBtn.backgroundColor = .white
        }
    }
    
    @IBAction func onOwnNext(_ sender: Any) {
        
        gotoFewVC("LoginVC")
    }
    
    @IBAction func onClickBack(_ sender: Any) {
        gotoFewVC("LoginVC")
    }
    
    @IBAction func onClickLeft(_ sender: Any) {
        gotoFewVC("FewQuestion4VC")
    }
    
    @IBAction func onClickRight(_ sender: Any) {
        gotoFewVC("LoginVC")
        
    }
}
