//
//  FewQuestiont3VC.swift
//  HT
//
//  Created by Developer on 11/6/19.
//  Copyright © 2019 dharmesh. All rights reserved.
//

import UIKit
import Foundation

class FewQuestiont3VC: BaseVC {
    
    var selectedPet = [Int]()
    
    @IBOutlet weak var petView: UIView!
    @IBOutlet weak var YesBtn: UIButton!
    @IBOutlet weak var NoBtn: UIButton!
    @IBOutlet weak var dogBtn: UIButton!
    @IBOutlet weak var catBtn: UIButton!
    @IBOutlet weak var BirdBtn: UIButton!
    @IBOutlet weak var FishBtn: UIButton!
    @IBOutlet weak var OtherBtn: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        petView.isHidden = true
    }
    
    @IBAction func onClickYesBtn(_ sender: Any) {
        YesBtn.backgroundColor = UIColor.init(hex: "#fdb913", alpha: 1)
        NoBtn.backgroundColor = .white
        petView.isHidden = false
    }
    @IBAction func onClickNoBtn(_ sender: Any) {
        
        YesBtn.backgroundColor = .white
        NoBtn.backgroundColor = UIColor.init(hex: "#fdb913", alpha: 1)
        petView.isHidden = true
    }
    
    
    @IBAction func onClickdogBtn(_ sender: UIButton) {
        
        let petNum = sender.tag
        
        if selectedPet.contains(petNum) {
            sender.backgroundColor = .white
            selectedPet.remove(at: selectedPet.indexes(of: petNum)[0])
            
        } else {
            sender.backgroundColor = UIColor(named: "primaryColor")
            selectedPet.append(petNum)
        }
    }
    
    @IBAction func onTapedNext(_ sender: Any) {
        gotoFewVC("FewQuestion4VC")
    }
    
    @IBAction func onClickLeftBtn(_ sender: Any) {
        gotoFewVC("FewQuestion2VC")
    }
    
    @IBAction func onClickRightBtn(_ sender: Any) {
        gotoFewVC("FewQuestion4VC")
    }
    
    @IBAction func onTapedBack(_ sender: Any) {
        gotoFewVC("LoginVC")
    }
    
}

extension Array where Element: Equatable {

    func indexes(of item: Element) -> [Int]  {
        return enumerated().compactMap { $0.element == item ? $0.offset : nil }
    }
}
