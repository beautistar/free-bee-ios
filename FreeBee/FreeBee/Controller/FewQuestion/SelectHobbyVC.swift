//
//  SelectHobbyVC.swift
//  HT
//
//  Created by Developer on 11/3/19.
//  Copyright © 2019 dharmesh. All rights reserved.
//
import Foundation
import UIKit
var hobbyData =  [HobbyModel]()

class SelectHobbyVC: BaseVC {

    var parentVC: FewQuestion1VC? = nil
    var count = 0
    
    @IBOutlet weak var ui_lblSectedState: UILabel!
    @IBOutlet weak var ui_collectionView: UICollectionView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        ui_collectionView.dataSource = self
        ui_collectionView.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        fetchData()
    }
    
    func fetchData() {

        hobbyData.removeAll()
        
        for one in hobbys {
            let model = HobbyModel(topic: one, state: false)
            hobbyData.append(model)
        }
        ui_collectionView.reloadData()
    }
    
    @IBAction func onTapedClose(_ sender: Any) {
        if let parentVC = parentVC {
            parentVC.closeHobbyVC()
        }
    }
    
    @IBAction func onTapedNext(_ sender: Any) {
        if let parentVC = parentVC {
            parentVC.setBobbyData()
        }
    }
    
}

extension SelectHobbyVC : UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return hobbyData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HobbyCell", for: indexPath) as! HobbyCell
        cell.entity = hobbyData[indexPath.row]
        
        return cell
    }
}

extension SelectHobbyVC : UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if self.count >= 20 {
            return
        }
        
        hobbyData[indexPath.row].selectState = !hobbyData[indexPath.row].selectState
        
        if hobbyData[indexPath.row].selectState {
            self.count += 1
        } else {
            self.count -= 1
        }

        self.ui_lblSectedState.text = "\(count) selected"
        self.ui_collectionView.reloadData()
    }
}

extension SelectHobbyVC: UICollectionViewDelegateFlowLayout {

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let w = CGFloat(hobbyData[indexPath.row].topic.count * 9 + 10)
        
        return CGSize(width: w, height: 30)
    }
}
