//
//  FewQuestionVC.swift
//  HT
//
//  Created by Developer on 11/2/19.
//  Copyright © 2019 dharmesh. All rights reserved.
//
import Foundation
import UIKit
import StepIndicator

class FewQuestion1VC: BaseVC {

    var selectHobbyVC: SelectHobbyVC!
    var userID = ""
    
    @IBOutlet weak var ui_selectHobbyNum: UILabel!
    @IBOutlet weak var ui_hobbyColl: UICollectionView!
    
    var selectedHobbys = [HobbyModel]()
    var selectedCount = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        ui_selectHobbyNum.text = ""
    }
    
    @IBAction func onTapedHobby(_ sender: Any) {
        selectHobbyVC = (self.storyboard?.instantiateViewController(withIdentifier: "SelectHobbyVC") as! SelectHobbyVC)
        selectHobbyVC.view.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)

        selectHobbyVC.parentVC = self

        UIView.animate(withDuration: 0.3, animations: {
            self.view.addSubview(self.selectHobbyVC.view)
        })
    }
    
    func closeHobbyVC() {
        UIView.animate(withDuration: 0.3, animations: {
            self.selectHobbyVC.view.removeFromSuperview()
        })
    }
    
    func setBobbyData(){
        closeHobbyVC()
        selectedHobbys.removeAll()
        selectedCount = 0
        for one in hobbyData {
            if one.selectState {
                selectedHobbys.append(one)
                selectedCount += 1
            }
        }
        ui_selectHobbyNum.text = "\(selectedCount) selected"
        ui_hobbyColl.reloadData()
    }
    
    @IBAction func onTapedBack(_ sender: Any) {
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let nextVC = storyBoard.instantiateViewController(withIdentifier: "LoginVC")
        nextVC.modalPresentationStyle = .fullScreen
        self.present(nextVC, animated: false, completion: nil)
    }
    
    @IBAction func onTapedNext(_ sender: Any) {

        gotoFewVC("FewQuestion2VC")
    }
    
    
    @IBAction func onClickLeft(_ sender: Any) {
        gotoFewVC("SignupVC")
    }
    
    @IBAction func onClickRight(_ sender: Any) {
        gotoFewVC("FewQuestion2VC")
    }
    
    @IBAction func onClickBack(_ sender: Any) {
        gotoFewVC("LoginVC")
    }
}

extension FewQuestion1VC : UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return selectedHobbys.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HobbyCell", for: indexPath) as! HobbyCell
        cell.entity = selectedHobbys[indexPath.row]
        
        return cell
    }
}

extension FewQuestion1VC : UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectedHobbys.remove(at: indexPath.row)
        self.selectedCount -= 1

        self.ui_selectHobbyNum.text = "\(selectedCount) selected"
        self.ui_hobbyColl.reloadData()
    }
}

extension FewQuestion1VC: UICollectionViewDelegateFlowLayout {

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let w = CGFloat(hobbyData[indexPath.row].topic.count * 10 + 10)
        
        return CGSize(width: w, height: 30)
    }
}
