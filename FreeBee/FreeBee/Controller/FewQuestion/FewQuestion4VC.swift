//
//  IndustryVC.swift
//  HT
//
//  Created by Developer on 11/6/19.
//  Copyright © 2019 dharmesh. All rights reserved.
//

import UIKit
import Foundation
import DropDown

class FewQuestion4VC: BaseVC {
    
    @IBOutlet weak var btnIndustry: UIButton!
    @IBOutlet weak var txtIndustry: UITextField!
    @IBOutlet weak var txtOccurence: UITextField!
    
    @IBOutlet weak var popupView: UIView!
    @IBOutlet weak var pupupTitle: UILabel!
    @IBOutlet weak var uiTableview: UITableView!
    @IBOutlet weak var uiCollectionView: UICollectionView!
    
    var showState = ""
    var filterData = [CategoryModel]()
    let dropDownIndustry = DropDown()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        uiTableview.dataSource = self
        uiTableview.delegate = self
        uiTableview.tableFooterView = UIView()
        
//        uiCollectionView.dataSource = self
//        uiCollectionView.delegate = self

        
        //MARK:-   Industry data set
        dropDownIndustry.dataSource = industryData
        dropDownIndustry.anchorView = btnIndustry
        dropDownIndustry.selectionAction = { [unowned self] (index, item) in
            if self.txtIndustry.text != item {
                self.txtOccurence.text = ""
            }
            self.txtIndustry.text = item
            self.txtIndustry.textColor = UIColor.init(hex: "#fdb913", alpha: 1)
            
        }
        dropDownIndustry.backgroundColor = UIColor.white
        dropDownIndustry.textColor = UIColor.black
        dropDownIndustry.separatorColor = kColorGray
    }
  
    //MARK:-custom action
    @IBAction func onTapedButton(_ sender: UIButton){

        if sender.tag == 1 {
            showState = "industry"
            dropDownIndustry.show()
            /*
            if showState == "industry" {} else {
                pupupTitle.text = "Choose the Industry"
                showState = "industry"
                filterData.removeAll()
                for one in industryData {
                    filterData.append(CategoryModel(index: 0, title: one, selected: false))
                }
                uiTableview.reloadData()
            }*/
            
        }
        else if sender.tag == 2 {
            uiTableview.isHidden = false
            uiCollectionView.isHidden = true
            if showState == "occupation" {} else {
                
                if txtIndustry.text! == "" {
                    messageBox(message: "Please choose the Industry.")
                    return
                }
                else {
                    showState = "occupation"
                    pupupTitle.text = "Choose the Occupation"
                    
                    filterData.removeAll()
                    
                    let dataOccupation = loadJsonFile("occupation")
                    let selectedIndustry = txtIndustry.text!.trimmed
                    var selectedOccupation = txtOccurence.text?.components(separatedBy: ";")
                    for index in 0 ..< selectedOccupation!.count {
                        selectedOccupation![index] = selectedOccupation![index].trimmed
                    }
                    
                    if dataOccupation!.keys.contains(selectedIndustry) {
                        for oneOccupation in dataOccupation![selectedIndustry] as! [String] {
                            let isSelected = selectedOccupation?.contains(oneOccupation)
                            filterData.append(CategoryModel(index: 0, title: oneOccupation, selected: isSelected!))
                        }
                        uiTableview.reloadData()
                        
                    } else {
                        // does not contain key
                        messageBox(message: "There is no occupation on selected industry.")
                        return
                    }
                }
            }

            popupView.isHidden = false
        }
        
    }
    
    @IBAction func onTapedCancel(_ sender: Any) {
        popupView.isHidden = true
    }
    
    @IBAction func onTapedSelect(_ sender: Any) {
        var tempTitle = ""
        
        for one in filterData {
            if one.selected  {
                if tempTitle.isEmpty {
                    tempTitle = one.title
                } else {
                    tempTitle = tempTitle + "; " + one.title
                }
            } else {}
        }
        
        if showState == "industry" {
            if txtIndustry.text == tempTitle {
                txtOccurence.text = ""
                txtOccurence.textColor = UIColor.init(hex: "#000", alpha: 1)
            }
            txtIndustry.text = tempTitle
            txtIndustry.textColor = UIColor.init(hex: "#fdb913", alpha: 1)
            
        } else if showState == "occupation"  {
            txtOccurence.text = tempTitle
            txtOccurence.textColor = UIColor.init(hex: "#fdb913", alpha: 1)
        }
        
        popupView.isHidden = true
    }
    
    @IBAction func onClickBack(_ sender: Any) {
        gotoFewVC("LoginVC")
    }
    
    @IBAction func onClickLeft(_ sender: Any) {
        gotoFewVC("FewQuestiont3VC")
    }
    
    @IBAction func onTapedNext(_ sender: Any) {
        gotoFewVC("FewQuestion5VC")
    }
    
    @IBAction func onClickRight(_ sender: Any) {
        gotoFewVC("FewQuestion5VC")
    }
    
}

//MARK:- UITableView
extension FewQuestion4VC : UITableViewDelegate {

    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {

        if filterData[indexPath.row].selected {
            cell.accessoryType = .checkmark
        } else {
            cell.accessoryType = .none
        }
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        filterData[indexPath.row].selected = !filterData[indexPath.row].selected
        uiTableview.reloadData()
    }
}

extension FewQuestion4VC : UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filterData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "CategoryCell") as! CategoryCell
        cell.entity = filterData[indexPath.row]
        
        return cell
    }
}
