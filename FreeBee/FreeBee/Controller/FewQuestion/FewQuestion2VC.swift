//
//  FewQuestion2VC.swift
//  HT
//
//  Created by Developer on 11/5/19.
//  Copyright © 2019 dharmesh. All rights reserved.
//

import UIKit
import Foundation

class FewQuestion2VC: BaseVC {
    
    @IBOutlet weak var btnYes: UIButton!
    @IBOutlet weak var btnNo: UIButton!
    @IBOutlet weak var childView: UIView!
    @IBOutlet weak var ui_buttonCollectionView: UICollectionView!
    @IBOutlet weak var ui_ageView: UIView!
    @IBOutlet weak var ui_ageColl: UICollectionView!
    
    var numbers = [ChildButtonModel]()
    var ageModels = [ChildInfoModel]()
   
    override func viewDidLoad() {
        super.viewDidLoad()
        childView.isHidden = true
        ui_ageView.isHidden = true
        loadNumberCollectionView()
    }
    
    func loadNumberCollectionView(){
        
        for i in 1 ..< 7 {
            let one = ChildButtonModel(number: "\(i)", state: false)
                numbers.append(one)
        }
        ui_buttonCollectionView.reloadData()
    }
    
    @IBAction func onClickYes(_ sender: Any) {
        
        btnYes.backgroundColor = UIColor.init(hex: "#fdb913", alpha: 1)
        btnNo.backgroundColor = .white
        
        childView.isHidden = false
        ui_ageView.isHidden = false
    }
    
    @IBAction func onClickNo(_ sender: Any) {
        
        btnNo.backgroundColor = UIColor.init(hex: "#fdb913", alpha: 1)
        btnYes.backgroundColor = .white
        childView.isHidden = true
        ui_ageView.isHidden = true
    }
    
    @IBAction func onTapedSex(_ sender: UIButton) {
        
        let orderNum = (sender.tag - sender.tag % 10) / 10
        let sexNum = sender.tag % 10
        
        ageModels[orderNum].selectedSex = sexNum
        ui_ageColl.reloadData()
        
    }
     
    @IBAction func onTapNext(_ sender: Any) {
        gotoFewVC("FewQuestiont3VC")
    }
    
    @IBAction func onClickBack(_ sender: Any) {
        gotoFewVC("LoginVC")
    }
    
       
    @IBAction func onClickLeftButton(_ sender: Any) {
        gotoFewVC("FewQuestion1VC")
    }
    
    @IBAction func onClickRightButton(_ sender: Any) {
        gotoFewVC("FewQuestiont3VC")
    }
    
}

//MARK: CollectionView

extension FewQuestion2VC : UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == ui_buttonCollectionView {
            return numbers.count
        } else {
            return ageModels.count
            
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == ui_buttonCollectionView {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ChildButtonCell", for: indexPath) as! ChildButtonCell
            cell.entity = numbers[indexPath.row]
            
            return cell
        } else {
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ChildInfoCell", for: indexPath) as! ChildInfoCell
            cell.ui_butMan.tag = indexPath.row * 10 + 1
            cell.ui_butWomen.tag = indexPath.row * 10 + 2
            cell.entity = ageModels[indexPath.row]
            
            return cell
            
        }
    }
}

extension FewQuestion2VC : UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if collectionView == ui_buttonCollectionView {
            
            for index in 0 ..< 6 {
                if index == indexPath.row {
                    self.numbers[index].selectState = true
                } else {
                    self.numbers[index].selectState = false
                }
            }
            
            self.ui_buttonCollectionView.reloadData()
            
            ageModels.removeAll()
            for i in 0 ..< indexPath.row + 1 {
                let one = ChildInfoModel(no: i + 1, selectedSex: 0, childAge: 0)
                ageModels.append(one)
            }
            self.ui_ageColl.reloadData()
            
        } else {
            
        }
        
    }
    
}

extension FewQuestion2VC: UICollectionViewDelegateFlowLayout {

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == ui_buttonCollectionView {
            return CGSize(width: 50, height: 50)
        }
        
        let w = (collectionView.bounds.width - 10) / 2
        
        return CGSize(width: w, height: 120)
    }
}

