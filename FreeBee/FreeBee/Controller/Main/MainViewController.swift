//
//  ViewController.swift
//  SlideMenuControllerSwift
//
//  Created by Yuji Hato on 12/3/14.
//

import UIKit
import SlideMenuControllerSwift
import Localize_Swift
import AFNetworking
import FCUUID
import Toucan
import MBProgressHUD

class MainViewControllerCell : UICollectionViewCell {
    
    @IBOutlet weak var imgItem: UIImageView!
    @IBOutlet weak var lblItemName: UILabel!
    
    var hud : MBProgressHUD!
    
    //MARK:- Memory Management Method
    
    deinit { //same like dealloc in ObjectiveC
        
    }
    
    //MARK:- Initialisation
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}

class MainViewController: BaseVC, PostVCDelegate, SearchDelegate, FilterVCDelegate {
    
    @IBOutlet weak var txfSearch: UITextField!
    @IBOutlet var rightItemsView: UIView!
    @IBOutlet weak var collectionItemsView: UICollectionView!
    @IBOutlet weak var bottomIndicator: UIActivityIndicatorView!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var uiMenuIcon: UIImageView!
    
    var hud : MBProgressHUD!
    var productItems : [ProductListModal] = []
    var productItemsOriginal : [ProductListModal] = []
    var searchRequestParam : RequestParamModal?
    var isReachToEnd : Bool = false
    
    let pageSize = 9
    let preloadMargin = 1
    var lastLoadedPage = 0
    
    @IBOutlet weak var layoutBottomGuide: NSLayoutConstraint!
    
    lazy var viewFullScreen : UIView = {
        let viewFullScreen = UIView(frame: UIScreen.main.bounds)
        viewFullScreen.backgroundColor = kColorBackground
        return viewFullScreen
    }()
    
    lazy var refreshControl: UIRefreshControl = {
        
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(handleRefresh(refreshControl:)), for: UIControlEvents.valueChanged)
        
        return refreshControl
    }()
    
    //MARK:- Memory Management Methods
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    deinit {
        MyNotificationCenter.removeObserver(self)
    }
    
    //MARK:- Custom Method
    @objc func configureUI() {
       
        self.title = "Home".localized()
        
        let rightItems = UIBarButtonItem(customView: rightItemsView)
        self.navigationItem.rightBarButtonItem = rightItems
        
        self.bottomIndicator.isHidden = true
        
        uiMenuIcon.image = uiMenuIcon.image?.imageWithColor(color: .white)
    }
    
    func requestForProductList(page : NSInteger, showLoader : Bool)  {
        
        let request = "\(kAPIProductList)"
        
        let requestParam = RequestParamModal()
        requestParam.action = kAPIProductList
        requestParam.age = userData!.userAge
        requestParam.gender = userData!.usergender
        requestParam.page = String(page + 1)

        self.hud = progressHUD(view: self.view, mode: .indeterminate)
        self.hud.show(animated: true)
        
        RequestManager.singleton.requestPOST(requestMethod: request, parameter: requestParam.toDictionary(), showLoader: false, successBlock: { (resposne :MessageModal, message : String?) in
            
            self.layoutBottomGuide.constant = 0
            self.refreshControl.endRefreshing()
            if page == 0 {
                self.productItems.removeAll()
            }
            
            self.hud.hide(animated: true)
            self.productItems.append(contentsOf: resposne.productList)
            
            print("productItems_count", self.productItems.count)
            self.productItemsOriginal = self.productItems
            self.bottomIndicator.isHidden = true
            self.collectionItemsView.reloadData()
            
        }, failureBlock: {(error : Error?) in
            self.hud.hide(animated: true)
            self.layoutBottomGuide.constant = 0
            self.bottomIndicator.isHidden = true
        })
    }
    
    func requestForSignup() {
        
        let request = "\(kAPIRegister)"
        
        let requestParam = RequestParamModal()
        requestParam.action = kAPIRegister
        //requestParam.deviceId = UIDevice.current.identifierForVendor?.uuidString
        //requestParam.deviceId = UUID().uuidString
        requestParam.deviceId = FCUUID.uuidForDevice()
        
        RequestManager.singleton.requestPOST(requestMethod: request, parameter: requestParam.toDictionary(), showLoader: true, successBlock: { (resposne :MessageModal, message : String?) in
            
            let currentUser = resposne.userInfo
            if currentUser != nil {
                
                if (currentUser!.userid != nil && currentUser!.userid != "0") {
                    MyUserDefaults.set(Int(currentUser!.userid), forKey: kCurrentUserId)
                }
                let userData = NSKeyedArchiver.archivedData(withRootObject: currentUser!.toDictionary())
                MyUserDefaults.setValue(userData, forKey: kCurrentUser)
                
                if currentUser?.username == "" {
                    
                    let controller = NavigationManager.singleton.screen(screenType: NavigationScreenType.NavigationScreenTypeEditProfile) as! EditProfileVC
                    self.present(controller, animated: false, completion: {
                        self.viewFullScreen.removeFromSuperview()
                    });
                }
                
            } else {
                
                let controller = NavigationManager.singleton.screen(screenType: NavigationScreenType.NavigationScreenTypeEditProfile) as! EditProfileVC
                self.present(controller, animated: false, completion: {
                    self.viewFullScreen.removeFromSuperview()
                });
            }
            self.requestForProductList(page: 0, showLoader: true)
            
        }, failureBlock: {(error : Error?) in
            
        })
    }
    
    func searchProduct(_ searchStr : String) {
        
        if searchStr.isEmpty {
            productItems = productItemsOriginal
        } else {
            var temp : [ProductListModal] = []
            for index in 0 ..< productItemsOriginal.count {
                if productItemsOriginal[index].productName.contains(searchStr) {
                    temp.append(productItemsOriginal[index])
                } else {}
            }
            productItems = temp
        }
        
        self.collectionItemsView.reloadData()
    }
    
    @objc func handleRefresh(refreshControl: UIRefreshControl) {
        
        isReachToEnd = false
        lastLoadedPage = 0
        requestForProductList(page: 0, showLoader : false)
    }
    
    //MARK:- Action Methods
    
    @IBAction func onTapedMenu(_ sender: Any) {
        self.slideMenuController()?.openLeft()
    }
    
    @IBAction func btnNetworkTapped(_ sender: UIButton) {
        
        let controller = NavigationManager.singleton.screen(screenType: NavigationScreenType.NavigationScreenTypeMyNetwords) as! MyNetworksVC
        controller.isMenuScreen = false
        navigationController?.pushViewController(controller, animated: true)
    }
    
    @IBAction func btnAddTapped(_ sender: UIButton) {
        
    }
    
    @IBAction func onFilterTaped(_ sender: UIButton) {
        
        let controller = NavigationManager.singleton.screen(screenType: NavigationScreenType.NavigationScreenTypeFilter) as! FilterVC
        controller.isMenuScreen = false
        controller.delegate = self
        navigationController?.pushViewController(controller, animated: true)
    }
    
    //MARK:- Notification
    @objc func profileTapped(notification : Notification) {
        
        let controller = NavigationManager.singleton.screen(screenType: NavigationScreenType.NavigationScreenTypeProfile) as! ProfileVC
        navigationController?.pushViewController(controller, animated: true)
    }
    
    //MARK:- PostVCDelegate
    func refreshProductList() {
        
        self.requestForProductList(page: 0, showLoader: false)
    }
    
    //MARK:- FilterDelegate   //SearchDelegate
    
    func applyFilterOnProductList(productItems: [ProductListModal], requestParam: RequestParamModal) {

        self.productItems = productItems
        productItemsOriginal = productItems
        self.collectionItemsView.reloadData()
    }
    
    //MARK:- UIViewController
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    
        if segue.identifier == "segueSearchVC" {
            
            let controller = segue.destination as! SearchVC        
            controller.requestParam = searchRequestParam
            controller.delegate = self
        } else if segue.identifier == "gotoPost" {
         
        
            let controller = segue.destination as! PostVC
            controller.isMenuScreen = false
            controller.delegate = self
            
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        txfSearch.delegate = self
        
        collectionItemsView.isHidden = true
        
        MyNotificationCenter.addObserver(self, selector: #selector(profileTapped), name: NSNotification.Name(rawValue: kNotificationHomeVC), object: nil)
        MyNotificationCenter.addObserver(self, selector: #selector(configureUI), name: NSNotification.Name( LCLLanguageChangeNotification), object: nil)
        configureUI()
        
        self.collectionItemsView.addSubview(self.refreshControl)
        self.collectionItemsView.alwaysBounceVertical = true
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1, execute: {
            
            self.collectionItemsView.isHidden = false
            if userId == 0 {
                self.requestForSignup()
            } else {
                self.requestForProductList(page: 0, showLoader: true)
            }
        })
        
        self.contentView.roundCorners([.topLeft, .topRight], radius: 20)
        if #available(iOS 11.0, *) {
            self.contentView.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        } else {
            // Fallback on earlier versions
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.setNavigationBarItem()
        
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
    }
    
}

extension MainViewController : UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if textField == txfSearch {
            searchProduct(textField.text!)
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == txfSearch {
            searchProduct(textField.text!)
        }
        return true
    }
    
}

extension MainViewController : UICollectionViewDelegateFlowLayout, UICollectionViewDelegate {
    
    //MARK:- UICollectionViewDelegateFlowLayout
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let width = (UIScreen.main.bounds.width - 30) / 2
        var height : CGFloat = width
        let product = productItems[indexPath.row]
        let myString: NSString = product.productName as NSString
        let size: CGSize = myString.size(withAttributes: [NSAttributedStringKey.strokeWidth:width ,NSAttributedStringKey.font: UIFont.systemFont(ofSize: 17.0)])
        
        return CGSize(width: width, height: height)
    }
    
    //MARK:- UICollectionViewDelegate
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let controller = NavigationManager.singleton.screen(screenType: NavigationScreenType.NavigationScreenTypeProfileDetail) as! ProfileDetailVC
        controller.product = productItems[indexPath.item]
        navigationController?.pushViewController(controller, animated: true)
    }
}

extension MainViewController : UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return productItems.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String.className(MainViewControllerCell.self), for: indexPath) as! MainViewControllerCell
        
        let product = productItems[indexPath.row]
        let imageURL = URL(string: product.productImage)
        if imageURL != nil {
            let imgDownloadRequest = URLRequest(url: imageURL!)
            cell.imgItem.setImageWith(imgDownloadRequest, placeholderImage: #imageLiteral(resourceName: "placeholder"), success: {(request: URLRequest, response: HTTPURLResponse?, image: UIImage) in
                
                cell.imgItem.image = Toucan(image: image).resizeByCropping(cell.imgItem.bounds.size).image
            }, failure: {(request: URLRequest, response: HTTPURLResponse?, error: Error) in })
        }
        cell.lblItemName.text = "   " + product.productName + "    "
        
        return cell;
    }
}


extension MainViewController : UIScrollViewDelegate {
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
        if let indexPath = collectionItemsView.indexPathsForVisibleItems.last {
            
            let nextPage: Int = Int(indexPath.row / pageSize) + 1
            let preloadIndex = nextPage * (pageSize - preloadMargin)
            
            if (indexPath.item >= preloadIndex && lastLoadedPage < nextPage) {
                
                layoutBottomGuide.constant = 50
                lastLoadedPage = nextPage
                bottomIndicator.isHidden = false
                requestForProductList(page: nextPage, showLoader: false)
            }
        }
    }
}

/*extension MainViewController : SlideMenuControllerDelegate {
    
    func leftWillOpen() {
        print("SlideMenuControllerDelegate: leftWillOpen")
    }
    
    func leftDidOpen() {
        print("SlideMenuControllerDelegate: leftDidOpen")
    }
    
    func leftWillClose() {
        print("SlideMenuControllerDelegate: leftWillClose")
    }
    
    func leftDidClose() {
        print("SlideMenuControllerDelegate: leftDidClose")
    }
    
    func rightWillOpen() {
        print("SlideMenuControllerDelegate: rightWillOpen")
    }
    
    func rightDidOpen() {
        print("SlideMenuControllerDelegate: rightDidOpen")
    }
    
    func rightWillClose() {
        print("SlideMenuControllerDelegate: rightWillClose")
    }
    
    func rightDidClose() {
        print("SlideMenuControllerDelegate: rightDidClose")
    }
}*/
