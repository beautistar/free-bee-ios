//
//  LeftViewController.swift
//  SlideMenuControllerSwift
//
//  Created by Yuji Hato on 12/3/14.
//

import UIKit
import Localize_Swift
import Toucan
import AFNetworking

protocol LeftMenuProtocol : class {
    func changeViewController(_ menu: NSInteger)
}

class LeftViewController : BaseVC, LeftMenuProtocol {
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var btnWelcome: UIButton!
    @IBOutlet weak var btnMyProfile: UIButton!
    @IBOutlet weak var lblListed: UILabel!
    @IBOutlet weak var lblListedCount: UILabel!
    @IBOutlet weak var lblLiked: UILabel!
    @IBOutlet weak var lblLikedCount: UILabel!
    @IBOutlet weak var lblBought: UILabel!
    @IBOutlet weak var lblBoughtCount: UILabel!
    
    @IBOutlet weak var uiListView: UIView!
    @IBOutlet weak var uiLinkView: UIView!
    @IBOutlet weak var uiBoughtView: UIView!
    
    
    var items = ["Home", "Post", "My Products", "My Bundle", "Help", "Invite Friends", "Settings"]
//    var icons = ["ic_menu_home", "ic_menu_camera", "ic_menu_people_know", "ic_menu_help", "ic_menu_share", "ic_menu_settings", "ic_menu_logout"]
    var icons = ["ic_home", "ic_post","ic_product" ,"ic_network", "ic_help", "ic_invate", "ic_setting"]
    
    var currentNavigationVC: UINavigationController!
    
    var navigationMainVC: UINavigationController!
    var navigationPostVC: UINavigationController!
    
    var navigationProductListVC: UINavigationController!
    var navigationMyNetworkVC : UINavigationController!
    var navigationHelpVC : UINavigationController!
    var navigationInviteFriendsVC : UINavigationController!
    var navigationSettingsVC : UINavigationController!
    
    //MARK:- Memory Management Methods
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    deinit {
         MyNotificationCenter.removeObserver(self)
    }
    
    //MARK:- Custom Methods
    @objc func configureUI() {
        
        LocalizedControl(view: lblListed, key: "Listed")
        LocalizedControl(view: lblBought, key: "Claimed")
        LocalizedControl(view: btnMyProfile, key: "My Profile")
        LocalizedControl(view: btnWelcome, key: "Welcome")
        
        imgUser.cornerRadius = imgUser.height/2
        uiListView.cornerRadius = uiListView.height/2
        uiLinkView.cornerRadius = uiLinkView.height/2
        uiBoughtView.cornerRadius = uiBoughtView.height/2
        
        
        tableView.reloadData()
    }
    
    //MARK:- Action Methods
    
    @IBAction func btnProfileTapped(_ sender: UIButton) {
        
        self.slideMenuController()?.closeLeft()
        switch currentNavigationVC {
        case navigationMainVC:
            MyNotificationCenter.post(name:  NSNotification.Name(rawValue: kNotificationHomeVC), object: nil)
            break
        case navigationPostVC:
            MyNotificationCenter.post(name:  NSNotification.Name(rawValue: kNotificationPostVC), object: nil)
            break
        case navigationProductListVC:
            MyNotificationCenter.post(name:  NSNotification.Name(rawValue: kNotificationProductListVC), object: nil)
            break
        case navigationMyNetworkVC:
            MyNotificationCenter.post(name:  NSNotification.Name(rawValue: kNotificationMyNetworkVC), object: nil)
            break
        case navigationHelpVC:
            MyNotificationCenter.post(name:  NSNotification.Name(rawValue: kNotificationHelpVC), object: nil)
            break
        case navigationSettingsVC:
            MyNotificationCenter.post(name:  NSNotification.Name(rawValue: kNotificationSettingsVC), object: nil)
            break
        default:
            break
        }
    }
    
    @IBAction func onLogoutTaped(_ sender: Any) {
        MyUserDefaults.removeObject(forKey: kCurrentUser);
        MyUserDefaults.set(Int("0"), forKey: kCurrentUserId)
        MyUserDefaults.synchronize()
        NavigationManager.singleton.setupLogin()
    }
    
    //MARK:- UIViewController
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
         MyNotificationCenter.addObserver(self, selector: #selector(configureUI), name: NSNotification.Name( LCLLanguageChangeNotification), object: nil)
        
        configureUI()
        
        //TODO: showing the item you like!
        self.tableView.separatorColor = UIColor(red: 224/255, green: 224/255, blue: 224/255, alpha: 1.0)
        
        navigationMainVC = NavigationManager.singleton.navigation(screenType: NavigationType.NavigationTypeHome)
        navigationPostVC = NavigationManager.singleton.navigation(screenType: NavigationType.NavigationTypePost)
        navigationProductListVC = NavigationManager.singleton.navigation(screenType: NavigationType.NavigationTypeProduct)
        navigationMyNetworkVC = NavigationManager.singleton.navigation(screenType: NavigationType.NavigationTypeMyNetwords)
        navigationHelpVC =  NavigationManager.singleton.navigation(screenType: NavigationType.NavigationTypeHelp)
        navigationInviteFriendsVC = NavigationManager.singleton.navigation(screenType: NavigationType.NavigationTypeInviteFriends)
        navigationSettingsVC = NavigationManager.singleton.navigation(screenType: NavigationType.NavigationTypeSettings)
        
        /*let nonMenuController = storyboard.instantiateViewController(withIdentifier: "NonMenuController") as! NonMenuController
        nonMenuController.delegate = self
        self.nonMenuViewController = UINavigationController(rootViewController: nonMenuController)*/
        
        currentNavigationVC = navigationMainVC
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if userData != nil {
            
            //image
            if userData!.userimage != nil && URL(string: userData!.userimage) != nil {
                let imgDownloadRequest = URLRequest(url: URL(string: userData!.userimage)!)
                imgUser.setImageWith(imgDownloadRequest, placeholderImage: #imageLiteral(resourceName: "profile"), success: {(request: URLRequest, response: HTTPURLResponse?, image: UIImage) in
                    
                   self.imgUser.image = Toucan(image: image).maskWithEllipse().image
                }, failure: {(request : URLRequest, response : HTTPURLResponse?, error : Error) in })
                
            } else if (userData!.imgProfile != nil) {
                
                imgUser.image = userData!.imgProfile
            } else {
                imgUser.image = #imageLiteral(resourceName: "profile")
            }
            
            //name
            if userData?.username != nil {
                btnMyProfile.setTitle(userData!.username, for: .normal)
            }
            
            if AFNetworkReachabilityManager.shared().isReachable {
                
                let request = "\(kAPIProfile)"
                
                let requestParam = RequestParamModal()
                requestParam.action = kAPIProfile
                requestParam.userId = String(userId)
                
                RequestManager.singleton.requestPOST(requestMethod: request, parameter: requestParam.toDictionary(), showLoader: false, successBlock: { (resposne :MessageModal, message : String?) in
                    
                    self.lblListedCount.text = String(resposne.userProfile.listOfProducts.count)
                    self.lblLikedCount.text = String(resposne.userProfile.productLikes.count)
                    self.lblBoughtCount.text = String(resposne.userProfile.productBought.count)
                    
                }, failureBlock: {(error : Error?) in })
            }
            
        } else {
            imgUser.image = #imageLiteral(resourceName: "profile")
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    
    func changeViewController(_ menu: NSInteger) {
        
        switch menu {
        case 0:
            currentNavigationVC = navigationMainVC
            self.slideMenuController()?.changeMainViewController(self.navigationMainVC, close: true)
        case 1:
            currentNavigationVC = navigationPostVC
            self.slideMenuController()?.changeMainViewController(self.navigationPostVC, close: true)
            break
        case 2:
            currentNavigationVC = navigationProductListVC
            self.slideMenuController()?.changeMainViewController(self.navigationProductListVC, close: true)
            break
        case 3:
            currentNavigationVC = navigationMyNetworkVC
            self.slideMenuController()?.changeMainViewController(self.navigationMyNetworkVC, close: true)
            break
        case 4:
            currentNavigationVC = navigationHelpVC
            self.slideMenuController()?.changeMainViewController(self.navigationHelpVC, close: true)
            break
        case 5:
            //currentNavigationVC = navigationInviteFriendsVC
            //self.slideMenuController()?.changeMainViewController(self.navigationInviteFriendsVC, close: true)
            NavigationManager.singleton.shareIt(view: self, message: "", image: nil)
            break
        case 6:
            currentNavigationVC = navigationSettingsVC
            self.slideMenuController()?.changeMainViewController(self.navigationSettingsVC, close: true)
            break
        case 7:
            MyUserDefaults.removeObject(forKey: kCurrentUser);
            MyUserDefaults.set(Int("0"), forKey: kCurrentUserId)
            MyUserDefaults.synchronize()
            NavigationManager.singleton.setupLogin()
            break
        default :
            break
        }
    }
}

extension LeftViewController : UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.changeViewController(indexPath.row)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
       
        if self.tableView == scrollView {
            
        }
    }
}

extension LeftViewController : UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "LeftViewControllerCell")
        cell?.imageView?.image = UIImage(named: icons[indexPath.row])
        cell?.textLabel?.text = items[indexPath.row].localized()
        return cell!
    }
}
