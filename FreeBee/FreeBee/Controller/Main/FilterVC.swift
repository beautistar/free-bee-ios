//
//  FilterVC.swift
//  HT
//
//  Created by RMS on 11/11/19.
//  Copyright © 2019 dharmesh. All rights reserved.
//

import UIKit

protocol FilterDelegate {
    
    func applyFilterOnProductList(productItems : [ProductListModal], requestParam : RequestParamModal)
}
class FilterVC: BaseVC, UINavigationControllerDelegate {
    
    var filterData = [CategoryModel]()
    var isMenuScreen : Bool = true
    var delegate : FilterVCDelegate?
    var requestParam : RequestParamModal?
    
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var uiTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        uiTableView.delegate = self
        uiTableView.dataSource = self
        uiTableView.tableFooterView = UIView()
        
        contentView.roundCorners([.topLeft, .topRight], radius:  20)
        
        for i in 0 ..< categorys.count {
            filterData.append(CategoryModel(index: i, title: categorys[i], selected: false))
        }
        
        uiTableView.reloadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
        
        requestParam = requestParam ?? RequestParamModal()
    }
    
    @IBAction func onBackTaped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onFitlerTaped(_ sender: Any) {
        
        var filterStr = ""
        for index in 0 ..< filterData.count {
            if filterData[index].selected {
                if filterStr.isEmpty {
                    filterStr = filterData[index].title
                } else {
                    filterStr += ";" + filterData[index].title
                }
            }
        }
        
        if filterStr.isEmpty == true {
            messageBox(message: "Please select filters")
            return
        }
        
        requestForProductList(filterStr : filterStr, showLoader: true)
        
//        self.delegate?.refreshProductList()
//        
//        self.navigationController?.popViewController(animated: true)
    }
    
    func requestForProductList(filterStr : String, showLoader : Bool)  {
        
        let request = "\(kAPIFilter)"
        
        requestParam!.action = kAPIFilter
        requestParam!.category = filterStr
        requestParam!.gender = userData!.usergender
        requestParam!.age = userData!.userAge
        
        RequestManager.singleton.requestPOST(requestMethod: request, parameter: requestParam!.toDictionary(), showLoader: true, successBlock: { (resposne :MessageModal, message : String?) in
            
            self.delegate?.applyFilterOnProductList(productItems: resposne.productList, requestParam: self.requestParam!)
            _ = self.navigationController?.popViewController(animated: true)
            
        }, failureBlock: {(error : Error?) in
            
             self.delegate?.applyFilterOnProductList(productItems: [], requestParam: self.requestParam!)
            _ = self.navigationController?.popViewController(animated: true)
        })
    }
}

extension FilterVC : UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {

        if filterData[indexPath.row].selected {
            cell.accessoryType = .checkmark
        } else {
            cell.accessoryType = .none
        }
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        filterData[indexPath.row].selected = !filterData[indexPath.row].selected
        uiTableView.reloadData()
    }

}

extension FilterVC : UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filterData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "CategoryCell") as! CategoryCell
        cell.entity = filterData[indexPath.row]
        
        return cell
    }
}

class CategoryModel {
    var index = 0
    var title = ""
    var selected = false
    
    init(index: Int, title : String, selected: Bool) {
        self.index = index
        self.title = title
        self.selected = selected
    }
}

class CategoryCell: UITableViewCell {
    
    @IBOutlet weak var title: UILabel!
    
    var entity : CategoryModel! {
            
        didSet{
            title.text = entity.title
            
            if entity.selected {
                self.accessoryType = .checkmark
            } else {
                self.accessoryType = .none
            }
        }
    }
}

protocol FilterVCDelegate {
    
    func applyFilterOnProductList(productItems : [ProductListModal], requestParam : RequestParamModal)
    
    func refreshProductList()
}
