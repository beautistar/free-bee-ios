//
//  BaseVC.swift
//  HT
//
//  Created by Dharmesh on 02/12/16.
//  Copyright © 2016 dharmesh. All rights reserved.
//

import UIKit
import Foundation
import SwiftyJSON

class BaseVC : UIViewController {      
    
    //MARK: Memory Management Method
    
    override func didReceiveMemoryWarning() {
        
    }
    
    deinit { }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    //MARK: UIView Life Cycle Method
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.edgesForExtendedLayout = UIRectEdge()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    func gotoFewVC(_ vcName: String) {
        
        self.storyboard?.instantiateInitialViewController()?.dismiss(animated: true, completion: nil)
        
        let toVC = self.storyboard?.instantiateViewController(withIdentifier: vcName)
        toVC!.modalPresentationStyle = .fullScreen
        self.present(toVC!,animated: false,completion: nil)
    }
    
    func showAlertOk(_ str: String) {
        let actionsheet = UIAlertController(title: kAppName, message: str, preferredStyle: .alert)
        
        let actionOK = UIAlertAction(title: kOk, style: UIAlertActionStyle.cancel, handler: {( action : UIAlertAction) in
        })
        actionsheet.addAction(actionOK)
        
        present(actionsheet, animated: true, completion: nil)
    }

}


func loadJsonFile(_ fileName: String) -> [String: AnyObject]? {
    if let path = Bundle.main.path(forResource: fileName, ofType: "json") {
        do {
            let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
            let jsonResult = try JSONSerialization.jsonObject(with: data, options: .mutableLeaves)
            if let jsonResult = jsonResult as? Dictionary<String, AnyObject> {
                
                jsonResult.sorted(by: {$0.key < $1.key})
                return jsonResult
                
                
//                var dictionary = jsonResult
//
//                if fileName == "distinations" {
//                    dataStateCity = jsonResult
//                    dataStateCity?.sorted(by: {$0.key < $1.key} )
//                    return dataStateCity;
//                } else {
//                    dataOccupation = jsonResult
//                    dataOccupation?.sorted(by: {$0.key < $1.key} )
//                    return dataOccupation;
//                }
            }
        } catch {
           // handle error
        }
    }
    
    return nil
}

func loadJson(filename fileName: String) -> [String: AnyObject]? {
    if let url = Bundle.main.url(forResource: fileName, withExtension: "json") {
        do {
            let data = try Data(contentsOf: url)
            let object = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
            if let dictionary = object as? [String: AnyObject] {
                return dictionary
            }
        } catch {
            print("Error!! Unable to parse  \(fileName).json")
        }
    }
    return nil
}

func loadJsonToDict(forFilename fileName: String) -> NSDictionary? {

    if let url = Bundle.main.url(forResource: fileName, withExtension: "json") {
        if let data = NSData(contentsOf: url) {
            do {
                let dictionary = try JSONSerialization.jsonObject(with: data as Data, options: .allowFragments) as? NSDictionary

                return dictionary
            } catch {
                print("Error!! Unable to parse  \(fileName).json")
            }
        }
        print("Error!! Unable to load  \(fileName).json")
    }

    return nil
}

func loadJson1(filename fileName: String) -> [String: AnyObject]? {
    if let url = Bundle.main.url(forResource: fileName, withExtension: "json") {
        if let data = NSData(contentsOf: url) {
            do {
                let object = try JSONSerialization.jsonObject(with: data as Data, options: .allowFragments)
                if let dictionary = object as? [String: AnyObject] {
                    return dictionary
                }
            } catch {
                print("Error!! Unable to parse  \(fileName).json")
            }
        }
        print("Error!! Unable to load  \(fileName).json")
    }
    return nil
}
