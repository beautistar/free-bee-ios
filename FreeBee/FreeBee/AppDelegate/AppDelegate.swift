//
//  AppDelegate.swift
//  HT
//
//  Created by Dharmesh on 30/11/16.
//  Copyright © 2016 dharmesh. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import AFNetworking

import Firebase
import FBSDKCoreKit
import GoogleSignIn
import UserNotificationsUI


let FB_SCHEME = "fb516615525799024"
//let GOOGLE_SCHEME  = "com.googleusercontent.apps.480946499113-6deojl08i8420vph3crmcsba8t2rv7rc"

let GOOGLE_SCHEME  = "998795177484-d6207j5agm7gvrjqkqo5lp5rsnigbt5k.apps.googleusercontent.com"

var dataStateCity : Dictionary<String, AnyObject>?

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var linkType = 1
    
    static var singleton : AppDelegate {
        return UIApplication.shared.delegate as! AppDelegate
    }
    
    //MARK:- AppDelegate
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        //start monitoring network connectivity
        AFNetworkReachabilityManager.shared().startMonitoring()
        AFNetworkActivityIndicatorManager.shared().isEnabled = true
        
        IQKeyboardManager.shared.enable = true
        FirebaseApp.configure()
        
        // for google signin
        GIDSignIn.sharedInstance()?.clientID = FirebaseApp.app()?.options.clientID
        
        dataStateCity = loadJsonFile("citys")
//        loadJsonFile("distinations")
        
        return true
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        //let loginManager = FBSDKLoginManager()
//        loginManager.logOut()
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }

    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
       if url.scheme == FB_SCHEME {
           return ApplicationDelegate.shared.application(application, open: url, sourceApplication: sourceApplication, annotation: annotation
           )
       } else {
           
           return (GIDSignIn.sharedInstance()?.handle(url))!
       }
    }
    
    @available(iOS 9.0, *)
    func application(_ application: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey: Any]) -> Bool {
        
        if url.scheme == FB_SCHEME {
            return ApplicationDelegate.shared.application(application, open: url, options: options)
        } else {
            
            return (GIDSignIn.sharedInstance()?.handle(url))!
        }
    }
    
}

