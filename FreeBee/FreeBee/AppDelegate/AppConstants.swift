//
//  AppConstants.swift
//  HT
//
//  Created by Dharmesh on 30/11/16.
//  Copyright © 2016 dharmesh. All rights reserved.
//

import UIKit
import Foundation
import Localize_Swift
import MBProgressHUD

let kAppName = "FreeBee".localized()

//let kColorOrange = UIColor(hex: "f26e22")

let kColorOrange = UIColor(hex: "FDB913")
//let kColorBackground = UIColor(hex: "EEEEEE")
let kColorBackground = UIColor(hex: "FDB913")
//let kColorRed = UIColor(hex: "DB4A54")
let KColorYellow = UIColor(hex: "FDB913")

//let kColorLike = UIColor(hex: "EB505A")
let kColorLike = UIColor(hex: "FDB913")
let kColorGray = UIColor.gray

var MyNotificationCenter : NotificationCenter {
    return NotificationCenter.default
}

let kNotificationHomeVC = "NotificationHomeVC"
let kNotificationPostVC = "NotificationPostVC"
let kNotificationProductListVC = "NotificationProductListVC"
let kNotificationMyNetworkVC = "NotificationMyNetworkVC"
let kNotificationHelpVC = "NotificationHelpVC"
let kNotificationSettingsVC = "NotificationSettingsVC"
let kNotificationVerifyLink = "NotificationVerifyLink"

var BlankView : UIView {
    
    let blankView = UIView()
    blankView.backgroundColor = UIColor.clear
    return blankView
}

var ScreenRect : CGRect {
    return UIScreen.main.bounds
}

var MyUserDefaults : UserDefaults {
    return UserDefaults.standard
}

let kCurrentUser = "currentUser"
let kCurrentUserId = "currentUserId"
let kCurrentUserEmail = "currentUserEmail"

let kCurrentPassword = "currentPassword"
let kLastShipAdress = "lastShipAdress"
let kLastShipRowNum = "lastShipAdress"

var userData : UserModal? {
    if let userData = MyUserDefaults.object(forKey: kCurrentUser) as? NSData {
        let dict = NSKeyedUnarchiver.unarchiveObject(with: userData as Data) as! NSDictionary
        return UserModal(fromDictionary: dict)
    }
    return nil
}

var userId : Int {
    
    let userId = MyUserDefaults.object(forKey: kCurrentUserId) as? Int
    if userId == nil {
        return 0
    } else {
        return userId!
    }
}

var userEamil : String {
    
    let userEamil = MyUserDefaults.object(forKey: kCurrentUserEmail) as? String
    if userEamil == nil {
        return ""
    } else {
        return userEamil!
    }
}

var userPassword : String {
    
    let userPassword = MyUserDefaults.object(forKey: kCurrentPassword) as? String
    if userPassword == nil {
        return ""
    } else {
        return userPassword!
    }
}

var lastShipRowNum : Int {
    
    let lastShipRowNum = MyUserDefaults.object(forKey: kLastShipRowNum) as? Int
    if lastShipRowNum == nil {
        return -1
    } else {
        return lastShipRowNum!
    }
}

var lastShipAddress : ShipAddressModel? {
    if let lastShipAddress = MyUserDefaults.object(forKey: kLastShipAdress) as? NSData {
        let dict = NSKeyedUnarchiver.unarchiveObject(with: lastShipAddress as Data) as! NSDictionary
        return ShipAddressModel(fromDictionary: dict)
    }
    return nil
}

/*func Localized(key : String) ->  String {
    return NSLocalizedString(key, comment: String())
}*/

let kCancel = "Cancel".localized()
let kOk = "Ok".localized()
let kCamera = "Camera".localized()
let kGallery = "Gallery".localized()
let kSettings = "Settings".localized()

//social 
let PREF_FIRST_FB_SIGNUP = "Pref_first_fb_signup"

func LocalizedControl(view : Any) {
    
    if view is UIButton {
        
        let button = view as! UIButton
        let keyNormal = button.title(for: .normal)
        let keyHighlighted = button.title(for: .highlighted)
        let keySelected = button.title(for: .selected)
        let keyDisable = button.title(for: .disabled)        
        
        /*if keyNormal != nil {
            button .setTitle(Localized(key: keyNormal!), for: .normal)
        }
        
        if keyHighlighted != nil {
            button .setTitle(Localized(key: keyNormal!), for: .highlighted)
        }
        
        if keySelected != nil {
            button .setTitle(Localized(key: keyNormal!), for: .selected)
        }
        
        if keyDisable != nil {
            button .setTitle(Localized(key: keyNormal!), for: .disabled)
        }*/
        
        if keyNormal != nil {
            button .setTitle(keyNormal!.localized(), for: .normal)
        }
        
        if keyHighlighted != nil {
            button .setTitle(keyNormal!.localized(), for: .highlighted)
        }
        
        if keySelected != nil {
            button .setTitle(keyNormal!.localized(), for: .selected)
        }
        
        if keyDisable != nil {
            button .setTitle(keyNormal!.localized(), for: .disabled)
        }
    }
    
    if view is UILabel {
        
        let label = view as! UILabel
        let key = label.text
        
        if key != nil {
            //label.text = Localized(key: key!)
            label.text = key!.localized()
        }
    }
    
    if view is UITextField {
        
        let label = view as! UITextField
        let key = label.placeholder
        
        if key != nil {
            //label.placeholder = Localized(key: key!)
            label.placeholder = key!.localized()
        }
    }
}

func LocalizedControl(view : Any, key : String) {
    
    if view is UIButton {
        
        let button = view as! UIButton
        
        button .setTitle(key.localized(), for: .normal)
        button .setTitle(key.localized(), for: .highlighted)
        button .setTitle(key.localized(), for: .selected)
        button .setTitle(key.localized(), for: .disabled)
    }
    
    if view is UILabel {
        
        let label = view as! UILabel
        label.text = key.localized()
    }
    
    if view is UITextField {
        
        let label = view as! UITextField
        label.placeholder = key.localized()
    }
}

func messageBox(message : String!) {
    let av = UIAlertView(title: kAppName, message: message.localized(), delegate: nil, cancelButtonTitle: kOk)
    av.show()
}

func cropToBounds(image: UIImage, width: Double, height: Double) -> UIImage {
    
    let contextImage: UIImage = UIImage(cgImage: image.cgImage!)
    
    let contextSize: CGSize = contextImage.size
    
    var posX: CGFloat = 0.0
    var posY: CGFloat = 0.0
    var cgwidth: CGFloat = CGFloat(width)
    var cgheight: CGFloat = CGFloat(height)
    
    // See what size is longer and create the center off of that
    if contextSize.width > contextSize.height {
        posX = ((contextSize.width - contextSize.height) / 2)
        posY = 0
        cgwidth = contextSize.height
        cgheight = contextSize.height
    } else {
        posX = 0
        posY = ((contextSize.height - contextSize.width) / 2)
        cgwidth = contextSize.width
        cgheight = contextSize.width
    }
    
    let rect: CGRect = CGRect(x: posX, y: posY, width: cgwidth, height: cgheight)
    
    // Create bitmap image from context using the rect
    let imageRef: CGImage = contextImage.cgImage!.cropping(to: rect)!
    
    // Create a new image based on the imageRef and rotate back to the original orientation
    let image: UIImage = UIImage(cgImage: imageRef, scale: image.scale, orientation: image.imageOrientation)
    
    return image
}

func progressHUD(view : UIView, mode: MBProgressHUDMode = .annularDeterminate) -> MBProgressHUD {
    
    let hud = MBProgressHUD .showAdded(to:view, animated: true)
    hud.mode = mode
    hud.label.text = "Loading";
    hud.animationType = .zoomIn
    hud.tintColor = UIColor.white
    hud.contentColor = kColorOrange
    return hud
}

let genders = ["male", "female"]

let hobbys = ["Hockey","Football","Baseball","Basketball", "Billiards", "Track and field", "Sailing", "Running", "Hiking", "Biking", "marathons", "triathlons", "kayaking", "canoeing", "boating", "fishing", "bowling", "tennis", "cricket", "volleyball", "archery", "reading", "writing", "drawing", "listening to music", "skateboarding", "soccer", "dancing", "skydiving", "flying", "traveling", "car racing", "motorcycle riding", "computers", "technology", "investing", "acting", "baking", "blogging", "board games", "video gaming", "programming", "martial arts", "collecting", "couponing", "crafts", "electronics", "embroidery", "fantasy sports", "fashion", "carpentry", "genealogy", "graphic design", "home improvement", "ice skating", "karaoke", "knitting", "painting", "photography", "musical instruments", "pottery", "puzzles", "scrapbooking", "sculpting", "sewing", "social media", "watching television", "watching movies", "weight lifting", "wine", "beer", "yoga", "climbing", "surfing", "swimming"]

let categorys = [
    "Electronics, Office & Desktop",
    "Golf, Sports & Outdoor",
    "Caps & Hats",
    "Stress Balls, Games & Toys",
    "Trade Show & Signage",
    "Personal Care & Beauty",
    "Automotive",
    "Tools & Flashlights",
    "Callendars & Planners",
    "Home, Kitchen & Pets",
    "Food & Candy"
]

let answerData = [
    "Free-Bee is a first of its kind advertising platform that allows companies to direct their promotional products to the right people using various targeting metrics.",
    "Free-Bee “claimers” must fill out a detailed questionnaire at signup. This allows Free-Bee to identify many metrics about them including their geographical location, age, gender, workplace, occupation, hobbies, interests and activities they participate in.",
    "Typically, a company will meet with potential clients or end users at networking events, conventions and one-on-one meet and greets. Now for the first time, companies are able to easily and accurately put their promotional products into the hands of the people who are most valuable without ever having to go through conventional channels. Many Free-Bee users have excess inventory of promotional products and no way to give them away. With Free-Bee these goods can remain valuable by broadening brand recognition to new audiences all over the world or in your immediate vicinity.",
    "“Claimers” can claim bundles of promotional products at no cost. Advertisers are charged a small fee per item “claimed.” The charge for standard free-bees such as pens, small calendars, keychains, small bags is $0.50 and the charge for premium free-bees such as flashlights, mugs, bottles, electronic devices, and apparel is $2.00.",
    "Advertisers must ship their promotional products in bulk to the Free-Bee warehouse after free-bees are listed on the platform and prior to listing activation. Once “claimers” claim their free-bees and finalize their bundles Free-Bee will ship the bundles via USPS as no cost to advertisers or claimers.",
    "In the old days promotional free-bees were undesirable, cheaply made throwaways of little value. Times have changes. Now literally thousands of cool and useful free-bees are manufactured around the world in tremendously large quantities. People are always excited to receive free things which are useful because they are paying for these goods every day.",
    "Yes. Advertisers set limits on how many of a given item a claimer can receive. For example, if you want each claimer to receive only one of your mugs you can set that limitation in the product listing page.",
    "By putting your branded free-bees into the hands of valuable, targeted audiences you will increase brand recognition cheaply using an entirely untapped advertising channel. Conventional digital advertising platforms are oversaturated. Viewers are inundated with hundreds of ads daily causing them to suffer from what’s called banner blindness. The effect of this is constantly decreasing effectiveness of traditional digital advertisements. Advertisers can overcome this by getting their branded goods physically into the hands of a targeted audience. Moreover, where a single display ad is seen once very briefly and then disappears, a physical item can remain visible and of use to audiences for weeks, months and even years.",
    "Imagine you make insoles for running shoes. You are currently expanding your physical retail presence to a particular city in which you previously had none. Now imagine, the value of placing a promotional item, let’s say a headband with your name, logo and slogan on it, into the hands of a runner who lives in that city. What is the value of that runner viewing and selecting your product, unpacking it and wearing it whenever he or she goes for a run? We think a lot.",
    "You will claim goods in what Free-Bee calls bundles. A bundle consists of no more than 10 freebees which can be any mixture of standard and premium free-bees.",
    "Claimers are limited to one bundle per month to ensure advertisers get great value and are not diluted by claimers being inundated with free goods, and to reduce abusive practices of the platform.",
    "Free-Bee has a list of cooperating promotional product manufacturers who offer special rates to Free-Bee users. Each manufacturer provides their own unique line of promotional products. Please see the list of cooperating manufacturers below. By using cooperating manufacturers, you will be able to have your products shipped directly to the Free-Bee warehouse, which will provide even more cost saving to you.",
    "Please consult our product category list to determine how your free-bees will be categorized. Free-Bee will ultimately decide how products are categorized.",
    "Yes. Free-Bee is rolling out a new communication tool which will allow advertisers to communicate with claimers who have claimed or wish to claim at least one of the advertiser’s free-bees. This tool will be beneficial in various situations including when advertisers want to vet claimers of high value promotional free-bees (Generally over $20 in value/item). Likewise, claimers are able to communicate with advertisers when they would like to discuss potential business relationships or to ask for more information about the advertiser.",
    "Yes. For low value free-bees (Generally below $20/item but typically under $5 production cost) and high value items, advertisers should assign targeting metrics which are available in the product listing page. For High Value free-bees, Advertisers will have the opportunity to vet claimers before allowing a claim to be processed. Advertisers are required to respond to a claimer in this situation within one hour or the claimer will be informed to move on."
]

let qData = [
    "What is Free-Bee?",
    "How does Free-Bee allow companies to put their promotional products into the hands of a targeted audience?",
    "What are the benefits to advertisers who use Free-Bee?",
    "What does it cost?",
    "How do my promotional products get from point A to point B? ",
    "Promotional products tend to be cheap throw away free-bees. Why would anyone want this stuff?",
    "Can I control the quantity each claimer can receive for each product listing?",
    "Why would I want to pay to send promotional products to people who I don’t know?",
    "What is an example of how this service can provide a return for me?",
    "As a Free-Bee claimer, what am I allowed to claim for free?",
    "How often can I claim a bundle?",
    "I don’t have any promotional products to give away. How can I obtain them?",
    "How are free-bees categorized as standard or promotional?",
    "Can Advertisers and Claimers communicate?",
    "Can Advertisers control who receives their free-bees?"
]

let TypeData = [
    "regular",
    "premium"
]

let industryData = [
    "Healthcare",
    "Insurance",
    "Automotive",
    "Manufacturing",
    "Retail",
    "Real Estate",
    "Legal",
    "Transportation/Logistics",
    "Financial Services",
    "Environment",
    "Energy",
    "Government",
    "Technology",
    "Education",
    "Hospitality",
    //"Transportation",
    "Information Technology",
    "Human Resources"
]

var ageData = [
    "12-17 years old",
    "18-24 years old",
    "25-34 years old",
    "35-44 years old",
    "45-54 years old",
    "55-64 years old",
    "65+ years old.",
]
