//
//  RequestManager.swift
//  HT
//
//  Created by Dharmesh on 15/12/16.
//  Copyright © 2016 dharmesh. All rights reserved.
//

import UIKit
import Foundation
import MBProgressHUD
import AFNetworking
import Localize_Swift

let kAPIProductList = "product_list"
let kAPIProductDetail = "product_detail"
let kAPIPostAdd = "post_ad"
let kAPIPostEdit = "edit_product"
let kAPIPostDelete = "delete_product"
let kAPIRegister = "register"//"register_user"
let kAPILogin = "login"
let kAPIRegisterSocialInit = "social_signup"
let kAPIRegisterSocialAdd = "register_userinfo"


let kAPIProfile = "user_profile"
let kAPIUpdateUser = "update_user"//
let kAPIVerifyFacebook = "verify_facebook"
let kAPIVerifyGoogle = "verify_google"

let kAPISearch = "search_product"
let kAPIFilter = "filter_product"

let kAPIFollowing = "user_following"
let kAPIFollowers = "user_follower"
let kAPISearchFriend = "search_friend"
let kAPIFriendsProfile = "friends_profile"
let kAPIFollowFriend = "follow_friend"
let kAPIUnfollowFriend = "unfollow"
let kAPILikeProduct = "like_product"
let kAPISoldProduct = "sold_product"


//rms
let kAPIAddCart = "add_cart_product"
let kAPIRemoveCart = "remove_cart"
let kAPIGetCartList = "get_cart_product"

let kAPISocialSign = "social_signup"

let kAPIGetAdress = "get_address"
let kAPIAddAdress = "add_address"
let kAPICheckout = "check_out"

class RequestManager : NSObject {
    
    static var singleton = RequestManager()
    
//    let kBaseURL = "http://52.40.130.22/index.php/api/"
    let kBaseURL = "http://free-bee.co/index.php/api/"
    
    let messageNoIntenetConnection = "No intenet connection".localized()
    let messageParsingError = "No valid response"
    let messageNoRecordFound = "No record found"
    
    var apiManager : AFHTTPSessionManager {
        
        let manager = AFHTTPSessionManager()
        
        //let requestSerializer = AFJSONRequestSerializer()
        //requestSerializer.stringEncoding = String.Encoding.utf8.rawValue
        //manager.requestSerializer = requestSerializer
        
        manager.responseSerializer = AFJSONResponseSerializer()
        
        let codeToAccept = NSMutableIndexSet()
        codeToAccept.add(400)
        codeToAccept.add(200)
        codeToAccept.add(201)
        manager.responseSerializer.acceptableContentTypes = ["text/html", "application/json"];
        manager.responseSerializer.acceptableStatusCodes = codeToAccept as IndexSet
        
        return manager
    }
    
    func requestGET(requestMethod : String, showLoader : Bool, successBlock:@escaping ((_ response : MessageModal, _ message : String?)->Void), failureBlock:@escaping ((_ error : Error?) -> Void)) {
        
        if !AFNetworkReachabilityManager.shared().isReachable {
            messageBox(message: messageNoIntenetConnection)
            return
        }
        
        let requestURL = kBaseURL.appending(requestMethod)
        
        debugPrint("------------- Start -------------");
        print("requestURL :- \(requestURL)");
        
        let escapedString = requestURL.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlFragmentAllowed)
        
        let window = AppDelegate.singleton.window?.rootViewController
        var hud : MBProgressHUD
        
        if showLoader && window != nil {
            hud = MBProgressHUD.showAdded(to: window!.view, animated: true)
        } else {
            hud = MBProgressHUD()
        }
        hud.mode = .indeterminate
        hud.label.text = "Loading";
        hud.animationType = .zoomIn
        hud.tintColor = UIColor.white
        hud.contentColor = kColorOrange
        
        let manager = apiManager
        
        manager.get(escapedString!, parameters: nil,progress: { (data) in }, success: { (dataTask, response) in
            
            if showLoader {
                hud.hide(animated: true)
            }
            
            if let data = response as? NSDictionary {
                
                print("response :- \(data)");
                
                debugPrint("------------- End -------------");
                
                let responseModal = MessageModal(fromDictionary: data)
                
                if responseModal.resultCode == 0 {
                    successBlock(responseModal, nil)
                }
                if responseModal.resultCode == 1 {
                    //messageBox(message: self.messageNoRecordFound)
                    failureBlock(nil)
                } else {
                    
                }
                
            } else {
                messageBox(message: self.messageParsingError)
            }
            
        }) { (dataTask, error) in
            
            if showLoader {
                hud.hide(animated: true)
            }
            messageBox(message: error.localizedDescription)
            print("error :- \(error.localizedDescription)");
            debugPrint("------------- End -------------");
            failureBlock(error)
        }
    }
    
    func requestPOST(requestMethod : String, parameter : NSDictionary, showLoader : Bool, successBlock:@escaping ((_ response : MessageModal, _ message : String?)->Void), failureBlock:@escaping ((_ error : Error?) -> Void)) {
        
        if !AFNetworkReachabilityManager.shared().isReachable {
            messageBox(message: messageNoIntenetConnection)
            return
        }
        
        let requestURL = kBaseURL.appending(requestMethod)
        
        debugPrint("------------- Start -------------");
        print("requestURL :- \(requestURL)");
        
        let escapedString = requestURL.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlFragmentAllowed)
        
        let window = AppDelegate.singleton.window?.rootViewController
        var hud : MBProgressHUD
        
        if showLoader && window != nil {
            hud = MBProgressHUD .showAdded(to: window!.view, animated: true)
        } else {
            hud = MBProgressHUD()
        }
        hud.mode = .indeterminate
        hud.label.text = "Loading";
        hud.animationType = .zoomIn
        hud.tintColor = UIColor.white
        hud.contentColor = kColorOrange
        
        //let parameterDict = NSMutableDictionary(dictionary: parameter)
        print("parameter :- \(parameter)");
        
        let manager = apiManager
        
        manager.post(escapedString!, parameters: parameter, progress: { (data) in
            
        }, success: { (dataTask, response) in
            
            if showLoader {
                hud.hide(animated: true)
            }
            
            if let data = response as? NSDictionary {
                
                print("response :- \(data)");
                
                debugPrint("------------- End -------------");
                
                let responseModal = MessageModal(fromDictionary: data)
                
                if responseModal.resultCode == 1 {
                    failureBlock(nil)
                } else {
                    successBlock(responseModal, nil)
                }
                
            } else {
                messageBox(message: self.messageParsingError)
            }
            
        }) { (dataTask, error) in
            
            if showLoader {
                hud.hide(animated: true)
            }
            messageBox(message: error.localizedDescription)
            print("error :- \(error.localizedDescription)");
            debugPrint("------------- End -------------");
            failureBlock(error)
        }
    }
    
    func requestRowPOST(requestMethod : String, parameter : NSDictionary, showLoader : Bool, successBlock:@escaping ((_ response : MessageModal, _ message : String?)->Void), failureBlock:@escaping ((_ error : Error?) -> Void)) {
        
        if !AFNetworkReachabilityManager.shared().isReachable {
            messageBox(message: messageNoIntenetConnection)
            return
        }
        
        let requestURL = kBaseURL.appending(requestMethod)
        
        debugPrint("------------- Start -------------");
        print("requestURL :- \(requestURL)");
        
        let escapedString = requestURL.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlFragmentAllowed)
        
        let window = AppDelegate.singleton.window?.rootViewController
        var hud : MBProgressHUD
        
        if showLoader && window != nil {
            hud = MBProgressHUD .showAdded(to: window!.view, animated: true)
        } else {
            hud = MBProgressHUD()
        }
        
        hud.mode = .indeterminate
        hud.label.text = "Loading";
        hud.animationType = .zoomIn
        hud.tintColor = UIColor.white
        hud.contentColor = kColorOrange
        
//        let parameterDict = NSMutableDictionary(dictionary: parameter)
        print("parameter :- \(parameter)");
        
        let manager = apiManager
        manager.requestSerializer = AFJSONRequestSerializer()
        
        manager.post(escapedString!, parameters: parameter, progress: {(data) in}, success: {(dataTask, response) in
            
            if showLoader {
                hud.hide(animated: true)
            }
            
            if let data = response as? NSDictionary {
                
                print("response :- \(data)");
                debugPrint("------------- End -------------");
                let responseModal = MessageModal(fromDictionary: data)
                
                if responseModal.resultCode == 1 {
                    failureBlock(nil)
                } else {
                    successBlock(responseModal, data["message"] as? String)
                }
            } else {
                messageBox(message: self.messageParsingError)
            }
            
        }) { (dataTask, error) in
            
            if showLoader {
                hud.hide(animated: true)
            }
            messageBox(message: error.localizedDescription)
            print("error :- \(error.localizedDescription)");
            debugPrint("------------- End -------------");
            failureBlock(error)
        }
    }
    
    func requestMultipartPOST(requestMethod : String, parameter : NSDictionary, showLoader : Bool, name : String, images : [UIImage], successBlock:@escaping ((_ response : MessageModal, _ message : String?) -> Void), failureBlock:@escaping ((_ error : Error?) -> Void)) {
        
        if !AFNetworkReachabilityManager.shared().isReachable {
            messageBox(message: messageNoIntenetConnection)
            return
        }
        
        let requestURL = kBaseURL.appending(requestMethod)
        
        debugPrint("------------- Start -------------");
        print("requestURL :- \(requestURL)");
        
        let escapedString = requestURL.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlFragmentAllowed)
        
        let window = AppDelegate.singleton.window?.rootViewController
        var hud : MBProgressHUD
        
        if showLoader && window != nil {
            hud = MBProgressHUD .showAdded(to: window!.view, animated: true)
        } else {
            hud = MBProgressHUD()
        }
        hud.mode = .indeterminate
        hud.label.text = "Requesting";
        hud.animationType = .zoomIn
        hud.tintColor = UIColor.white
        hud.contentColor = kColorOrange
        
        //let parameterDict = NSMutableDictionary(dictionary: parameter)
        print("parameter :- \(parameter)");
        
        let manager = apiManager
        
        manager.post(escapedString!, parameters: parameter, constructingBodyWith: {(formData : AFMultipartFormData) in
        
            for imgObject in images {
                
                let imgData = UIImageJPEGRepresentation(imgObject, 0.9)
                if imgData != nil {
                    formData.appendPart(withFileData: imgData!, name: name, fileName: "image.jpeg", mimeType: "image/jpeg")
                }
            }
            
        }, progress: {( progress : Progress) in
            
        }, success: {(dataTask, response) in
            
            if showLoader {
                hud.hide(animated: true)
            }
            
            if let data = response as? NSDictionary {
                
                print("response :- \(data)");
                
                debugPrint("------------- End -------------");
                
                let responseModal = MessageModal(fromDictionary: data)
                
                if responseModal.resultCode == 1 {
                    //messageBox(message: self.messageNoRecordFound)
                    failureBlock(nil)
                } else {
                    successBlock(responseModal, nil)
                }
                
            } else {
                messageBox(message: self.messageParsingError)
            }
            
        }, failure: { (dataTask, error) in
        
            if showLoader {
                hud.hide(animated: true)
            }
            messageBox(message: error.localizedDescription)
            print("error :- \(error.localizedDescription)");
            debugPrint("------------- End -------------");
            failureBlock(error)
        })
        
    }
    
    //MARK:- Initialisation
    override init() {
        super.init()
        
        //AFNetworkReachabilityManager.shared().startMonitoring()
    }
}

