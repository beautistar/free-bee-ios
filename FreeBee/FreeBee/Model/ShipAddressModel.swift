//
//  ShipAdressModel.swift
//  FreeBee
//
//  Created by AngelDev on 2/4/20.
//  Copyright © 2020 dharmesh. All rights reserved.
//

import Foundation
import SwiftyJSON

class ShipAddressModel {
    var shipId : String!
    var shipName : String!
    var shipAdress : String!
    var shipCity : String!
//    var shipDistrict : String!
    var shipProvince : String!
    var shipZipcode : String!
    var created_at : String!
    
    init () {
        shipId      = ""
        shipName    = ""
        shipAdress  = ""
        shipCity    = ""
        shipProvince = ""
        shipZipcode = ""
        created_at  = ""
    }
    
    init(fromDictionary dictionary: NSDictionary){
        shipId      = dictionary["id"] as? String
        shipName    = dictionary["name"] as? String
        shipAdress  = dictionary["address"] as? String
        shipCity    = dictionary["city"] as? String
        shipProvince = dictionary["state"] as? String
        shipZipcode = dictionary["zipcode"] as? String
        created_at  = dictionary["created_at"] as? String
    }
    
    func toDictionary() -> NSDictionary {
        let dictionary = NSMutableDictionary()
        
        if shipId != nil{
            dictionary["id"] = shipId
        }
        if shipName != nil{
            dictionary["name"] = shipName
        }
        if shipAdress != nil{
            dictionary["address"] = shipAdress
        }
        if shipCity != nil{
            dictionary["city"] = shipCity
        }
        
        if shipProvince != nil{
            dictionary["state"] = shipProvince
        }
        if shipZipcode != nil{
            dictionary["zipcode"] = shipZipcode
        }
        if created_at != nil{
            dictionary["created_at"] = created_at
        }
        
        return dictionary
    }
    
    
    
    init(formJson json : JSON) {
        
        shipId      = json["id"].stringValue
        shipName    = json["name"].stringValue
        shipAdress  = json["address"].stringValue
        shipCity    = json["city"].stringValue
        shipProvince = json["state"].stringValue
        shipZipcode = json["zipcode"].stringValue
        created_at  = json["created_at"].stringValue
    }
    
    /*
    init(shipId : String, shipName : Bool ) {
        
        self.shipId = shipId
        self.shipName = shipName
    }*/
}
