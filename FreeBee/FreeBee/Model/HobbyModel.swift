//
//  HobbyModel.swift
//  HT
//
//  Created by Developer on 11/3/19.
//  Copyright © 2019 dharmesh. All rights reserved.
//

import Foundation

class HobbyModel {
    var topic = ""
    var selectState = false
    
    init(topic : String, state : Bool ) {
        
        self.topic = topic
        self.selectState = state
    }
}
