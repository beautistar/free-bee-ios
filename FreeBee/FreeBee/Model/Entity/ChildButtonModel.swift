//
//  ChildButtonModel.swift
//  HT
//
//  Created by Developer on 11/6/19.
//  Copyright © 2019 dharmesh. All rights reserved.
//

import Foundation

class ChildButtonModel {
    var number = ""
    var selectState = false
    
    
    init(number : String, state : Bool) {
        
        self.number = number
        self.selectState = state
        
    }
}
