//
//	UserProfile.swift
//
//	Create by Dharmesh Avaiya on 7/1/2017
//	Copyright © 2017 Azilen Technologies. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class UserProfile : NSObject, NSCoding{
    
    var countFollowers : Int!
    var countFollowing : Int!
    var isVerified : Int!
    var listOfProducts : [ProductListModal]!
    var productBought : [ProductListModal]!
    var productLikes : [ProductListModal]!
    var userid : String!
    var userimage : String!
    var username : String!
    var verifyFacebook : String!
    var verifyGoogle : String!
    var userAge: String!
    var usergender: String!
    
    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: NSDictionary){
        countFollowers = dictionary["count_followers"] as? Int
        countFollowing = dictionary["count_following"] as? Int
        isVerified = dictionary["is_verified"] as? Int
        listOfProducts = [ProductListModal]()
        if let listOfProductsArray = dictionary["list_of_products"] as? [NSDictionary]{
            for dic in listOfProductsArray{
                let value = ProductListModal(fromDictionary: dic)
                listOfProducts.append(value)
            }
        }
        productBought = dictionary["product_bought"] as? [ProductListModal
        
        
        ]
        productLikes = [ProductListModal]()
        if let productLikesArray = dictionary["product_likes"] as? [NSDictionary]{
            for dic in productLikesArray{
                let value = ProductListModal(fromDictionary: dic)
                productLikes.append(value)
            }
        }
        userid = dictionary["userid"] as? String
        userimage = dictionary["userimage"] as? String
        username = dictionary["username"] as? String
        verifyFacebook = dictionary["verify_facebook"] as? String
        verifyGoogle = dictionary["verify_google"] as? String
        userAge = dictionary["age"] as? String
        usergender = dictionary["gender"] as? String
        
    }
    
    /**
     * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> NSDictionary {
        let dictionary = NSMutableDictionary()
        if countFollowers != nil{
            dictionary["count_followers"] = countFollowers
        }
        if countFollowing != nil{
            dictionary["count_following"] = countFollowing
        }
        if isVerified != nil{
            dictionary["is_verified"] = isVerified
        }
        if listOfProducts != nil{
            var dictionaryElements = [NSDictionary]()
            for listOfProductsElement in listOfProducts {
                dictionaryElements.append(listOfProductsElement.toDictionary())
            }
            dictionary["list_of_products"] = dictionaryElements
        }
        if productBought != nil{
            dictionary["product_bought"] = productBought
        }
        if productLikes != nil{
            var dictionaryElements = [NSDictionary]()
            for productLikesElement in productLikes {
                dictionaryElements.append(productLikesElement.toDictionary())
            }
            dictionary["product_likes"] = dictionaryElements
        }
        if userid != nil{
            dictionary["userid"] = userid
        }
        if userimage != nil{
            dictionary["userimage"] = userimage
        }
        if username != nil{
            dictionary["username"] = username
        }
        if verifyFacebook != nil{
            dictionary["verify_facebook"] = verifyFacebook
        }
        if verifyGoogle != nil{
            dictionary["verify_google"] = verifyGoogle
        }
        if userAge != nil{
            dictionary["age"] = userAge
        }
        if usergender != nil{
            dictionary["gender"] = userAge
        }
        
        return dictionary
    }
    
    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        countFollowers = aDecoder.decodeObject(forKey: "count_followers") as? Int
        countFollowing = aDecoder.decodeObject(forKey: "count_following") as? Int
        isVerified = aDecoder.decodeObject(forKey: "is_verified") as? Int
        listOfProducts = aDecoder.decodeObject(forKey: "list_of_products") as? [ProductListModal]
        productBought = aDecoder.decodeObject(forKey: "product_bought") as? [ProductListModal]
        productLikes = aDecoder.decodeObject(forKey: "product_likes") as? [ProductListModal]
        userid = aDecoder.decodeObject(forKey: "userid") as? String
        userimage = aDecoder.decodeObject(forKey: "userimage") as? String
        username = aDecoder.decodeObject(forKey: "username") as? String
        verifyFacebook = aDecoder.decodeObject(forKey: "verify_facebook") as? String
        verifyGoogle = aDecoder.decodeObject(forKey: "verify_google") as? String
        userAge = aDecoder.decodeObject(forKey: "age") as? String
        usergender = aDecoder.decodeObject(forKey: "gender") as? String
        
    }
    
    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder) {

        if countFollowers != nil{
            aCoder.encode(countFollowers, forKey: "count_followers")
        }
        if countFollowing != nil{
            aCoder.encode(countFollowing, forKey: "count_following")
        }
        if isVerified != nil{
            aCoder.encode(isVerified, forKey: "is_verified")
        }
        if listOfProducts != nil{
            aCoder.encode(listOfProducts, forKey: "list_of_products")
        }
        if productBought != nil{
            aCoder.encode(productBought, forKey: "product_bought")
        }
        if productLikes != nil{
            aCoder.encode(productLikes, forKey: "product_likes")
        }
        if userid != nil{
            aCoder.encode(userid, forKey: "userid")
        }
        if userimage != nil{
            aCoder.encode(userimage, forKey: "userimage")
        }
        if username != nil{
            aCoder.encode(username, forKey: "username")
        }
        if verifyFacebook != nil{
            aCoder.encode(verifyFacebook, forKey: "verify_facebook")
        }
        if verifyGoogle != nil{
            aCoder.encode(verifyGoogle, forKey: "verify_google")
        }
        if userAge != nil{
            aCoder.encode(userAge, forKey: "age")
        }
        if usergender != nil{
            aCoder.encode(userAge, forKey: "gender")
        }
        
    }
}
