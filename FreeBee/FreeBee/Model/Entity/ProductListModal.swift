//
//	ProductList.swift
//
//	Create by Dharmesh Avaiya on 15/12/2016
//	Copyright © 2016 Azilen Technologies. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation

class ProductListModal : NSObject, NSCoding{

	var productId : String!
	var productImage : String!
	var productName : String!
    var soldCount : String!
    var type : String!
    var productDescription : String!
    var product_username : String!

	init(fromDictionary dictionary: NSDictionary){
		productId = dictionary["product_id"] as? String
		productImage = dictionary["product_image"] as? String
		productName = dictionary["product_name"] as? String
        productDescription = dictionary["product_description"] as? String
        soldCount = dictionary["sold_count"] as? String
        type = dictionary["type"] as? String
        product_username = dictionary["product_username"] as? String
	}

	/** Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary {
		let dictionary = NSMutableDictionary()
        
		if productId != nil{
			dictionary["product_id"] = productId
		}
		if productImage != nil{
			dictionary["product_image"] = productImage
		}
		if productName != nil{
			dictionary["product_name"] = productName
		}
        if productDescription != nil{
            dictionary["product_description"] = productDescription
        }
        if soldCount != nil{
            dictionary["sold_count"] = soldCount
        }
        if type != nil{
            dictionary["type"] = type
        }
        if product_username != nil{
            dictionary["product_username"] = product_username
        }
        
        
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder) {
        productId = aDecoder.decodeObject(forKey: "product_id") as? String
        productImage = aDecoder.decodeObject(forKey: "product_image") as? String
        productName = aDecoder.decodeObject(forKey: "product_name") as? String
        productDescription = aDecoder.decodeObject(forKey: "product_description") as? String
        soldCount = aDecoder.decodeObject(forKey: "sold_count") as? String
        type = aDecoder.decodeObject(forKey: "type") as? String
        product_username = aDecoder.decodeObject(forKey: "product_username") as? String
        
	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder) {
        
		if productId != nil{
			aCoder.encode(productId, forKey: "product_id")
		}
		if productImage != nil{
			aCoder.encode(productImage, forKey: "product_image")
		}
		if productName != nil{
			aCoder.encode(productName, forKey: "product_name")
		}
        if productDescription != nil{
            aCoder.encode(productDescription, forKey: "product_description")
        }
        if soldCount != nil{
            aCoder.encode(soldCount, forKey: "sold_count")
        }
        if type != nil{
            aCoder.encode(type, forKey: "type")
        }
        if product_username != nil{
            aCoder.encode(type, forKey: "product_username")
        }
	}

}
