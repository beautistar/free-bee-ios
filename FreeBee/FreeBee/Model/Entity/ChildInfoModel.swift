//
//  ChildInfoModel.swift
//  HT
//
//  Created by Developer on 11/7/19.
//  Copyright © 2019 dharmesh. All rights reserved.
//

import Foundation

class ChildInfoModel {
        
    var no = 0
    var selectedSex = 0
    var childAge = 0
    
    init (no: Int, selectedSex: Int, childAge: Int) {
        self.no = no
        self.selectedSex = selectedSex
        self.childAge = childAge
    }
}

