//
//  RequestParamModal.swift
//  HT
//
//  Created by Dharmesh on 17/12/16.
//  Copyright © 2016 dharmesh. All rights reserved.
//

import Foundation
import UIKit

class RequestParamModal : NSObject {
    
    var Id : String!
    var userId : String!
    var page : String!
    var action : String!
    var productId : String! //product_id
    var productSmallId : String! //product_id
    var productName : String! //product_name
    var productImage : [UIImage]! //product_image
    var productDescription : String! //product_description
    var productPrice : String! //product_price
    var category : String!
    var latitude : String!
    var longitude : String!
    var deviceId : String! //device_id
    var priceFrom : String! //price_from
    var priceTo : String! //price_to
    var searchtext : String! //searchtext
    var friendid : String!
    var userPhoto : String! //user_photo
    var username : String!
    var type : String!
    var email : String!
    var password : String!
    var phone : String!
    
    //post add
    
    var productQuantityAvailable : String!
    var productQuantityPerUser : String!
    var productAgeFrom : String!
    var productAgeTo : String!
    var productGender : String!
    var productHobby : String!
    var productIndustry : String!
    var productOccupation : String!
    var zipCode : String!
    var productCity : String!
    var productState : String!
    var productCountry : String!
    
    var cartNum : String!
    var productType : String!
    
    //register
    var address : String!
    var city : String!
    var state : String!
    var age : String!
    var gender : String!
    var socialId : String!
    var photoUrl : String!
    var socialType : String!
    var profileZipcode : String!
    var shipname : String!
    
    //checkout
    
    var addressId : String!
    var orderList :[Dictionary<String, String>] = [] //= Dictionary<String, Any>()
    
    
    func toDictionary() -> NSDictionary {
        let dictionary = NSMutableDictionary()
        if Id != nil{
            dictionary["id"] = Id
        }
        if userId != nil{
            dictionary["userid"] = userId
        }
        if page != nil{
            dictionary["page"] = page
        }
        if action != nil {
            dictionary["action"] = action
        }
        if productId != nil {
            dictionary["product_id"] = productId
        }
        if productSmallId != nil {
            dictionary["productid"] = productSmallId
        }
        if productName != nil {
            dictionary["product_name"] = productName
        }
        if productImage != nil {
            dictionary["product_image"] = productImage
        }
        if productDescription != nil {
            dictionary["product_description"] = productDescription
        }
        if productPrice != nil {
            dictionary["product_price"] = productPrice
        }
        if category != nil {
            dictionary["category"] = category
        }
        if latitude != nil {
            dictionary["latitude"] = latitude
        }
        if longitude != nil {
            dictionary["longitude"] = longitude
        }
        if deviceId != nil {
            dictionary["device_id"] = deviceId
        }
        if priceFrom != nil {
            dictionary["price_from"] = priceFrom
        }
        if priceTo != nil {
            dictionary["price_to"] = priceTo
        }
        if searchtext != nil {
            dictionary["searchtext"] = searchtext
        }
        if friendid != nil {
            dictionary["friendid"] = friendid
        }
        if userPhoto != nil {
            dictionary["user_photo"] = userPhoto
        }
        if username != nil {
            dictionary["username"] = username
        }
        if type != nil {
            dictionary["type"] = type
        }
        if email != nil {
            dictionary["email"] = email
        }
        if password != nil {
            dictionary["password"] = password
        }
        if phone != nil {
            dictionary["phone"] = phone
        }
        
        //by RMS
        
        if productType != nil {
            dictionary["type"] = productType
        }
        
        if productQuantityAvailable != nil {
            dictionary["quantity_available"] = productQuantityAvailable
        }
        if productQuantityPerUser != nil {
            dictionary["quantity_per_user"] = productQuantityPerUser
        }
        
        if productAgeFrom != nil {
            dictionary["age_min"] = productAgeFrom
        }
        if productAgeTo != nil {
            dictionary["age_max"] = productAgeTo
        }
        if productGender != nil {
            dictionary["gender"] = productGender
        }
        if productHobby != nil {
            dictionary["hobby"] = productHobby
        }
        if productIndustry != nil {
            dictionary["industry"] = productIndustry
        }
        if productOccupation != nil {
            dictionary["occupation"] = productOccupation
        }
        if zipCode != nil {
            dictionary["zipcode"] = zipCode
        }
        if productCity != nil {
            dictionary["city"] = productCity
        }
        if productState != nil {
            dictionary["state"] = productState
        }
        if productCountry != nil {
            dictionary["country"] = productCountry
        }
        
        if address != nil {
            dictionary["address"] = address
        }
        if city != nil {
            dictionary["city"] = city
        }
        if state != nil {
            dictionary["state"] = state
        }
        if age != nil {
            dictionary["age"] = age
        }
        if gender != nil {
            dictionary["gender"] = gender
        }
        if socialId != nil {
            dictionary["social_id"] = socialId
        }
        if photoUrl != nil {
            dictionary["photo_url"] = photoUrl
        }
        if socialType != nil {
            dictionary["social_type"] = socialType
        }
        if profileZipcode != nil {
            dictionary["zip_code"] = profileZipcode
        }
        
        if cartNum != nil {
            dictionary["count"] = cartNum
        }
        if shipname != nil {
            dictionary["name"] = shipname
        }
        
        if addressId != nil {
            dictionary["address_id"] = addressId
        }
        if orderList.count > 0 {
            dictionary["order_list"] = orderList
        }
        
        
        return dictionary
    }
}
