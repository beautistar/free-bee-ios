//
//	ProductDetail.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class ProductDetailModal : NSObject, NSCoding{
    
    var category : String!
    var countMutualFriend : Int!
    var isLiked : String!
    var addedCart : String!
    var isSold : String!
    var productDescription : String!
    var productId : String!
    var productImage : [String]!
    var productLatitude : String!
    var productLongitude : String!
    var productName : String!
    var productOwnerId : String!
    var productOwnerImage : String!
    var productOwnerName : String!
    var productOwnerPhone : String!
    var productPrice : String!
    var verifyFacebook : String!
    var verifyGoogle : String!
    
    var enableQuantity : String!
    var userQuantity: String!
    var claimed : String!
    var type : String!
    
    var gender : String!
    var age_min : String!
    var age_max : String!
    var hobby : String!
    var industry : String!
    var zipcode : String!
    var occupation : String!
    var city : String!
    var state : String!
    var country : String!
    
    
    init(fromDictionary dictionary: NSDictionary){
        category = dictionary["category"] as? String
        countMutualFriend = dictionary["count_mutual_friend"] as? Int
        isLiked = dictionary["is_liked"] as? String
        addedCart = dictionary["cart_added"] as? String
        isSold = dictionary["is_sold"] as? String
        productDescription = dictionary["product_description"] as? String
        productId = dictionary["product_id"] as? String
        productImage = dictionary["product_image"] as? [String]
        productLatitude = dictionary["product_latitude"] as? String
        productLongitude = dictionary["product_longitude"] as? String
        productName = dictionary["product_name"] as? String
        productOwnerId = dictionary["product_owner_id"] as? String
        productOwnerImage = dictionary["product_owner_image"] as? String
        productOwnerName = dictionary["product_owner_name"] as? String
        productOwnerPhone = dictionary["product_owner_phone"] as? String
        productPrice = dictionary["product_price"] as? String
        verifyFacebook = dictionary["verify_facebook"] as? String
        verifyGoogle = dictionary["verify_google"] as? String
        
        enableQuantity = dictionary["quantity_available"] as? String
        userQuantity =  dictionary["quantity_per_user"] as? String
        claimed = dictionary["sold_count"] as? String
        type = dictionary["type"] as? String
        
        gender = dictionary["gender"] as? String
        age_min = dictionary["age_min"] as? String
        age_max = dictionary["age_max"] as? String
        hobby = dictionary["hobby"] as? String
        industry = dictionary["industry"] as? String
        zipcode = dictionary["zipcode"] as? String
        occupation = dictionary["occupation"] as? String
        city = dictionary["city"] as? String
        state = dictionary["state"] as? String
        country = dictionary["country"] as? String
        
    }
    
    func toDictionary() -> NSDictionary {
        let dictionary = NSMutableDictionary()
        if category != nil{
            dictionary["category"] = category
        }
        if countMutualFriend != nil{
            dictionary["count_mutual_friend"] = countMutualFriend
        }
        if isLiked != nil{
            dictionary["is_liked"] = isLiked
        }
        if addedCart != nil{
            dictionary["cart_added"] = addedCart
        }
        if isSold != nil{
            dictionary["is_sold"] = isSold
        }
        if productDescription != nil{
            dictionary["product_description"] = productDescription
        }
        if productId != nil{
            dictionary["product_id"] = productId
        }
        if productImage != nil{
            dictionary["product_image"] = productImage
        }
        if productLatitude != nil{
            dictionary["product_latitude"] = productLatitude
        }
        if productLongitude != nil{
            dictionary["product_longitude"] = productLongitude
        }
        if productName != nil{
            dictionary["product_name"] = productName
        }
        if productOwnerId != nil{
            dictionary["product_owner_id"] = productOwnerId
        }
        if productOwnerImage != nil{
            dictionary["product_owner_image"] = productOwnerImage
        }
        if productOwnerName != nil{
            dictionary["product_owner_name"] = productOwnerName
        }
        if productOwnerPhone != nil{
            dictionary["product_owner_phone"] = productOwnerPhone
        }
        if productPrice != nil{
            dictionary["product_price"] = productPrice
        }
        if verifyFacebook != nil{
            dictionary["verify_facebook"] = verifyFacebook
        }
        if verifyGoogle != nil{
            dictionary["verify_google"] = verifyGoogle
        }
    
        if enableQuantity != nil {
            dictionary["quantity_available"] = enableQuantity
        }
        if userQuantity != nil {
            dictionary["quantity_per_user"] = userQuantity
        }
        
        if claimed != nil {
            dictionary["sold_count"] = claimed
        }
        
        if type != nil {
            dictionary["type"] = type
        }
        
        if gender != nil {
            dictionary["gender"] = gender
        }
        if age_min != nil {
            dictionary["age_min"] = age_min
        }
        if age_max != nil {
            dictionary["age_max"] = age_max
        }
        if hobby != nil {
            dictionary["hobby"] = hobby
        }
        if industry != nil {
            dictionary["industry"] = industry
        }
        if zipcode != nil {
            dictionary["zipcode"] = zipcode
        }
        if occupation != nil {
            dictionary["occupation"] = occupation
        }
        if city != nil {
            dictionary["city"] = city
        }
        if state != nil {
            dictionary["state"] = state
        }
        if country != nil {
            dictionary["country"] = country
        }
        
        
        return dictionary
    }
    
    @objc required init(coder aDecoder: NSCoder) {
        category = aDecoder.decodeObject(forKey:
            "category") as? String
        countMutualFriend = aDecoder.decodeObject(forKey: "count_mutual_friend") as? Int
        isLiked = aDecoder.decodeObject(forKey: "is_liked") as? String
        addedCart = aDecoder.decodeObject(forKey: "cart_added") as? String
        isSold = aDecoder.decodeObject(forKey: "is_sold") as? String
        productDescription = aDecoder.decodeObject(forKey: "product_description") as? String
        productId = aDecoder.decodeObject(forKey: "product_id") as? String
        productImage = aDecoder.decodeObject(forKey: "product_image") as? [String]
        productLatitude = aDecoder.decodeObject(forKey: "product_latitude") as? String
        productLongitude = aDecoder.decodeObject(forKey: "product_longitude") as? String
        productName = aDecoder.decodeObject(forKey: "product_name") as? String
        productOwnerId = aDecoder.decodeObject(forKey: "product_owner_id") as? String
        productOwnerImage = aDecoder.decodeObject(forKey: "product_owner_image") as? String
        productOwnerName = aDecoder.decodeObject(forKey: "product_owner_name") as? String
        productOwnerPhone = aDecoder.decodeObject(forKey: "product_owner_phone") as? String
        productPrice = aDecoder.decodeObject(forKey: "product_price") as? String
        verifyFacebook = aDecoder.decodeObject(forKey: "verify_facebook") as? String
        verifyGoogle = aDecoder.decodeObject(forKey: "verify_google") as? String
        
        enableQuantity = aDecoder.decodeObject(forKey: "quantity_available") as? String
        userQuantity = aDecoder.decodeObject(forKey: "quantity_per_user") as? String
        claimed = aDecoder.decodeObject(forKey: "sold_count") as? String
        type = aDecoder.decodeObject(forKey: "type") as? String
        
        gender = aDecoder.decodeObject(forKey: "gender") as? String
        age_min = aDecoder.decodeObject(forKey: "age_min") as? String
        age_max = aDecoder.decodeObject(forKey: "age_max") as? String
        hobby = aDecoder.decodeObject(forKey: "hobby") as? String
        industry = aDecoder.decodeObject(forKey: "industry") as? String
        zipcode = aDecoder.decodeObject(forKey: "zipcode") as? String
        occupation = aDecoder.decodeObject(forKey: "occupation") as? String
        city = aDecoder.decodeObject(forKey: "city") as? String
        state = aDecoder.decodeObject(forKey: "state") as? String
        country = aDecoder.decodeObject(forKey: "country") as? String
        
    }
    
    @objc func encode(with aCoder: NSCoder) {

        if category != nil{
            aCoder.encode(category, forKey: "category")
        }
        if countMutualFriend != nil{
            aCoder.encode(countMutualFriend, forKey: "count_mutual_friend")
        }
        if isLiked != nil{
            aCoder.encode(isLiked, forKey: "is_liked")
        }
        if addedCart != nil{
            aCoder.encode(isLiked, forKey: "cart_added")
        }
        if isSold != nil{
            aCoder.encode(isSold, forKey: "is_sold")
        }
        if productDescription != nil{
            aCoder.encode(productDescription, forKey: "product_description")
        }
        if productId != nil{
            aCoder.encode(productId, forKey: "product_id")
        }
        if productImage != nil{
            aCoder.encode(productImage, forKey: "product_image")
        }
        if productLatitude != nil{
            aCoder.encode(productLatitude, forKey: "product_latitude")
        }
        if productLongitude != nil{
            aCoder.encode(productLongitude, forKey: "product_longitude")
        }
        if productName != nil{
            aCoder.encode(productName, forKey: "product_name")
        }
        if productOwnerId != nil{
            aCoder.encode(productOwnerId, forKey: "product_owner_id")
        }
        if productOwnerImage != nil{
            aCoder.encode(productOwnerImage, forKey: "product_owner_image")
        }
        if productOwnerName != nil{
            aCoder.encode(productOwnerName, forKey: "product_owner_name")
        }
        if productOwnerPhone != nil{
            aCoder.encode(productOwnerPhone, forKey: "product_owner_phone")
        }
        if productPrice != nil{
            aCoder.encode(productPrice, forKey: "product_price")
        }
        if verifyFacebook != nil{
            aCoder.encode(verifyFacebook, forKey: "verify_facebook")
        }
        if verifyGoogle != nil{
            aCoder.encode(verifyGoogle, forKey: "verify_google")
        }
        
        if enableQuantity != nil {
            aCoder.encode(enableQuantity, forKey: "quantity_available")
        }
        if userQuantity != nil {
            aCoder.encode(userQuantity, forKey: "quantity_per_user")
        }
        if claimed != nil {
            aCoder.encode(claimed, forKey: "sold_count")
        }
        if type != nil {
            aCoder.encode(claimed, forKey: "type")
        }
        
        
        if gender != nil {
            aCoder.encode(gender, forKey: "gender")
        }
        if age_min != nil {
            aCoder.encode(age_min, forKey: "age_min")
        }
        if age_max != nil {
            aCoder.encode(age_max, forKey: "age_max")
        }
        if hobby != nil {
            aCoder.encode(hobby, forKey: "hobby")
        }
        if industry != nil {
            aCoder.encode(industry, forKey: "industry")
        }
        if zipcode != nil {
            aCoder.encode(zipcode, forKey: "zipcode")
        }
        if occupation != nil {
            aCoder.encode(occupation, forKey: "occupation")
        }
        if city != nil {
            aCoder.encode(city, forKey: "city")
        }
        if state != nil {
            aCoder.encode(state, forKey: "state")
        }
        if country != nil {
            aCoder.encode(country, forKey: "country")
        }
        
    }
    
}
